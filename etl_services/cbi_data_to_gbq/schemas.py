import pandas as pd

CBI_PLENTIFIC_WORKORDER_SCHEMAS = {
    'id':int,
    'plentific_uuid':pd.StringDtype(),
    'request_data':pd.StringDtype(),
    'response_data':pd.StringDtype(),
    'category_code':pd.StringDtype(),
    'hostify_reservation_id':pd.StringDtype(),
    'shared_property_id':float,
    'shared_property_uuid':pd.StringDtype(),
    'shared_property_name':pd.StringDtype(),
    'category_id':pd.StringDtype(),
    'status':pd.StringDtype(),
    'status_change_reason':pd.StringDtype(),
    'activity':pd.StringDtype(),
    'reference_date':pd.StringDtype(),
    'preferred_start_datetime':pd.StringDtype(),
    'preferred_end_datetime':pd.StringDtype(),
    'error':pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},

    }
}


STK_UNITY_SCHEMAS = {
    'id':int,
    'created_by':pd.StringDtype(),
    'updated_by':pd.StringDtype(),
    'is_soft_deleted':pd.BooleanDtype(),
    'base_ptr_id':pd.StringDtype(),
    'name':pd.StringDtype(),
    'property_unique_number':pd.StringDtype(),
    'is_cluster':pd.BooleanDtype(),
    'unit_size':pd.StringDtype(),
    'is_available':pd.BooleanDtype(),
    'occupancy_for':pd.StringDtype(),
    'allowed_people':int,
    'room_type_id':pd.StringDtype(),
    'hostify_parent_id':pd.StringDtype(),
    'hostify_child_id':pd.StringDtype(),
    'is_active_listing':pd.BooleanDtype(),
    'shared_property_id':pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},

    }

}


CBI_HOSTIFY_RESERVATIONS_SCHEMAS = {
    'id':int,
    'data':pd.StringDtype(),
    'hostify_id':pd.StringDtype(),
    'api_account':pd.StringDtype(),
    'hostify_listing_id':pd.StringDtype(),
    'source':pd.StringDtype(),
    'confirmation_code':pd.StringDtype(),
    'hostify_guest_id':pd.StringDtype(),
    'work_orders_applied':pd.StringDtype(),
    'sms_sent':pd.StringDtype(),
    'check_in':pd.StringDtype(),
    'check_out':pd.StringDtype(),
    'status':pd.StringDtype(),
    'accomodation_charge_link':pd.StringDtype(),
    'deposit_charge_link':pd.StringDtype(),
    'is_paid':pd.BooleanDtype(),
    'is_invoiced':pd.BooleanDtype(),
    'is_deposit_invoiced':pd.BooleanDtype(),
    'has_bill_transaction_fee':pd.BooleanDtype(),
    'has_bill_channel_commission':pd.BooleanDtype(),
    'has_bill_deposit_transaction_fee':pd.BooleanDtype(),
    'date_fields': {
        'updated_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},

    }
}


CBI_PLENTIFIC_JOB_SCHEMAS = {
    'id':int,
    'plentific_uuid':pd.StringDtype(),
    'request_data':pd.StringDtype(),
    'response_data':pd.StringDtype(),
    'work_order_id':pd.StringDtype(),
    'job_id':pd.StringDtype(),
    'booking_created':pd.BooleanDtype(),
    'booking_uuid':pd.StringDtype(),
    'booking_status':pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'booking_end_datetime': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'booking_start_datetime': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
    }
}


CBI_XEROINVOICE_SCHEMAS = {
    'id':int,
    'parent_document_id':pd.StringDtype(),
    'xero_tenant_id':pd.StringDtype(),
    'reservation_id':pd.StringDtype(),
    'stripe_payout_id':pd.StringDtype(),
    'xero_tracking_category_option_id':pd.StringDtype(),
    'xero_invoice_id':pd.StringDtype(),
    'xero_invoice_number':pd.StringDtype(),
    'sent_to_guest':pd.BooleanDtype(),
    'doctype':pd.StringDtype(),
    'subtype':pd.StringDtype(),
    'request_data':pd.StringDtype(),
    'response_data':pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'check_in': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},


    }
}


STK_PLENTIFIC_PROPERTY_SCHEMAS = {
    'id':int,
    'plentific_uuid':pd.StringDtype(),
    'request_data':pd.StringDtype(),
    'response_data':pd.StringDtype(),
    'unit_id':pd.StringDtype(),
    'shared_property_id':pd.StringDtype(),
    'created_by':pd.StringDtype(),
    'updated_by':pd.StringDtype(),
    'is_deleted':pd.BooleanDtype(),
    'date_fields': {
        'updated_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
    }
}