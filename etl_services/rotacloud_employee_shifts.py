from rotacloud.fetch_data import fetch_all_rotacloud_employee_shifts
from rotacloud.api_clients.rotacloud_employee_shifts_api_client import RotacloudEmployeeShiftsAPIClient
from rotacloud.schemas import API_ROTACLOUD_EMPLOYEE_SHIFTS_SCHEMA
from gbq.run_query import push_data
from config.credentials import TABLE_ROTACLOUD_EMPLOYEE_SHIFTS
from datetime import datetime, timedelta
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

ROTACLOUD_USERS_API_CLIENT = RotacloudEmployeeShiftsAPIClient()

def start_end_dat():
    now = datetime.now() 
    yesterday = datetime.now() - timedelta(1)
    previous_start = datetime( yesterday.year, yesterday.month, yesterday.day ).timestamp()
    previous_end = datetime( now.year, now.month, now.day ).timestamp()
    return {'start':int(previous_start),'end':int(previous_end)}

START_END_PARAMS = start_end_dat()

try:
    dataframe, _ = fetch_all_rotacloud_employee_shifts(
        ROTACLOUD_USERS_API_CLIENT,
        resource_name = 'rotacloud_employee_shifts',
        schema = API_ROTACLOUD_EMPLOYEE_SHIFTS_SCHEMA,
        params={'start':START_END_PARAMS['start'],'end':START_END_PARAMS['end']}
    )
    push_data(
        dataframe,
        TABLE_ROTACLOUD_EMPLOYEE_SHIFTS,
        if_exists='append'
        )
    logger_info(logger, 'Data pushed in Rotacloud Employee Shifts')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ROTACLOUD EMPLOYEE SHIFTS WHILE PUSHING DATA")