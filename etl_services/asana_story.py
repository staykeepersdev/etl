from asana.fetch_data import fetch_all_asana_data
from asana.api_clients.story_api_client import StoryAPIClient
from asana.schemas import API_STORY_SCHEMA
from config.credentials import TABLE_ASANA_TASK_STORY
from gbq.run_query import push_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

STORY_API_CLIENT = StoryAPIClient()

try:
    dataframe, _ = fetch_all_asana_data(
        STORY_API_CLIENT,
        resource_name = 'asana_ops_tasks_story',
        schema = API_STORY_SCHEMA
    )


    push_data(
        dataframe,
        TABLE_ASANA_TASK_STORY,
        if_exists='append'
        )
    logger_info(logger, f"Data pushed in Asana Story")
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ASANA STORY WHILE PUSHING DATA")

