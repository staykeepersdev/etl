''' INSERT ONLY (append table) - KEEP HISTORICAL DATA
    Insert new and updated listings data as new row to keep data history.
'''
import logging

from config.credentials  import (TABLE_STK_LISTINGS, 
                                TABLE_LP_LISTINGS, 
                                TABLE_TLL_SLL_LISTINGS
                                )
from hostify.api_clients.listing_api_client import ListingAPIClient
from hostify.fetch_data import fetch_all_hostify_data
from hostify.schemas import (API_LISTING_SCHEMA,
                             API_LISTING_STK_CUSTOM_FIELDS,
                             API_LISTING_LP_CUSTOM_FIELDS
                            )
from slack.utils import exception_catcher
from utils import prepare_push_data, parse_args



logger = logging.getLogger(__name__)
STK_LISTING_API_CLIENT = ListingAPIClient('Staykeepers')
LEAP_SLL_TLL_LISTING_API_CLIENT = ListingAPIClient()


@exception_catcher
def import_stk_listings():
    """STK_LISTINGS ETL """

    logger.info('STK Listings ETL Process started')
    stk_dataframe, all_schema = fetch_all_hostify_data(api_client=STK_LISTING_API_CLIENT,
                                                       resource_name='listings',
                                                       schema=API_LISTING_SCHEMA,
                                                       custom_fields_schema=API_LISTING_STK_CUSTOM_FIELDS,
                                                       params={
                                                           'per_page': 1000}
                                                       )
    logger.info('STK Listings - Fetched all Hostify Data')

    prepare_push_data(stk_dataframe,
                      all_schema,
                      TABLE_STK_LISTINGS
                      )

    logger.info('STK Listings ETL Process completed')


@exception_catcher
def import_leap_listings():
    """LEAP_SLL_TLL_LISTINGS ETL """

    logger.info('LEAP_SLL_TLL Listings ETL Process started')

    lp_sll_tll_df, all_schema = fetch_all_hostify_data(api_client=LEAP_SLL_TLL_LISTING_API_CLIENT,
                                                       resource_name='listings',
                                                       schema={
                                                           **API_LISTING_SCHEMA},
                                                       custom_fields_schema=API_LISTING_LP_CUSTOM_FIELDS,
                                                       params={
                                                           'per_page': 1000}
                                                       )
    logger.info('LEAP_SLL_TLL Listings - Fetched all Hostify Data')

    # LP prefix
    leap_dataframe = lp_sll_tll_df[lp_sll_tll_df['nickname'].str.startswith(
        'LP')]

    # LEAP
    prepare_push_data(leap_dataframe,
                      all_schema,
                      TABLE_LP_LISTINGS
                      )

    logger.info('LP Listings ETL Process completed')

    # ('SLL_', 'TLL_', 'TTL_') and no prefix
    tll_sll_dataframe = lp_sll_tll_df[lp_sll_tll_df['nickname'].str.startswith(
        ('SLL_', 'TLL_', 'TTL_'))]

    # TLL_SLL
    prepare_push_data(tll_sll_dataframe,
                      all_schema,
                      TABLE_TLL_SLL_LISTINGS
                      )

    logger.info('TLL_SLL Listings ETL Process completed')


if __name__ == '__main__':
    parser = parse_args()
    args = parser.parse_args()
    func_call = args.__dict__['type']
    logging.basicConfig(filename='/var/log/etl', level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    if func_call == 'stk':
        import_stk_listings()
    elif func_call == 'leap':
        import_leap_listings()
