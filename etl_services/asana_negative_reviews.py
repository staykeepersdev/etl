import logging
from asana.fetch_data import fetch_all_asana_data
from asana.api_clients.task_api_client import TaskAPIClient
from config.credentials import TABLE_ASANA_NEGATIVE_REVIEWS, NEGATIVE_REVIEW_PROJECT_ID
from gbq.run_query import push_data
from asana.schemas import API_NEGATIVE_REVIEW_SCHEMA
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

TASK_API_CLIENT = TaskAPIClient()

logger = logging.getLogger(__name__)

try:
    dataframe, _ = fetch_all_asana_data(
        TASK_API_CLIENT,
        resource_name = 'asana_negative_guest_reviews',
        schema = API_NEGATIVE_REVIEW_SCHEMA,
        project_id = NEGATIVE_REVIEW_PROJECT_ID,
        params={'opt_pretty':'true', 'opt_expand':"assignee"}
    )

    push_data(
        dataframe,
        TABLE_ASANA_NEGATIVE_REVIEWS,
        if_exists='replace'
        )
    logger_info(logger, f'Asana Negative Reviews ETL Process completed - Replaced entire table with '
                f'new Negative Reiviews data')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ASANA NEGATIVE REVIEWS WHILE PUSHING DATA")