
import os
import sys
import unittest
import pandas as pd

sys.path.insert(0, os.path.abspath(
    os.path.join(os.path.dirname(__file__), '..')))

from utils import compare_data


# Historical Data Test cases - Listings, Reservations, Transactions

class TestHistoricalData(unittest.TestCase):

    def setUp(self):
        # gbq data
        self.historical_data = [
            [1, 'Test 1', 'test_xx_22', False],
            [1, 'Test 1 - updated', 'test_xx_22', False],
            [2, 'Test 2', 'test_xx_22', False],
            [2, 'Test 2', 'test_xx_22', True],
            [3, 'Test 3', 'test_xx_33', False],
            [4, 'Test 4', 'test_xx_44', False],
            [6, 'Test 6', 'test_xx_66', False]
        ]
        self.column_names = ['api_id', 'name', 'nickname', 'is_deleted']
        self.source_df = pd.DataFrame(
            self.historical_data, columns=self.column_names)

    def test_new_insert_data(self):
        # is_deleted column is being added as a new column of dataframe 
        # after fetching data from Hostify - default value assigned as False
        # means data is active
        
        new_data = [[1, 'Test 1 - updated', 'test_xx_22', False],
                    [5, 'Test 5', 'test_xx_55', False],  # --> new insert
                    [4, 'Test 4', 'test_xx_44', False],
                    [3, 'Test 3', 'test_xx_33', False]
                    ]

        new_df = pd.DataFrame(new_data, columns=self.column_names)

        push_data = compare_data('', new_df, self.source_df)
        push_data.drop(['batch_inserted_at'], axis=1, inplace=True)
        
        # Case 1  : New Inserts
        # Check if [5, 'Test 5', 'test_xx_55', False] in push_data which will be
        # appended as new row
        self.assertIn([5, 'Test 5', 'test_xx_55', False],
                      push_data.values.tolist())


    def test_insert_updated_data(self):

        # is_deleted = False assinged automatically after fetching data from Hostify
        # means data is active
        new_data = [[1, 'Test 1 - updated', 'test_xx_22', False],
                    [4, 'Test 4', 'test_xx_44', False],
                    [3, 'Test 3', 'test_xx_33', False],
                    [6, 'Test 6', 'test_xx_66**', False]  # --> updated
                    ]

        new_df = pd.DataFrame(new_data, columns=self.column_names)

        push_data = compare_data('', new_df, self.source_df)
        push_data.drop(['batch_inserted_at'], axis=1, inplace=True)

        # Case 2 : Updated data
        # Check if  [6, 'Test 6', 'test_xx_66**', False] in push_data which will be
        # appended as new row to keep historical data

        self.assertIn([6, 'Test 6', 'test_xx_66**', False],
                      push_data.values.tolist())


    def test_inactive_data(self):
        '''
        If for example; a stk listing is inactive and it's data not in the newly 
        fetched Hostify data, assign is_deleted column as True to keep inactive 
        listings historical data
        '''

        # Case 1 : If data with api_id 6 deleted, update is_deleted column as True and
        # append  [6, 'Test 6', 'test_xx_66', True] as a new row

        # Case --> [6, 'Test 6', 'test_xx_66', False] exists in historical df, not in new_data

        new_data = [[1, 'Test 1 - updated', 'test_xx_22', False],
                    [4, 'Test 4', 'test_xx_44', False],
                    [3, 'Test 3', 'test_xx_33', False],
                    ]

        new_df = pd.DataFrame(new_data, columns=self.column_names)
        push_data = compare_data('', new_df, self.source_df)
        push_data.drop(['batch_inserted_at'], axis=1, inplace=True)
        
        # Check [6, 'Test 6', 'test_xx_66', True] in push_data
        self.assertIn([6, 'Test 6', 'test_xx_66', True],
                      push_data.values.tolist())


    def test_inactive_data_to_active(self):
        '''
        If for example; an inactive stk listing active again and it's data in the newly 
        fetched Hostify data, assign is_deleted column as False and insert a new row
        to make it active again
        ''' 
        self.historical_data.remove([6, 'Test 6', 'test_xx_66', False])
        self.historical_data.append([6, 'Test 6', 'test_xx_66', True])
        new_source_df = pd.DataFrame(
            self.historical_data, columns=self.column_names)

        # Case 2: If api_id=6 is active again, append new row with column is_deleted as False

        new_data = [[1, 'Test 1 - updated', 'test_xx_22', False],
                    [4, 'Test 4', 'test_xx_44', False],
                    [3, 'Test 3', 'test_xx_33', False],
                    [6, 'Test 6', 'test_xx_66', False]
                    ]

        new_df = pd.DataFrame(new_data, columns=self.column_names)

       
        
        push_data = compare_data('', new_df, new_source_df)
        push_data.drop(['batch_inserted_at'], axis=1, inplace=True)

        # Check [6, 'Test 6', 'test_xx_66', False] in push_data
        self.assertIn([6, 'Test 6', 'test_xx_66', False],
                      push_data.values.tolist())
    




if __name__ == '__main__':
    unittest.main()
