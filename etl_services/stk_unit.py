from datetime import datetime, timedelta

import pandas as pd

from config.credentials import STK_BASE_URL, TABLE_STK_UNIT_TABLE_NAME
from gbq.run_query import push_data
from push_cbi_and_stk_data.call_api import fetch_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info
from cbi_data_to_gbq.schemas import STK_UNITY_SCHEMAS
from hostify.utils import get_schema_columns



logger = logging.getLogger(__name__)

schema = STK_UNITY_SCHEMAS
params = {"date" : datetime.now().date() - timedelta(days=1)}

try:
    data = fetch_data(f'{STK_BASE_URL}unit',params)
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN STK UNIT API ON {params['date']}")
try:
    for obj in data:
        obj['base_ptr_id'] = str(obj['base_ptr_id'])
        obj['room_type_id'] = str(obj['room_type_id'])
        obj['property_unique_number'] = str(obj['property_unique_number'])
    column_names = get_schema_columns(schema)
    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    unit_df = pd.DataFrame(data,columns=column_names).astype(all_schema) 
    unit_df['created_at'] = pd.to_datetime(unit_df["created_at"])
    unit_df['updated_at'] = pd.to_datetime(unit_df["updated_at"])
    if not unit_df.empty:
        push_data(unit_df, TABLE_STK_UNIT_TABLE_NAME,  if_exists="append")
        logger_info(logger, 'Unit Data Pushed in GBQ')
    else:
        logger_info(logger, 'No Data Found in Unit')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN STK UNIT WHILE PUSHING DATA ON {params['date']}")