from .base import BaseAPIClient

class ProjectsAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='projects')

    def get_projecst(self,params):
        return self._get_json_response(url=f'{self.list_url()}' , params = params)

