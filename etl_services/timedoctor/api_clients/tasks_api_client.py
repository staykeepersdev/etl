from .base import BaseAPIClient

class TasksAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='tasks')

    def get_tasks(self,params):
        return self._get_json_response(url=f'{self.list_url()}' , params = params)

