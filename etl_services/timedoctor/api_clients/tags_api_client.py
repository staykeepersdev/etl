from .base import BaseAPIClient

class TagsAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='tags')

    def get_tags(self,params):
        return self._get_json_response(url=f'{self.list_url()}' , params = params)

