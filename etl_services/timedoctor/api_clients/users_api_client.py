from .base import BaseAPIClient

class UserAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='users')

    def get_user(self,params):
        return self._get_json_response(url=f'{self.list_url()}' , params = params)

    def get_user_list(self,params):
        user_list = []
        data = self._get_json_response(url=f'{self.list_url()}' , params = params)['data']
        for i in data:
            user_list.append(i['id'])
        return user_list
