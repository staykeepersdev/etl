from datetime import datetime
import json
import requests
from urllib.parse import urljoin
from config.credentials import TIMEDOCTOR_BASE_URL, TIMEDOCTOR_HEADER_ACCEPT

def save_new_token(new_token):
    with open('timedoctor_token.json', 'w') as json_file:
        json.dump(new_token,json_file)

try: 
    def handling_expired_access_token():
        with open('timedoctor_token.json', 'r') as json_file:
            result = json.load(json_file)
            token_regenerate_token_url =  requests.post(url=f'{TIMEDOCTOR_BASE_URL}authorization/login',headers={'Content-Type':TIMEDOCTOR_HEADER_ACCEPT,'Accept':TIMEDOCTOR_HEADER_ACCEPT},json={
                                                        "email": "hr@staykeepers.com",
                                                        "password": "HR@timedoctor2",
                                                        "permissions": "write"
                                                        },params={'token':result.get('token')})
            if token_regenerate_token_url.status_code == 200:
                save_new_token(new_token=json.loads(token_regenerate_token_url.content)['data'])
                return json.loads(token_regenerate_token_url.content)['data']
            
except Exception as e:
    print(e)

with open('timedoctor_token.json', 'r') as json_file:
            ACCESS_AND_REFRESH_TOKEN = json.load(json_file)['token']
class BaseAPIClient:

    def __init__(
        self,
        endpoint=None,
        url=TIMEDOCTOR_BASE_URL,
    ):
        self.endpoint = endpoint
        self.url = url
        self.headers = {
            'Accept' : TIMEDOCTOR_HEADER_ACCEPT
        }
        
    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers, params=params)
        if response.status_code == 401:
            params_copy=params.copy()
            params_copy['token'] = handling_expired_access_token()['token']
            response = requests.get(
                url=url, headers=self.headers, params=params_copy)
        return json.loads(response.content)

    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def get_list(self,params):
        if self.__class__.__name__ == 'UserAPIClient':
            data = self.get_user(params)['data']
            for i in  data:
                i['hiredAt'] = i['hiredAt'].replace('T',' ')[:-5]
                i['createdAt'] = i['createdAt'].replace('T',' ')[:-5]
                i['tagIds'] = i['tagIds'] if i['tagIds'] else None
                i['onlyProjectIds'] = i['onlyProjectIds'] if i['onlyProjectIds'] else None
            return data
    
        elif self.__class__.__name__ == 'WorklogsAPIClient':
            data = self.get_worklogs(params)['data']
            final_data=[]
            for i in data:
                for j in i:
                    j['start'] = j['start'].replace('T',' ')[:-5]
                    final_data.append(j)  
            return final_data
        
        elif self.__class__.__name__ == 'TagsAPIClient':
            data = self.get_tags(params)['data']
            for i in data:
                i['createdAt'] = i['createdAt'].replace('T',' ')[:-5]
                i['modifiedAt'] = i['modifiedAt'].replace('T',' ')[:-5]
                i['managers'] = i['managers'] if i['managers'] else None
                i['selfUser'] = i.get("selfUser", 0)
            return data 
        
        elif self.__class__.__name__ == 'ProjectsAPIClient':
            return self.get_projecst(params)['data']
        
        elif self.__class__.__name__ == 'TasksAPIClient':
            data = self.get_tasks(params)['data']
            for i in data:
                i['folders'] = i['folders'] if i['folders'] else None
            return data
  