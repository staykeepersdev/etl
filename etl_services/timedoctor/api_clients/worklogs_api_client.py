from .base import BaseAPIClient

class WorklogsAPIClient(BaseAPIClient):
    
    def __init__(self):
        super().__init__(endpoint='activity/worklog')

    def get_worklogs(self,params):
        return self._get_json_response(url=f'{self.list_url()}' , params = params)
