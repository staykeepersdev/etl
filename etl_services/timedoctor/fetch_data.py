import logging
import pandas
import pytz
from datetime import datetime
import concurrent.futures
from hostify.utils import get_schema_columns, raise_api_conn_err
logger = logging.getLogger(__name__)

DEFAULT_MAX_WORKERS = 10

def fetch_all_timdoctor_user_data(api_client,
                           resource_name,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Timedoctor Users Data"""
    resources = []
    data = api_client.get_list(params=params)
    if not data:
        raise_api_conn_err(data.get('error'), api_client, params)    
    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names = get_schema_columns(custom_fields_schema, custom=True)

    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client,params,
            resource_name, column_names,
            schema, custom_fields_schema)}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())
    logger.info(f'Collected all {resource_name} data from Timedoctor Users api')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
 
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema

def fetch_all_timdoctor_worklogs_data(api_client,
                           resource_name,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Timedoctor Worklogs Data"""
    resources = []
    page=1
    data = api_client.get_list(params=params)
    if not data:
        raise_api_conn_err(data.get('error'), api_client, params)

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names = get_schema_columns(custom_fields_schema, custom=True)

    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client,params,
            resource_name, column_names,
            schema, custom_fields_schema)}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())
    logger.info(f'Collected all {resource_name} data from Timedoctor Worklogs api')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
 
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema

def fetch_all_timdoctor_tags_data(api_client,
                           resource_name,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Timedoctor Tags Data"""
    resources = []
    data = api_client.get_list(params=params)
    if not data:
        raise_api_conn_err(data.get('error'), api_client, params)

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names = get_schema_columns(custom_fields_schema, custom=True)
    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client,params,
            resource_name, column_names,
            schema, custom_fields_schema)}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())
    
    logger.info(f'Collected all {resource_name} data from Timedoctor Tags api')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
 
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema

def fetch_all_timdoctor_projects_data(api_client,
                           resource_name,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Timedoctor  Projects Data"""
    resources = []
    data = api_client.get_list(params=params)
    if not data:
        raise_api_conn_err(data.get('error'), api_client, params)

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names = get_schema_columns(custom_fields_schema, custom=True)
    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client,params,
            resource_name, column_names,
            schema, custom_fields_schema)}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())
    
    logger.info(f'Collected all {resource_name} data from Timedoctor Projects api')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
 
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema

def fetch_all_timdoctor_tasks_data(api_client,
                           resource_name,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Timedoctor Tasks Data"""
    resources = []
    data = api_client.get_list(params=params)
    if not data:
        raise_api_conn_err(data.get('error'), api_client, params)

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names = get_schema_columns(custom_fields_schema, custom=True)
    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client,params,
            resource_name, column_names,
            schema, custom_fields_schema)}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())
    
    logger.info(f'Collected all {resource_name} data from Timedoctor Tasks api')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
 
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema

def api_call_per_page(api_client, params,
                      resource_name,
                      column_names,
                      schema,
                      custom_fields_schema):
    resources = []
    data = api_client.get_list(params=params)
    
    if data:
        resource = data
        for row in resource:
            # Convert custom fields datetime columns string to datetime
            if custom_fields_schema:
                custom_fields = row.get('custom_fields')
                row.update({f"custom_field_{field.get('id')}": field.get(
                    'value') for field in custom_fields})

                for key, val in custom_fields_schema['date_fields'].items():
                    timestamp = datetime.strptime(row[f"custom_field_{key}"], val['format']
                                                  ).replace(tzinfo=pytz.utc
                                                            ) if row.get(f"custom_field_{key}") else None

                    row[f"custom_field_{key}"] = timestamp

            # Convert Datetime columns string to datetime
            date_fields = schema.get('date_fields')
            if date_fields:
                for key, val in date_fields.items():
                    timestamp = datetime.strptime(row[key], val['format']
                                                  ).replace(tzinfo=pytz.utc
                                                            ) if row[key] else None
                    row[key] = timestamp

            # Fix for: some float columns have empty string data. This is required to cast float type.
            new_row = [[None if value == '' else value for value in list(
                map(row.get, column_names))]]
            resources.extend(new_row)

    else:
        err_msg = data.get('error')
        raise_api_conn_err(err_msg, api_client, params)
    return resources