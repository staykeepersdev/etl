import pandas as pd

API_TIMEDOCTOR_USER_SCHEMA = {
    'role' : pd.StringDtype(),
    'tagIds' : pd.StringDtype(),
    'onlyProjectIds' : pd.StringDtype(),
    'timezone' : pd.StringDtype(),
    'isSilent' : pd.BooleanDtype(),
    'isInteractive' : pd.BooleanDtype(),
    'trackingMode' : pd.StringDtype(),
    'name' : pd.StringDtype(),
    'employeeId' : pd.StringDtype(),
    'exists' : pd.BooleanDtype(),
    'active' : pd.BooleanDtype(),
    'screenshots' : int,
    'videos' : pd.StringDtype(),
    'workCheckInterval' : int,
    'allowEditTime' : pd.BooleanDtype(),
    'poorTimeusePopup' : pd.BooleanDtype(),
    'allowDeleteScreenshots' : pd.BooleanDtype(),
    'tasksMode' : pd.StringDtype(),
    'trackingType' : pd.StringDtype(),
    'showOnReports' : pd.BooleanDtype(),
    'emailReports' : pd.StringDtype(),
    'blurScreenshots' : pd.BooleanDtype(),
    'allowNoBreak' : pd.BooleanDtype(),
    'trackInternetConnectivity' : pd.StringDtype(),
    'signupStep' : int,
    'stripUrlQuery' : pd.BooleanDtype(),
    'allowAdminSID' : pd.BooleanDtype(),
    'hideScreencasts' : pd.BooleanDtype(),
    'payrollAccess' : pd.BooleanDtype(),
    'billingAccess' : pd.BooleanDtype(),
    'payrollFeature' : pd.BooleanDtype(),
    'screencastsFeature' : pd.BooleanDtype(),
    'workScheduleFeature' : pd.BooleanDtype(),
    'trackConnectivity' : pd.BooleanDtype(),
    'allowManagerTagCategories' : pd.BooleanDtype(),
    'allowManagerProjectsTasks' : pd.BooleanDtype(),
    'allowManagerInviteUsers' : pd.BooleanDtype(),
    'allowManagerWorkSchedules' : pd.BooleanDtype(),
    'forceAutostart' : pd.BooleanDtype(),
    'firstDayOfWeek' : int,
    'custom' : pd.StringDtype(),
    'webAndAppTracking' : pd.StringDtype(),
    'worklife' : pd.StringDtype(),
    'id' : pd.StringDtype(),
    'email' : pd.StringDtype(),
    'emailConfirmed' : pd.BooleanDtype(),
    'hasPassword' : pd.BooleanDtype(),
    'managerBannerDismissed' : pd.StringDtype(),
    'status' : pd.StringDtype(),
    'date_fields': {
        'hiredAt': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'createdAt': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
    }
}

API_TIMEDOCTOR_WORKLOGS_SCHEMA = {
    'userId' : pd.StringDtype(),
    'taskId' : pd.StringDtype(),
    'taskName' : pd.StringDtype(),
    'time' : int,
    'mode' : pd.StringDtype(),
    'projectId' : pd.StringDtype(),
    'projectName' : pd.StringDtype(),
    'reason' : pd.StringDtype(),
    'deviceId' : pd.StringDtype(),
    'date_fields': {
        'start': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
    }
}

API_TIMEDOCTOR_TAGS_SCHEMA = {
    'name' : pd.StringDtype(),
    'special' : pd.StringDtype(),
    'users' : int,
    'usersOnReports' : int,
    'selfUser' : int,
    'managedUsers' : int,
    'managedUsersOnReports' : int,
    'id' : pd.StringDtype(),
    'creatorId' : pd.StringDtype(),
    'deleted' : pd.BooleanDtype(),
    'readOnly' : pd.BooleanDtype(),
    'managers' : pd.StringDtype(),
    'date_fields': {
        'createdAt': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'modifiedAt': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
    }
}

API_TIMEDOCTOR_PROJECTS_SCHEMA = {
    'id' : pd.StringDtype(),
    'name' : pd.StringDtype(),
    'creatorId' : pd.StringDtype(),
    'weight' : int,
    'deleted' : pd.BooleanDtype(),
    'scope' : pd.StringDtype(),
    'integration' : pd.StringDtype(),
}

API_TIMEDOCTOR_TASKS_SCHEMA = {
    'id' : pd.StringDtype(),
    'name' : pd.StringDtype(),
    'project' : pd.StringDtype(),
    'status' : pd.StringDtype(),
    'reporterId' : pd.StringDtype(),
    'deleted' : pd.BooleanDtype(),
    'folders' : pd.StringDtype(),
    'integration' : pd.StringDtype(),
}