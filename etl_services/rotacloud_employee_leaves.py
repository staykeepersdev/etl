from rotacloud.fetch_data import fetch_all_rotacloud_employee_leaves
from rotacloud.api_clients.rotacloud_employee_leaves_api_client import RotacloudEmployeeLeavesAPIClient
from rotacloud.schemas import API_ROTACLOUD_EMPLOYEE_LEAVES_SCHEMA
from gbq.run_query import push_data
from config.credentials import TABLE_ROTACLOUD_EMPLOYEE_LEAVES
from datetime import datetime
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

ROTACLOUD_USERS_API_CLIENT = RotacloudEmployeeLeavesAPIClient()
THIS_YEAR = datetime.now().year
try:
    dataframe, _ = fetch_all_rotacloud_employee_leaves(
        ROTACLOUD_USERS_API_CLIENT,
        resource_name = 'rotacloud_employee_leaves',
        schema = API_ROTACLOUD_EMPLOYEE_LEAVES_SCHEMA,
        params={'year':THIS_YEAR}
    )
    push_data(
        dataframe,
        TABLE_ROTACLOUD_EMPLOYEE_LEAVES,
        if_exists='replace'
        )
    logger_info(logger, 'Data pushed in Rotacloud Employee Leaves')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ROTACLOUD EMPLOYEE LEAVES WHILE PUSHING DATA")
