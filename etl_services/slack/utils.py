import os
import traceback

from config.credentials import DIR_PATH
from slack.webhook_client import SlackWebhookClient


SLACK_WEBHOOK_CLIENT = SlackWebhookClient()


def create_blocks_params(title, message):
    blocks = [
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": title
            }
        },

        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": message
            }
        },
        {
            "type": "divider"
        }
    ]
    return blocks


def report_error_to_slack(blocks):

    return SLACK_WEBHOOK_CLIENT.post_webhook_message(blocks)


def slack_exception_handler(errors=(Exception, ), default_value=''):
    def decorator(func):
        def inner_function(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                parse_push_error(func.__doc__, e)
                return default_value

        return inner_function
    return decorator


exception_catcher = slack_exception_handler((), default_value='default')


def parse_push_error(func_name, e):
    virtual_env_path = os.path.dirname(DIR_PATH) + '/venv'
    error_lists = [str(traceback_err).replace("\n", " ").strip() for traceback_err in traceback.format_tb(
        e.__traceback__) if virtual_env_path not in traceback_err]
    traceback_str = '\n'.join(error_lists)

    report_error_to_slack(
        create_blocks_params(f'*{func_name} Process Failed *',
                             f'*Error* \n {repr(e)} \n'
                             f'*Traceback*: \n {traceback_str}'
                             ))
