import json
import requests

from urllib.parse import urljoin


class BaseAPIClient:
    def _response_handle(self, response):
        if response.status_code == 204:
            return True
        try:
            return json.loads(response.content)
        except:
            return response.content

    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers, params=params)
        return self._response_handle(response)

    def _post_json_response(self, url: str, data=None):
        response = requests.post(url=url, headers=self.headers, json=data)
        return self._response_handle(response)

    def _patch_json_response(self, url: str, data=None):
        response = requests.patch(url=url, headers=self.headers, json=data)
        return self._response_handle(response)

    def _delete_json_response(self, url: str, data=None):
        response = requests.delete(url=url, headers=self.headers, json=data)
        return self._response_handle(response)

    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def detail_url(self, identifier: int):
        return urljoin(self.list_url(), str(identifier))

    def get_list(self, params=None):
        url = self.list_url()
        return self._get_json_response(url=url, params=params)

    def get_item(self, identifier: int, params=None):
        url = self.detail_url(identifier=identifier)
        return self._get_json_response(url=url, params=params)

    def post_item(self, data=None):
        url = self.list_url()
        return self._post_json_response(url=url, data=data)

    def patch_item(self, identifier: int, data=None):
        url = self.detail_url(identifier=identifier)
        return self._patch_json_response(url=url, data=data)

    def delete_item(self, identifier: int, data=None):
        url = self.detail_url(identifier=identifier)
        return self._delete_json_response(url=url, data=data)
