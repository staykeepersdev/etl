from .base import BaseAPIClient
from config.credentials import SLACK_WEBHOOK_ENDPOINT, SLACK_WEBHOOK_URL


class SlackWebhookClient(BaseAPIClient):
    def __init__(self):
        self.url = SLACK_WEBHOOK_URL
        self.endpoint = SLACK_WEBHOOK_ENDPOINT
        self.headers = {
            'Content-type': 'application/json'
        }

    def post_webhook_message(self, blocks):
        data = {
            'blocks': blocks
        }
        return self.post_item(data)
