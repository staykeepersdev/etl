from config.credentials import TABLE_TIMEDOCOTR_USERS_TABLE_NAME,COMPANY_ID
from gbq.run_query import push_data
from timedoctor.fetch_data import fetch_all_timdoctor_user_data
from timedoctor.api_clients.users_api_client import UserAPIClient
from timedoctor.schemas import API_TIMEDOCTOR_USER_SCHEMA
from timedoctor.api_clients.base import ACCESS_AND_REFRESH_TOKEN
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)


USER_API_CLIENT = UserAPIClient()

try:
  user_dataframe, _ = fetch_all_timdoctor_user_data(api_client=USER_API_CLIENT,
                                            resource_name='user_data',
                                            schema=API_TIMEDOCTOR_USER_SCHEMA,
                                            params={
                                                  'token':ACCESS_AND_REFRESH_TOKEN,
                                                  'company':COMPANY_ID, 
                                                  'limit':500,
                                                }
                                            )
  push_data(user_dataframe,
            TABLE_TIMEDOCOTR_USERS_TABLE_NAME,
            if_exists='replace'
          )
  logger_info(logger, 'User data pushed in GBQ')
except Exception as e:
  logger_error(logger, f"{e}: EXCEPTION OCCURED IN TIMEDOCTOR USERS WHILE PUSHING DATA")