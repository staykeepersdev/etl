import logging
from asana.fetch_data import fetch_all_asana_data
from asana.api_clients.task_api_client import TaskAPIClient
from config.credentials import TABLE_ASANA_GUEST_REQUEST_OR_COMPLAINT, GUEST_REQUESTS_OR_COMPLAINT
from gbq.run_query import push_data
from asana.schemas import API_GUEST_REQUESTS_AND_COMPLAINT_SCHEMA
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

TASK_API_CLIENT = TaskAPIClient()

try:
    dataframe, _ = fetch_all_asana_data(
        TASK_API_CLIENT,
        resource_name = 'asana_guest_review_or_complaint',
        schema = API_GUEST_REQUESTS_AND_COMPLAINT_SCHEMA,
        project_id = GUEST_REQUESTS_OR_COMPLAINT,
        params={'opt_pretty':'true', 'opt_expand':"assignee"}
    )

    push_data(
        dataframe,
        TABLE_ASANA_GUEST_REQUEST_OR_COMPLAINT,
        if_exists='replace'
        )

    logger_info(logger, f'Asana Guest Request Or Complaint ETL Process completed - Replaced entire table with '
                f'NEW Asana Guest Request Or Complaint data')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN GUEST REQUEST OR COMPLAINT WHILE PUSHING DATA")