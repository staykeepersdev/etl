from asana.fetch_data import fetch_all_asana_data
from asana.api_clients.task_api_client import TaskAPIClient
from asana.schemas import API_TASK_SCHEMA
from config.credentials import TABLE_ASANA_OPS_TASKS
from gbq.run_query import push_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

TASK_API_CLIENT = TaskAPIClient()

try:
    dataframe, _ = fetch_all_asana_data(
        TASK_API_CLIENT,
        resource_name = 'asana_ops_tasks',
        schema = API_TASK_SCHEMA
    )

    push_data(
        dataframe,
        TABLE_ASANA_OPS_TASKS,
        if_exists='append'
        )
    logger_info(logger, f"Data pushed for Asana Tasks")
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ASANA TASKS WHILE PUSHING DATA")
