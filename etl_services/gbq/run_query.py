import pandas
import pandas_gbq

from google.oauth2 import service_account
from config.credentials import (GBQ_DATASET_ID,
                                GBQ_CHUNK_SIZE,
                                GBQ_PROJECT,
                                GBQ_LOCATION,
                                GOOGLE_CLI_CREDS)


#  Credentials to use for Google APIs.
GOOGLE_CLIENT_CREDENTIALS = service_account.Credentials.from_service_account_file(
    filename=GOOGLE_CLI_CREDS
)
pandas_gbq.context.credentials = GOOGLE_CLIENT_CREDENTIALS
pandas_gbq.context.project = GBQ_DATASET_ID


def push_data(dataframe, table_name, if_exists='append'):
    '''
    table_name e.g : `landlordhostifydatabase.stk_listings`

    if_exists: 
        'replace'
        If table exists, drop it, recreate it, and insert data.

        'append'
        If table exists, insert data. Create if does not exist.

    '''
    full_table_name = '%s.%s' % (GBQ_PROJECT, table_name)

    # For debugging ...
    # print(f'\nDataframe keys:\n{dataframe.keys()}\n\nDataframe data types:\n{dataframe.dtypes}\n')

    dataframe.to_gbq(
        destination_table=full_table_name,
        project_id=GBQ_DATASET_ID,
        location=GBQ_LOCATION,
        chunksize=GBQ_CHUNK_SIZE,
        if_exists=if_exists
    )


def run_query(table_name=None, query=None):
    '''
    full_table_name e.g : `itdata-308413.landlordhostifydatabase.stk_listings`
    '''

    full_table_name = '%s.%s.%s' % (GBQ_DATASET_ID, GBQ_PROJECT, table_name)

    # Get latest data(batch_inserted_at) for each api_id 
    if not query:
        query = """
                WITH subq AS (
                SELECT *, MAX(batch_inserted_at) OVER (PARTITION BY api_id)
                as max_date  FROM `%s` 
                )
                SELECT * FROM subq WHERE batch_inserted_at = max_date
        
         """ % full_table_name

    data = pandas.read_gbq(query, project_id=GBQ_DATASET_ID, dialect='standard')
    return data
