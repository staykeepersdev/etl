SELECT
	hda.* ,
	hdh.title AS "title" ,
	pcc."name" AS "city" ,
	pccountry."name" AS "country" ,
	pcr."name" AS "room_type_name" ,
	pcb."name" AS "building_name" ,
	pcl."name" AS "landlord_name" ,
	'airbnb' AS "partner_name",
	pcb.address AS "address"
FROM hostify_data_airbnblisting hda
LEFT JOIN hostify_data_hostifylisting hdh
ON hda.hostify_listing_id = hdh.id
LEFT JOIN properties_core_roomtype pcr
ON hdh.room_type_id = pcr.id
LEFT JOIN properties_core_building pcb
ON pcr.building_id = pcb.id
LEFT JOIN properties_core_landlord pcl
ON pcb.landlord_id = pcl.id
LEFT JOIN properties_core_city pcc
ON pcb.city_id = pcc.id
LEFT JOIN properties_core_country pccountry
ON pcb.country_id = pccountry.id
WHERE hda.status LIKE 'listed';