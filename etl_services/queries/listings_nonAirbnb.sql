SELECT
	cpo.* ,
	pcr."name" AS "room_type_name" ,
	pcb."name" AS "building_name" ,
	pcl."name" AS "landlord_name" ,
	cpp."name" AS "partner_name",
	pcb.address AS "address"
FROM channel_partners_outboundlisting cpo
LEFT JOIN properties_core_roomtype pcr
ON cpo.room_type_id = pcr.id
LEFT JOIN properties_core_building pcb
ON pcr.building_id = pcb.id
LEFT JOIN properties_core_landlord pcl
ON pcb.landlord_id = pcl.id
LEFT JOIN channel_partners_partner cpp
ON cpo.partner_id = cpp.id
WHERE cpo.is_public = TRUE
	AND cpo.status LIKE 'active';