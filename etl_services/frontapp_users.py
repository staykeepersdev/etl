from config.credentials import TABLE_FRONTAPP_USER_TABLE_NAME
from gbq.run_query import push_data
from frontapp.fetch_data import fetch_all_user_data
from frontapp.api_clients.users_api_clients import UserAPIClient
from frontapp.schemas import API_USER_SCHEMA
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info


logger = logging.getLogger(__name__)

USER_API_CLIENT = UserAPIClient()

def import_all_users():
    # USERS
    try:
        user_dataframe, _ = fetch_all_user_data(api_client=USER_API_CLIENT,
                                                  resource_name='frontapp_users',
                                                  schema=API_USER_SCHEMA,
                                                  )
        push_data(user_dataframe,
                  TABLE_FRONTAPP_USER_TABLE_NAME,
                  if_exists='replace'
                )  
        logger_info(logger, 'Data pushed in Frontapp Users')
    except Exception as e:
        logger_error(logger, f"{e}: EXCEPTION OCCURED IN FRONTAPP USERS WHILE PUSHING DATA")
import_all_users()

