from datetime import datetime, timedelta

import pandas as pd

from config.credentials import CBI_DATA_BASE_URL, TABLE_PLENTIFICJOB
from gbq.run_query import push_data
from push_cbi_and_stk_data.call_api import fetch_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info
from cbi_data_to_gbq.schemas import CBI_PLENTIFIC_JOB_SCHEMAS
from hostify.utils import get_schema_columns

logger = logging.getLogger(__name__)

schema = CBI_PLENTIFIC_JOB_SCHEMAS
params = {"date" : datetime.now().date() - timedelta(days=1)}

try:
    data = fetch_data(f'{CBI_DATA_BASE_URL}plentificjob',params)
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI PLENTIFICJOB API ON {params['date']}")

try:
    for obj in data:
        obj['response_data'] = str(obj['response_data'])
        obj['request_data'] = str(obj['request_data'])
        obj['work_order_id'] = str(obj['work_order_id'])
        obj['job_id'] = str(obj['job_id'])

    column_names = get_schema_columns(schema)
    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    plentific_job_data = pd.DataFrame(data, columns=column_names).astype(all_schema)
    plentific_job_data['created_at'] = pd.to_datetime(plentific_job_data["created_at"])
    plentific_job_data['updated_at'] = pd.to_datetime(plentific_job_data["updated_at"])
    plentific_job_data['booking_start_datetime'] = pd.to_datetime(plentific_job_data["booking_start_datetime"])
    plentific_job_data['booking_end_datetime'] = pd.to_datetime(plentific_job_data["booking_end_datetime"])
    if not plentific_job_data.empty:
        push_data(plentific_job_data, TABLE_PLENTIFICJOB,  if_exists="append")
        logger_info(logger, 'Plentificjob data pushed in GBQ')
    else:
        logger_info(logger, 'No Data Found Plentificjob')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI PLENTIFICJOB WHILE PUSHING DATA ON {params['date']}")
