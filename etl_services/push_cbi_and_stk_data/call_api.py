import requests
import json

def fetch_data(url,params):
    response = requests.get(url=url,params=params)     
    return json.loads(response.content)['data']