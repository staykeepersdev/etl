import argparse
import logging
import pandas as pd
from copy import deepcopy

from datetime import date, timedelta, datetime

from pandas import merge
from config.credentials import CHECKINS_TEAM_ID, TEAM_ID
from gbq.run_query import run_query, push_data


logger = logging.getLogger(__name__)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--type')
    return parser


def trim_all_string_columns(df, all_schema):
    """
    Trim leading and trailing whitespaces, and replace \r, \n, \t with empty string
    of each value across all series in dataframe
    """

    def trim_all_columns(df): 
        trim_strings = lambda x: x.strip().replace('\r', '').replace('\n', '')\
        .replace('\t', '') if isinstance(x, str) else x
        return df.applymap(trim_strings)
        
   
    df_obj = df.select_dtypes(['object', 'string'])
    df[df_obj.columns] = trim_all_columns(df_obj)
    
    df_obj = df.select_dtypes(['object', 'string'])
    df[df_obj.columns] = df_obj.replace({'': None}) #np.nan
    df = df.astype(all_schema)
    return df


def new_rows(new_df, source_df):
    """Returns just the rows from the new dataframe that do not exist in the source dataframe"""

    inserted_ids = [y for x in merge(new_df['api_id'], source_df['api_id'], how='left', indicator=True
                                     ).query("_merge == 'left_only'"
                                             ).drop('_merge', 1).values.tolist() for y in x]
    new_insert_data = new_df.loc[new_df['api_id'].isin(inserted_ids)]
    return new_insert_data


def drop_column_from_df(df, column_name):
    df.pop(column_name)
    return df


def deleted_rows(new_df, source_df):

    deleted_ids = [y for x in merge(new_df['api_id'], source_df['api_id'], how='outer', indicator=True
                                    ).query("_merge == 'right_only'"
                                            ).drop('_merge', 1).values.tolist() for y in x]
    return deleted_ids


def fetch_historical_data(target_table, all_schema):
    # Use for brand new tables with 0 rows
    # historical_dataframe = pd.DataFrame([])

    historical_dataframe = run_query(table_name=target_table)
    historical_dataframe.drop(['batch_inserted_at', 'max_date'], 
                              axis=1, inplace=True)
    historical_dataframe = historical_dataframe.astype(all_schema)
    return historical_dataframe


def prepare_push_data(new_dataframe, all_schema, target_table):

    new_dataframe.rename(columns={'id': 'api_id'}, inplace=True)

    if not 'api_id' in all_schema.keys():
        all_schema['api_id'] = all_schema.pop('id')

    new_dataframe = trim_all_string_columns(new_dataframe, all_schema)

    historical_dataframe = fetch_historical_data(target_table, all_schema)
    
    to_push_data = compare_data(target_table,
                                new_dataframe,
                                historical_dataframe)

    if not to_push_data.empty:
        push_data(to_push_data, target_table)
        logger.info(f'{len(to_push_data.index)} data pushed '
                    f'to the table: {target_table}')


def compare_data(target_table, new_dataframe, source_dataframe):
    ''' Compare newly fetched data with historical gbq table to push(append) new 
        and updated data 

        # Listings, Transactions, Reservations - historical data
                Insert new data + updated data --> insert_data
                Insert deleted data(with is_deleted=True) --> deleted_data

    '''

    # These api_id's data exist in related gbq table(source dataframe) -
    # but not exist in new Hostify data - which means inactived or deleted
    deleted_ids = deleted_rows(new_dataframe, source_dataframe)

    # Updates + new data
    insert_data = pd.concat([new_dataframe,
                             source_dataframe,
                             source_dataframe]).drop_duplicates(keep=False)
    '''
    ### FOR DEBUGGING ###
    new_data = new_rows(new_dataframe, source_dataframe)
    print(
        f'{len(new_data.index)} new data will be inserted into {target_table}')

    updates_only = pd.concat(
        [insert_data, new_data, new_data]).drop_duplicates(keep=False)

    print(
        f'{len(updates_only.index)} updated data will be inserted into {target_table}')
    ##############
    '''

    # Create new rows for deleted ones which indicates these are deleted (is_deleted=True )
    deleted_str = ','.join([str(x) for x in deleted_ids])
    if deleted_str:
        inactive_data = source_dataframe.loc[source_dataframe['api_id'].isin(
            deleted_ids)]

        # Filter if the deletion has already taken place
        inactive_data = inactive_data.loc[inactive_data['is_deleted'] == False]
        if not inactive_data.empty:
            inactive_data['is_deleted'] = True
            logger.info(f'api_id:{deleted_str} deleted, new rows '
                        f'will be pushed with the column is_deleted=True')

            insert_data = insert_data.append(inactive_data)

    # new inserts, updated rows and
    # deleted rows with is_deleted=True flag
    if not insert_data.empty:
        '''
        ## DEBUGGING ###
        for i in updates_only['api_id'].tolist()[:min(len(updates_only['api_id'].tolist()),10)]:
            print(insert_data.loc[insert_data['api_id'] == i].values.tolist()[0])
            print(source_dataframe.loc[source_dataframe['api_id'] == i].values.tolist()[0])
            print()
        print('-----------------------------------------------')
        ##############
        '''

        insert_data['batch_inserted_at'] = datetime.utcnow()
        logger.info(f'{len(insert_data.index)} new + updated data will '
                    f'be inserted into {target_table}')
    else:
        logger.info( f'No new/update data to insert {target_table}')
    return insert_data


def get_calendarday_params():
    start_date = date.today() - timedelta(days=1)
    end_date = start_date + timedelta(days=9*30)
    params = {
        'per_page': 1000,
        'start_date':  start_date.strftime("%Y-%m-%d"),
        'end_date': end_date.strftime("%Y-%m-%d")
    }

    return params

def get_tasks_param_list(api_client, params):
    param_list = []
    param_list.append(deepcopy(params))
    data = api_client.search_tasks(params=params)
    while(len(data['data']) == 100):
        created_at_before = data['data'][-1]['created_at']
        params['created_at.before'] = created_at_before
        param_list.append(deepcopy(params))
        data = api_client.search_tasks(params=params)
    return param_list


def get_param(resource_name = None):
    if resource_name == 'asana_ops_tasks':
        params={
            'modified_on':None,
            'teams.any':TEAM_ID,
            'sort_by':'created_at',
            'opt_fields':'gid,name,modified_at,resource_type,workspace,tags,start_on,start_at,followers,permalink_url,projects.name,parent,notes,due_at,due_on,created_at,completed,completed_at,assignee.gid,assignee_status,custom_fields.name,custom_fields.display_value,attachments.gid',
            'created_at.before':None
        }

    if resource_name == 'asana_check_ins':
        params={
            'modified_on':None,
            'teams.any':CHECKINS_TEAM_ID,
            'sort_by':'created_at',
            'opt_fields':'gid,name,modified_at,resource_type,workspace,tags,start_on,start_at,followers,permalink_url,projects.name,parent,notes,due_at,due_on,created_at,completed,completed_at,assignee.gid,assignee_status,custom_fields.name,custom_fields.display_value,attachments.gid',
            'created_at.before':None
        }

    return params