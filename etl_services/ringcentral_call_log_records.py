from datetime import datetime, timezone, timedelta
from config.credentials import TABLE_RINGCENTRAL_CALL_LOG_RECORDS_TABLE_NAME
from gbq.run_query import push_data
from ringcentral.fetch_data import fetch_all_call_log_data
from ringcentral.api_clients.call_log_records import CallLogAPIClient
from ringcentral.schemas import API_RINGCENTRAL_CALL_LOG_SCHEMA
import json
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

CALL_LOG_API_CLIENT = CallLogAPIClient()       

dateFrom = str((datetime.now(timezone.utc)-timedelta(hours=1)).isoformat()).split('+')[0]


def import_all_call_log_records_data():
    try:
      call_log_records_dataframe, _ = fetch_all_call_log_data(api_client=CALL_LOG_API_CLIENT,
                                              resource_name='ringcentral_call_log_records',
                                              schema=API_RINGCENTRAL_CALL_LOG_SCHEMA,
                                              params={
                                                      'perPage':1000,
                                                      'dateFrom': dateFrom,
                                                      }
                                              )
      # if call_log_records_dataframe
      if not call_log_records_dataframe.empty:
        push_data(call_log_records_dataframe,
                  TABLE_RINGCENTRAL_CALL_LOG_RECORDS_TABLE_NAME,
                  if_exists='append'
                  )
        logger_info(logger, 'Data pushed in Ringcentral Call Log Records')
      else:
        logger_info(logger, 'Data Not Found in Ringcentral Call Log Records')
    except Exception as e:
        logger_error(logger, f"{e}: EXCEPTION OCCURED IN RINGCENTRAL CALL LOG RECORDS WHILE PUSHING DATA")

import_all_call_log_records_data()
