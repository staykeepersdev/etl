from datetime import datetime,timedelta
from config.credentials import TABLE_RINGCENTRAL_CALL_PERFORMANCE_TABLE_NAME
from gbq.run_query import push_data
from ringcentral.fetch_data import fetch_all_call_performance_data
from ringcentral.api_clients.call_performance_api_client import CallPerformanceAPIClient
from ringcentral.schemas import API_RINGCENTRAL_CALL_PERFORMANCE_SCHEMA
import json
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)


CALL_PERFORMANCE_API_CLIENT = CallPerformanceAPIClient()

API_BODY_DATA = {
    "grouping": {
        "groupBy": "UserGroups",
    },
    "timeSettings": {
        "timeZone": "Europe/London",
        "timeRange": {
            "timeFrom": (datetime.now()-timedelta(days=1)).replace(hour=00,minute=00,second=00,microsecond=00).isoformat()+'.000Z',
            "timeTo": datetime.now().replace(hour=00,minute=00,second=00,microsecond=00).isoformat()+'.000Z'
        }
    },

    "responseOptions": {
        "counters": {
            "allCalls": {
                "aggregationType": "Sum"
            },
            "callsByDirection": {
                "aggregationType": "Sum"
            },
            "callsByOrigin": {
                "aggregationType": "Sum"
            },
            "callsByResponse": {
                "aggregationType": "Sum"
            },
            "callsSegments": {
                "aggregationType": "Sum"
            },
            "callsByResult": {
                "aggregationType": "Sum"
            },
            "callsByCompanyHours": {
                "aggregationType": "Sum"
            },
            "callsByActions": {
                "aggregationType": "Sum"
            },
            "callsByType": {
                "aggregationType": "Sum"
            }
        },
        "timers": {
            "allCallsDuration": {
                "aggregationType": "Sum"
            },
            "callsDurationByDirection": {
                "aggregationType": "Sum"
            },
            "callsDurationByOrigin": {
                "aggregationType": "Sum"
            },
            "callsDurationByResponse": {
                "aggregationType": "Sum"
            },
            "callsSegmentsDuration": {
                "aggregationType": "Sum"
            },
            "callsDurationByResult": {
                "aggregationType": "Sum"
            },
            "callsDurationByCompanyHours": {
                "aggregationType": "Sum"
            },
            "callsDurationByType": {
                "aggregationType": "Sum"
            }
        }
    }
}


def import_all_call_performance_data():
    # CALL_PERFORMANCE
    try:
        call_performance_dataframe, _ = fetch_all_call_performance_data(api_client=CALL_PERFORMANCE_API_CLIENT,
                                                resource_name='ringcentral_call_performance',
                                                schema=API_RINGCENTRAL_CALL_PERFORMANCE_SCHEMA,
                                                body_data = json.dumps(API_BODY_DATA)
                                                )
        push_data(call_performance_dataframe,
                TABLE_RINGCENTRAL_CALL_PERFORMANCE_TABLE_NAME,
                if_exists='append'
                )
        logger_info(logger, 'Data pushed in Ringcentral Call Performance')
    except Exception as e:
        logger_error(logger, f"{e}: EXCEPTION OCCURED IN RINGCENTRAL CALL PERFORMACE WHILE PUSHING DATA")

import_all_call_performance_data()
