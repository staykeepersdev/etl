import logging
from asana.api_clients.search_api_client import SearchAPIClient
from asana.fetch_data import fetch_asana_checkins_and_tasks
from datetime import datetime, timedelta
from asana.schemas import API_CHECKINS_SCHEMA
from config.credentials import TABLE_ASANA_CHECKINS
from gbq.run_query import push_data
from utils import get_param
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

SEARCH_API_CLIENT = SearchAPIClient()

TODAY = datetime.now().date()
YESTERDAY = TODAY - timedelta(1)

modified_dates =[]

#For yesterday data
modified_dates.append(YESTERDAY.strftime(format="%Y-%m-%d"))

for modified_date in modified_dates:
    params = get_param(resource_name = 'asana_check_ins')
    params['modified_on'] = modified_date
    try:
        dataframe, _ = fetch_asana_checkins_and_tasks(
            SEARCH_API_CLIENT,
            resource_name = 'asana_check_ins',
            schema = API_CHECKINS_SCHEMA,
            params=params
        )

        push_data(
            dataframe,
            TABLE_ASANA_CHECKINS,
            if_exists='append'
            )
        logger_info(logger, f"Data pushed in Asana Checkins")
    except Exception as e:
        logger_error(logger, f"{e}: EXCEPTION OCCURED IN ASANA CHECK-INS WHILE PUSHING DATA ON {YESTERDAY}")
        

