from datetime import datetime, timedelta

import pandas as pd

from config.credentials import CBI_DATA_BASE_URL, TABLE_XEROINVOICE
from gbq.run_query import push_data
from push_cbi_and_stk_data.call_api import fetch_data 
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info
from hostify.utils import get_schema_columns
from cbi_data_to_gbq.schemas import CBI_XEROINVOICE_SCHEMAS


logger = logging.getLogger(__name__)

schema = CBI_XEROINVOICE_SCHEMAS
params = {"date" : datetime.now().date() - timedelta(days=1)}

try:
    data = fetch_data(f'{CBI_DATA_BASE_URL}xeroinvoice',params)
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI XEROINVOICE API ON {params['date']}")

try:
    for obj in data:
        obj['request_data'] = str(obj['request_data'])
        obj['response_data'] = str(obj['response_data'])
        obj['parent_document_id'] = str(obj['parent_document_id'])
        obj['xero_tenant_id'] = str(obj['xero_tenant_id'])
        obj['reservation_id'] = str(obj['reservation_id'])
        obj['stripe_payout_id'] = str(obj['stripe_payout_id'])
        obj['xero_tracking_category_option_id'] = str(obj['xero_tracking_category_option_id'])
   
    column_names = get_schema_columns(schema)
    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    xeroinvoice_data = pd.DataFrame(data, columns=column_names).astype(all_schema)
    xeroinvoice_data['created_at'] = pd.to_datetime(xeroinvoice_data["created_at"])
    xeroinvoice_data['updated_at'] = pd.to_datetime(xeroinvoice_data["updated_at"])
    xeroinvoice_data['check_in'] = pd.to_datetime(xeroinvoice_data["check_in"])
    if not xeroinvoice_data.empty:
        push_data(xeroinvoice_data, TABLE_XEROINVOICE,  if_exists="append")
        logger_info(logger, 'Xeroinvoice Data Pushed in GBQ')
    else:
         logger_info(logger, 'No Data Found in Xeroinvoice')
        
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI XEROINVOICE WHILE PUSHING DATA ON {params['date']}")