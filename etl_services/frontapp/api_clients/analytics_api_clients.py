from .base import BaseAPIClient

class AnalyticsAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='analytics/reports/')

    def get_analytics_data(self,data):
        return self._get_json_response(url=self.list_url(),data=data)
