from .base import BaseAPIClient

class UserAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='teammates')

    def get_user_data(self):
        return self._get_json_response(url=self.list_url())
