import json
import requests
from urllib.parse import urljoin
from config.credentials import FRONTAPP_API_URL,FRONTAPP_HEADER_ACCEPT,FRONTAPP_TOKEN
from datetime import datetime
import time


class BaseAPIClient:

    def __init__(
        self,
        endpoint='None',
        url=FRONTAPP_API_URL,
    ):
        self.endpoint = endpoint
        self.url = url
        self.headers_user = {
            'Authorization': FRONTAPP_TOKEN,
            'Accept':FRONTAPP_HEADER_ACCEPT
        }
        self.headers_analytics = {
            'Authorization': FRONTAPP_TOKEN,
            'Content-Type':FRONTAPP_HEADER_ACCEPT,
            'Accept':FRONTAPP_HEADER_ACCEPT,
        }


    def _get_json_response(self, url: str,params=None,data=None):
        if self.__class__.__name__ == 'UserAPIClient':
            response = requests.get(url=url, headers=self.headers_user, params=params)
            return json.loads(response.content)
        if self.__class__.__name__ == 'AnalyticsAPIClient':
            response = requests.post(url=url, headers=self.headers_analytics,data=data ,params=params)
            return json.loads(response.content)
 
    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def get_list(self, data=None):
        if self.__class__.__name__ == 'UserAPIClient':
            return self._get_json_response(url=self.list_url())['_results']
        if self.__class__.__name__ == 'AnalyticsAPIClient':
            analytics=None
            for i in range(10):
                get_data = self._get_json_response(url=self.list_url(),data=data)
                if "_error" not in get_data.keys():
                    analytics = get_data
                    break
                time.sleep(3)
            return [{'report_uid':analytics['_links']['self'].split('/')[-1],'team_ids':json.loads(data)['filters']['team_ids'][0],'avg_csat_survey_response':analytics['metrics'][0]['value'],'avg_first_response_time':analytics['metrics'][1]['value'],'avg_handle_time':analytics['metrics'][2]['value'],'avg_response_time':analytics['metrics'][3]['value'],'avg_sla_breach_time':analytics['metrics'][4]['value'],'avg_total_reply_time':analytics['metrics'][5]['value'],'num_active_segments_full':analytics['metrics'][6]['value'],'num_archived_segments':analytics['metrics'][7]['value'],'num_archived_segments_with_reply':analytics['metrics'][8]['value'],'num_csat_survey_response':analytics['metrics'][9]['value'],'num_messages_received':analytics['metrics'][10]['value'],'num_messages_sent':analytics['metrics'][11]['value'],'new_segments_count':analytics['metrics'][12]['value'],'num_sla_breach':analytics['metrics'][13]['value'],'pct_csat_survey_satisfaction':analytics['metrics'][14]['value'],'pct_tagged_conversations':analytics['metrics'][15]['value'],'num_open_segments_start':analytics['metrics'][16]['value'],'num_workload_segments':analytics['metrics'][17]['value'],'num_closed_segments':analytics['metrics'][18]['value'],'num_open_segments_end':analytics['metrics'][19]['value'],'start':str(datetime.fromtimestamp(json.loads(data)['start'])),'end':str(datetime.fromtimestamp(json.loads(data)['end']))}]