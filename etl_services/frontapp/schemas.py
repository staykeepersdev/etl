import pandas as pd

API_USER_SCHEMA = {
    
    'id':pd.StringDtype(),
    'email':pd.StringDtype(),
    'username':pd.StringDtype(),
    'first_name':pd.StringDtype(),
    'last_name' :pd.StringDtype(),
    'is_blocked':pd.BooleanDtype()
}

API_ANALYTICS_SCHEMA = {
    'report_uid':pd.StringDtype(),
    'team_ids':int,
    'avg_csat_survey_response':int,
    'avg_first_response_time':float,
    'avg_handle_time':float,
    'avg_response_time':float,
    'avg_sla_breach_time':float,
    'avg_total_reply_time':float,
    'new_segments_count':int,
    'num_active_segments_full':int,
    'num_archived_segments':int,
    'num_archived_segments_with_reply':int,
    'num_csat_survey_response':int,
    'num_messages_received':int,
    'num_messages_sent':int,
    'num_sla_breach':int,
    'pct_csat_survey_satisfaction':float,
    'pct_tagged_conversations':float,
    'num_open_segments_start':int,
    'num_closed_segments':int,
    'num_open_segments_end':int,
    'num_workload_segments':int,
    'date_fields': {
        'start': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},
        'end': {'type': 'date', 'format': '%Y-%m-%d %H:%M:%S'},

    }
}