import sentry_sdk
import logging
import sentry_sdk
from sentry_sdk.integrations.logging import LoggingIntegration

sentry_logging = LoggingIntegration(
    level=logging.INFO,        # Capture info and above as breadcrumbs
    event_level=logging.ERROR,  # Send errors as events
)

ENVIRONMENT = "master"
SANTARY_SDK = sentry_sdk.init(
    dsn="http://49736ae8f07742a29707484e4bd412e2@178.62.8.93:9000/11",
    environment=ENVIRONMENT,
    integrations=[
        sentry_logging,
    ],
    traces_sample_rate=1.0
)