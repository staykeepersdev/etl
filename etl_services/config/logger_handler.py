import logging

def logger_info(logger, e):
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    logger.info(e)

def logger_error(logger, e):
    console = logging.StreamHandler()
    console.setLevel(logging.ERROR)
    logger.error(e)