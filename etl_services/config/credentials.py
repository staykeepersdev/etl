import os
CURR_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
DIR_PATH = os.path.dirname(CURR_DIR_PATH)
# Pipedrive tokem
pipedrive_token = "5213cb520323dc11e6ba7c428dfc0a6408cfd5c1"

# PROPERTIES CORE DATABASE
HOST = "209.97.180.87"
DB_NAME = "staylivegrow"
USER = "read_only_access"
PASSWORD = "read@only@access"
PORT = "5432"

# SLACK WEBHOOK -  etl_errors channel
SLACK_WEBHOOK_ENDPOINT = 'T2Y7B0TE1/B028S0T1DA9/2FisL8GQRWJiVoJcNJqw1qNQ'
SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/'


# HOSTIFY
STK_API_KEY = 'uOHXyOd0uiegVb7BfqT4uq0U6AeZmdOu'
LEAP_SLL_TLL_API_KEY = 'KuNFAC2y1mQd82wd9aJ3adM9Xmgks3ei'
BASE_URL = 'https://pmsapi.staykeepers.co/'


# Google BigQuery
GOOGLE_CLI_CREDS = os.path.join(CURR_DIR_PATH +
                                '/itdata-308413-36f4e74d7495.json')

GBQ_LOCATION = "europe-west2",
GBQ_CHUNK_SIZE = 100000
GBQ_DATASET_ID = 'itdata-308413'
GBQ_PROJECT = 'staging'


# GBQ Tables
TABLE_STK_LISTINGS = 'stk_listings'
TABLE_STK_PARENT_CHILD_LISTINGS = 'stk_parent_child_listings'
TABLE_TLL_SLL_LISTINGS = 'tll_sll_listings'
TABLE_LP_LISTINGS = 'lp_listings'

TABLE_STK_TRANSACTIONS = 'stk_transactions'
TABLE_TLL_SLL_TRANSACTIONS = 'tll_sll_transactions'
#TABLE_LP_TRANSACTIONS = 'z_tmp_lp_transactions'
TABLE_LP_TRANSACTIONS = 'lp_transactions'

TABLE_STK_RESERVATIONS = 'stk_reservations'
#TABLE_TLL_SLL_RESERVATIONS = 'z_tmp_tll_sll_reservations'
TABLE_TLL_SLL_RESERVATIONS = 'tll_sll_reservations'
#TABLE_LP_RESERVATIONS = 'z_tmp_lp_reservations'
TABLE_LP_RESERVATIONS = 'lp_reservations'

TABLE_STK_CALENDARDAYS = 'stk_calendardays'
TABLE_TLL_SLL_CALENDARDAYS = 'tll_sll_calendardays'
TABLE_LP_CALENDARDAYS = 'lp_calendardays'

TABLE_REVIEWS = 'reviews'

#Table_ids
TABLE_ASANA_OPS_TASKS = 'asana_ops_tasks'
TABLE_ASANA_TASK_STORY = 'asana_ops_tasks_story'
TABLE_ASANA_USERS = 'asana_users'
TABLE_ASANA_POSITIVE_REVIEWS = 'asana_positive_guest_reviews'
TABLE_ASANA_NEGATIVE_REVIEWS = 'asana_negative_guest_reviews'
TABLE_ASANA_CHECKINS = 'asana_check_ins'
TABLE_ASANA_GUEST_REQUEST_OR_COMPLAINT = 'asana_guest_requests_or_complaint'

#API gid
TEAM_ID = 1178143868843955
POSITIVE_REVIEW_PROJECT_ID = '1203609855571011'
NEGATIVE_REVIEW_PROJECT_ID = '1203609878405856'
WORKSPACE_ID = 401527272682400
GUEST_REQUESTS_OR_COMPLAINT = 1203609880650049

#Asana API URL and HEADER
ASANA_BASE_URL = 'https://app.asana.com/api/1.0/'
ASANA_AUTHORIZATION_HEADER = 'Bearer 1/1200653458178581:ff4507c124f54697fb13631ae3b08ff7'

CHECKINS_TEAM_ID = '1178143868843951'

#Timedoctor API URL and HEADER for v2
TIMEDOCTOR_BASE_URL='https://api2.timedoctor.com/api/1.0/'
TIMEDOCTOR_HEADER_ACCEPT='application/json'
COMPANY_ID = 'Yv-x6jSts1sSCa3K'

TABLE_TIMEDOCOTR_USERS_TABLE_NAME = 'timedoctor_users'
TABLE_TIMEDOCTOR_WORKLOGS_TABLE_NAME = 'timedoctor_worklogs'
TABLE_TIMEDOCTOR_TASKS_TABLE_NAME = 'timedoctor_tasks'
TABLE_TIMEDOCTOR_TAGS_TABLE_NAME = 'timedoctor_tags'
TABLE_TIMEDOCTOR_PROJECTS_TABLE_NAME = 'timedoctor_projects'

#Rotacloud API URL and HEADER
ROTACLOUD_BASE_URL = 'https://api.rotacloud.com/v1/'
ROTACLOUD_AUTHORIZATION_HEADER = 'Bearer 4unr7yZwvMcOuQf4WIjWyQcoPsJDrOvPj03WKZzVLIF433nZgIafR4RDeF60Hwcg'
TABLE_ROTACLOUD_USERS = 'rotacloud_users' 
TABLE_ROTACLOUD_EMPLOYEE_SHIFTS = "rotacloud_employee_shifts"
TABLE_ROTACLOUD_EMPLOYEE_LEAVES = "rotacloud_employee_leaves"


#RingCentral API URL and HEADER
RINGCENTRAL_API_URL = 'https://platform.ringcentral.com'
RINGCENTRAL_CLIENT_ID_OR_BASIC_AUTH_USERNAME='XJTsNTI6R4mmRmT5ggMJLA'
RINGCENTRAL_CLIENT_SECRET_OR_BASIC_AUTH_PASSWORD='9T7XXnPWShyZI7wAbcc9zgZX8mF6T0QPyrWkxTVPnsHw'
RINGCENTRAL_USERNAME='+442039004150'
RINGCENTRAL_PASSWORD='Inbox123'
RINGCENTRAL_EXTENSION='375'
RINGCENTRAL_GRANT_TYPE='password'
RINGCENTRAL_HEADER_CONTENT_TYPE='application/x-www-form-urlencoded'
RINGCENTRAL_HEADER_ACCEPT='application/json'
TABLE_RINGCENTRAL_USER_TABLE_NAME='ringcentral_users'
TABLE_RINGCENTRAL_CALL_PERFORMANCE_TABLE_NAME='ringcentral_call_performance'
TABLE_RINGCENTRAL_USER_PERFORMANCE_TABLE_NAME='ringcentral_user_performance'
TABLE_RINGCENTRAL_CALL_LOG_RECORDS_TABLE_NAME='ringcentral_call_log'

#Frontapp API URL and HEADER
FRONTAPP_API_URL='https://api2.frontapp.com/'
FRONTAPP_HEADER_ACCEPT='application/json'
FRONTAPP_TOKEN='Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzY29wZXMiOlsicHJpdmF0ZToqIiwic2hhcmVkOioiLCJwcm92aXNpb25pbmciXSwiaWF0IjoxNjE5MDAyMTMyLCJpc3MiOiJmcm9udCIsInN1YiI6ImIxNTFkZjI0NWQwZmU5MGYxYzZmIiwianRpIjoiOTA0ZmVmZWNhMzhkODFkYiJ9.Xc_yUHaNZlNIJJ0B45ovGYSzgI68-0lFe9gQqvBa5F8'



TABLE_FRONTAPP_USER_TABLE_NAME = 'frontapp_users'
TABLE_FRONTAPP_ANALYTICS_TABLE_NAME = 'frontapp_analytics'


#cbi_data API URL 
CBI_DATA_BASE_URL = 'https://www.api.inbox.staylivegrow.com/api/v1/etl/'


TABLE_PLENTIFICJOB = 'plentificjob' 
TABLE_PLENTIFICWORKORDER = 'plentificworkorder' 
TABLE_HOSTIFYRESERVATION = 'hostifyreservation' 
TABLE_XEROINVOICE = 'xeroinvoice'


#STK_data API URL
STK_BASE_URL = "https://www.api.homes.staykeepers.com/api/v1/onboarding/etl/"


TABLE_STK_UNIT_TABLE_NAME = "unit"
TABLE_STK_PLENTIFIC_PROPERTY_TABLE_NAME = "plentificproperty"