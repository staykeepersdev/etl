'''
    INSERT
    Insert active & inactive listings' calendarday data 
    This code does not compare if new calendarday data exist in gbq tables and
    pushes every insert and updates.
    
    GBQ script will be removing duplicate calendar day data from the tables

'''
import logging

from config.credentials import (TABLE_LP_CALENDARDAYS,
                                TABLE_STK_CALENDARDAYS, 
                                TABLE_TLL_SLL_CALENDARDAYS,
                                TABLE_STK_LISTINGS,
                                TABLE_TLL_SLL_LISTINGS,
                                TABLE_LP_LISTINGS)

from gbq.run_query import run_query, push_data
from hostify.api_clients.calendar_api_client import CalendarAPIClient
from hostify.fetch_data import fetch_all_calendarday_data
from hostify.schemas import API_CALENDARDAY_SCHEMA
from slack.utils import exception_catcher
from utils import get_calendarday_params, parse_args


logger = logging.getLogger(__name__)
STK_CALENDAR_API_CLIENT = CalendarAPIClient('Staykeepers')
LEAP_SLL_TLL_CALENDAR_API_CLIENT = CalendarAPIClient()


@exception_catcher
def import_stk_calendardays():
    """STK_CALENDARDAY ETL """

    logger.info('STK Calendarday ETL Process started')

    stk_gbq_df = run_query(TABLE_STK_LISTINGS)  
    listings = list(set(stk_gbq_df['api_id'].values.tolist()))
    
    date_params = get_calendarday_params()
    params_with_listing_id = [
        {'listing_id': listing_id, **date_params} for listing_id in listings]

    logger.info('STK Calendarday - Fetched all Hostify Data')

    stk_dataframe = fetch_all_calendarday_data(api_client=STK_CALENDAR_API_CLIENT,
                                               resource_name='calendar',
                                               schema=API_CALENDARDAY_SCHEMA,
                                               custom_fields_schema={},
                                               params=params_with_listing_id
                                               )

    stk_dataframe.rename(columns={'id': 'api_id'}, inplace=True)

    logger.info('STK Calendarday - Fetched all Hostify Data')

    push_data(stk_dataframe,
              TABLE_STK_CALENDARDAYS
            )

    logger.info('STK Calendarday ETL Process completed')


@exception_catcher
def import_lp_calendardays():
    """LEAP_CALENDARDAY ETL """

    logger.info('LEAP Calendarday ETL Process started')

    lp_gbq_df = run_query(TABLE_LP_LISTINGS)

    listings = list(set(lp_gbq_df['api_id'].values.tolist()))

    date_params = get_calendarday_params()
    params_with_listing_id = [
        {'listing_id': listing_id, **date_params} for listing_id in listings]

    leap_dataframe = fetch_all_calendarday_data(api_client=LEAP_SLL_TLL_CALENDAR_API_CLIENT,
                                                resource_name='calendar',
                                                schema=API_CALENDARDAY_SCHEMA,
                                                custom_fields_schema={},
                                                params=params_with_listing_id
                                                )
    
    leap_dataframe.rename(columns={'id': 'api_id'}, inplace=True)

    logger.info('LEAP Calendarday - Fetched all Hostify Data')

    # LEAP
    push_data(leap_dataframe,
              TABLE_LP_CALENDARDAYS
              )

    logger.info('LEAP Calendarday ETL Process completed')


@exception_catcher
def import_tll_sll_calendardays():
    """SLL_TLL_CALENDARDAY ETL """

    logger.info('TLL_SLL Calendarday ETL Process started')

    tll_sll_gbq_df = run_query(TABLE_TLL_SLL_LISTINGS)

    listings = list(set(tll_sll_gbq_df['api_id'].values.tolist()))

    date_params = get_calendarday_params()
    params_with_listing_id = [
        {'listing_id': listing_id, **date_params} for listing_id in listings]

    tll_sll_dataframe = fetch_all_calendarday_data(api_client=LEAP_SLL_TLL_CALENDAR_API_CLIENT,
                                                   resource_name='calendar',
                                                   schema=API_CALENDARDAY_SCHEMA,
                                                   custom_fields_schema={},
                                                   params=params_with_listing_id
                                                   )

    tll_sll_dataframe.rename(columns={'id': 'api_id'}, inplace=True)
    
    logger.info('TLL_SLL Calendarday - Fetched all Hostify Data')

    # TLL_SLL
    push_data(tll_sll_dataframe,
             TABLE_TLL_SLL_CALENDARDAYS
            )

    logger.info('TLL_SLL Calendarday ETL Process completed')


if __name__ == '__main__':
    parser = parse_args()
    args = parser.parse_args()
    func_call = args.__dict__['type']
    logging.basicConfig(filename='/var/log/etl', level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    if func_call == 'stk':
        import_stk_calendardays()
    elif func_call == 'leap':
        import_lp_calendardays()
    elif func_call == 'tll_sll':
        import_tll_sll_calendardays()
