from datetime import datetime, timedelta

import pandas as pd

from config.credentials import CBI_DATA_BASE_URL, TABLE_PLENTIFICWORKORDER
from gbq.run_query import push_data
from push_cbi_and_stk_data.call_api import fetch_data
from hostify.utils import get_schema_columns
from cbi_data_to_gbq.schemas import CBI_PLENTIFIC_WORKORDER_SCHEMAS
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

schema = CBI_PLENTIFIC_WORKORDER_SCHEMAS
params = {"date" : datetime.now().date() - timedelta(days=1)}

try:
    data = fetch_data(f'{CBI_DATA_BASE_URL}plentificworkorder',params)
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI PLENTIFIC WORKORDER API ON {params['date']}")
    
try:
    for obj in data:
        obj['request_data'] = str(obj['request_data'])
        obj['response_data'] = str(obj['response_data'])
        obj['activity'] = str(obj['activity'])
        obj['hostify_reservation_id'] = str(obj['hostify_reservation_id'])
        obj['category_id'] = str(obj['category_id'])

    column_names = get_schema_columns(schema)
    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    plentific_workorder = pd.DataFrame(data, columns=column_names).astype(all_schema)   
    plentific_workorder['created_at'] = pd.to_datetime(plentific_workorder["created_at"])
    plentific_workorder['updated_at'] = pd.to_datetime(plentific_workorder["updated_at"])
    if not plentific_workorder.empty:
        push_data(plentific_workorder, TABLE_PLENTIFICWORKORDER,  if_exists="append")
        logger_info(logger, 'Plentificworkorder Data Pushed Successfully in GBQ ')
    else:
        logger_info(logger, 'No Data Found in Plentificworkorder')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI PLENTIFIC WORKORDER WHILE PUSHING DATA ON {params['date']}")
