from config.credentials import TABLE_FRONTAPP_ANALYTICS_TABLE_NAME
from gbq.run_query import push_data
from frontapp.fetch_data import fetch_all_analytics_data
from frontapp.api_clients.analytics_api_clients import AnalyticsAPIClient
from frontapp.schemas import API_ANALYTICS_SCHEMA
from datetime import datetime, timedelta
import json
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

USER_API_CLIENT = AnalyticsAPIClient()

def start_end_date():
    now = datetime.now() 
    yesterday = datetime.now() - timedelta(1)
    previous_start = datetime( yesterday.year, yesterday.month, yesterday.day ).timestamp()
    previous_end = datetime( now.year, now.month, now.day ).timestamp()
    return {'start':int(previous_start),'end':int(previous_end)}

START_END_PARAMS = start_end_date()


API_BODY_DATA={
    "start": START_END_PARAMS['start'],
    "end": START_END_PARAMS['end'],
    "filters": {
        "team_ids": [
            "1275336"
        ]
    },
    "metrics": [
        "avg_csat_survey_response",
        "avg_first_response_time",
        "avg_handle_time",
        "avg_response_time",
        "avg_sla_breach_time",
        "avg_total_reply_time",
        "new_segments_count",
        "num_active_segments_full",
        "num_archived_segments",
        "num_archived_segments_with_reply",
        "num_csat_survey_response",
        "num_messages_received",
        "num_messages_sent",
        "num_sla_breach",
        "pct_csat_survey_satisfaction",
        "pct_tagged_conversations",
        "num_open_segments_start",
        "num_closed_segments",
        "num_open_segments_end",
        "num_workload_segments"
    ]
}


def import_all_analytics():
    # ANALYTICS
    try:
        analytics_dataframe, _ = fetch_all_analytics_data(api_client=USER_API_CLIENT,
                                                resource_name='frontapp_analytics',
                                                schema=API_ANALYTICS_SCHEMA,
                                                body_data=json.dumps(API_BODY_DATA)
                                                )
        push_data(analytics_dataframe,
                TABLE_FRONTAPP_ANALYTICS_TABLE_NAME,
                if_exists='append'
                )
        logger_info(logger, 'Data pushed in Frontapp Analytics')
    except Exception as e:
        logger_error(logger, f"{e}: EXCEPTION OCCURED IN FRONTAPP ANALYTICS WHILE PUSHING DATA")
import_all_analytics()