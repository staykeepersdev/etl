''' INSERT ONLY (append table) - KEEP HISTORICAL DATA
    Insert new and updated listings data as new row to keep data history.
'''
import logging
import pandas
import sys

from config.credentials  import TABLE_STK_PARENT_CHILD_LISTINGS

from hostify.api_clients.listing_api_client import ListingAPIClient
from hostify.fetch_data import fetch_all_hostify_data, fetch_listings_children_data

from hostify.schemas import API_PARENT_LISTING_SCHEMA, API_CHILD_LISTING_SCHEMA, API_PARENT_CHILD_LISTING_SCHEMA
from slack.utils import exception_catcher
from utils import prepare_push_data

logger = logging.getLogger(__name__)
STK_LISTING_API_CLIENT = ListingAPIClient('Staykeepers')

LOG_FILE_LOCATION = './etl.log' # '/var/log/etl'

@exception_catcher
def fetch_stk_parent_child_listings():
    """STK_PARENT_CHILD_LISTINGS ETL"""

    # Fetch Parents
    stk_parents_df, all_schema = fetch_all_hostify_data(
        api_client=STK_LISTING_API_CLIENT,
        resource_name='listings',
        schema=API_PARENT_LISTING_SCHEMA,
        custom_fields_schema=None,
        params={'per_page': 100} # 1000
    )
    renamed_parent_columns = {column: f'parent_{column}' for column in stk_parents_df.keys()}
    stk_parents_df.rename(columns=renamed_parent_columns, inplace=True)

    # Fetch Children
    stk_children_df = fetch_listings_children_data(
        stk_parents_df,
        STK_LISTING_API_CLIENT.get_listing_children,
        schema=API_CHILD_LISTING_SCHEMA,
    )
    renamed_child_columns = {column: f'child_{column}' for column in stk_children_df.keys() if column != 'parent_id'}
    stk_children_df.rename(columns=renamed_child_columns, inplace=True)

    # Merge Parents + Children
    stk_parents_children_df = pandas.merge(
        stk_parents_df,
        stk_children_df,
        on='parent_id',
    )
    # stk_parent_children_columns = {**renamed_parent_columns, **renamed_child_columns}

    return stk_parents_children_df


if __name__ == '__main__':
    logging.basicConfig(filename=LOG_FILE_LOCATION, level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

    logger.info('STK Parent->Child Listings')
    stk_parents_children_df = fetch_stk_parent_child_listings()

    # Required by the prepare_push_data(...)
    stk_parents_children_df.rename(columns={'child_id': 'api_id'}, inplace=True)
    API_PARENT_CHILD_LISTING_SCHEMA['api_id'] = API_PARENT_CHILD_LISTING_SCHEMA.pop('child_id')

    stk_parents_children_df.insert(len(stk_parents_children_df.columns), 'is_deleted', False)

    prepare_push_data(
        stk_parents_children_df,
        API_PARENT_CHILD_LISTING_SCHEMA,
        TABLE_STK_PARENT_CHILD_LISTINGS,
    )