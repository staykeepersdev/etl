from datetime import datetime, timedelta

import pandas as pd

from config.credentials import CBI_DATA_BASE_URL, TABLE_HOSTIFYRESERVATION
from gbq.run_query import push_data
from push_cbi_and_stk_data.call_api import fetch_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info
from cbi_data_to_gbq.schemas import CBI_HOSTIFY_RESERVATIONS_SCHEMAS
from hostify.utils import get_schema_columns


logger = logging.getLogger(__name__)

schema = CBI_HOSTIFY_RESERVATIONS_SCHEMAS
params = {"date" : datetime.now().date() - timedelta(days=1)}

try:
    data = fetch_data(f'{CBI_DATA_BASE_URL}hostifyreservation',params)
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI HOSTIFYRESERVATIONS API ON {params['date']}")

try:
    for obj in data:
        obj['work_orders_applied'] = str(obj['work_orders_applied'])
        obj['sms_sent'] = str(obj['sms_sent'])
        obj['data'] = str(obj['data'])
        obj['hostify_id'] = str(obj['hostify_id'])
        obj['hostify_guest_id'] = str(obj['hostify_guest_id'])
        obj['hostify_listing_id'] = str(obj['hostify_listing_id'])

    column_names = get_schema_columns(schema)
    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    hostifyreservation_df = pd.DataFrame(data, columns=column_names).astype(all_schema)
    hostifyreservation_df['created_at'] = pd.to_datetime(hostifyreservation_df["created_at"])
    hostifyreservation_df['updated_at'] = pd.to_datetime(hostifyreservation_df["updated_at"])
    if not hostifyreservation_df.empty:
        push_data(hostifyreservation_df, TABLE_HOSTIFYRESERVATION, if_exists="append")
        logger_info(logger, 'Hostifyreservations Data Pushed in GBQ')
    else:
        logger_info(logger, 'No Data Found in Hostifyreservations')
    
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN CBI HOSTIFYRESERVATIONS WHILE PUSHING DATA ON {params['date']}")
