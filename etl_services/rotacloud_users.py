from rotacloud.fetch_data import fetch_all_rotacloud_users
from rotacloud.api_clients.rotacloud_users_api_client import RotacloudUsersAPIClient
from rotacloud.schemas import API_ROTACLOUD_USERS_SCHEMA
from gbq.run_query import push_data
from config.credentials import TABLE_ROTACLOUD_USERS
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)


ROTACLOUD_USERS_API_CLIENT = RotacloudUsersAPIClient()
try:
    dataframe, _ = fetch_all_rotacloud_users(
        ROTACLOUD_USERS_API_CLIENT,
        resource_name = 'rotacloud_users',
        schema = API_ROTACLOUD_USERS_SCHEMA
    )

    push_data(
        dataframe,
        TABLE_ROTACLOUD_USERS,
        if_exists='replace'
        )
    logger_info(logger, 'Data pushed in Rotacloud Users')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ROTACLOUD USERS WHILE PUSHING DATA")   

