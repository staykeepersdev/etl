from config.credentials import TABLE_TIMEDOCTOR_TAGS_TABLE_NAME,COMPANY_ID
from gbq.run_query import push_data
from timedoctor.fetch_data import fetch_all_timdoctor_tags_data
from timedoctor.api_clients.tags_api_client import TagsAPIClient
from timedoctor.schemas import API_TIMEDOCTOR_TAGS_SCHEMA
from timedoctor.api_clients.base import ACCESS_AND_REFRESH_TOKEN
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

TAGS_API_CLIENT = TagsAPIClient()

try:
  tags_dataframe, _ = fetch_all_timdoctor_tags_data(api_client=TAGS_API_CLIENT,
                                          resource_name='tags_data',
                                          schema=API_TIMEDOCTOR_TAGS_SCHEMA,
                                          params={
                                            'token':ACCESS_AND_REFRESH_TOKEN,
                                            'company':COMPANY_ID, 
                                          }
                                          )

  push_data(tags_dataframe,
          TABLE_TIMEDOCTOR_TAGS_TABLE_NAME,
          if_exists='replace'
          )
  logger_info(logger, 'Tags data pushed in GBQ')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN TIMEDOCTOR TAGS WHILE PUSHING DATA")