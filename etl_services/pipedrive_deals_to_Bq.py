#!/usr/bin/env python
"""
Description: Connects to Pipedrive deals endpoint and extracts all columns into Google bigquery
"""
__author___ = "Pedro Magalhães"
__email__ = "pedro@wininnkepeers.net"
__status__ = "PROD"
__version__ = "1.0.0"

"""
Importing relevant libraries
"""
import logging
import argparse
import pandas_gbq
import json
import requests
import pandas as pd
import numpy as np
import math
from datetime import datetime as dt
from config.credentials import (pipedrive_token)
from google.oauth2 import service_account
from config.credentials import (GBQ_DATASET_ID,
                                GBQ_CHUNK_SIZE,
                                GBQ_PROJECT,
                                GBQ_LOCATION,
                                GOOGLE_CLI_CREDS) 

"""
Building connection to services
"""

# Building google bigquery connection using service account. Defines env variable
GOOGLE_CLIENT_CREDENTIALS = service_account.Credentials.from_service_account_file(
    filename=GOOGLE_CLI_CREDS
)
pandas_gbq.context.credentials = GOOGLE_CLIENT_CREDENTIALS
pandas_gbq.context.project = GBQ_DATASET_ID

# init logger
logger = logging.getLogger(__name__)



"""
Fetching data from api request, parse and clean results
"""

def query_size():
    """
    Calculate the number of pages

    Returns:
    number pages  [int]: number of pages to loop over for api request
    """
    
    url =  "https://api.pipedrive.com/v1/deals/summary?api_token=" + pipedrive_token
    request = requests.get(url)
    size = json.loads(request.content)["data"]["total_count"]
    number_pages = math.ceil( size / 500 )
    
    return number_pages


def api_request(number_pages):
    
    """
    Extracts and transforms data from pipedrive

    Input:
    number pages  [int]: number of pages to loop over for api request
    
    Returns:
    data [df]: dataframe containing all data
    """
    
    # Loop api request
    df_results = pd.DataFrame()

    for i in range(number_pages + 1):
        url = "https://api.pipedrive.com/v1/deals?api_token=" + pipedrive_token +   "&limit=500&start=" + str(i)
        
        response = requests.get(url)
        data = pd.json_normalize((response.json())["data"])
        df_results = df_results.append(data, ignore_index=True)
        print("Finished index", i, "at", dt.now())
        
    # convert to dataframe and rename    
    data = pd.DataFrame(df_results)
    data = data.rename(columns ={
        "837d6779870cadbee9f83abd963f6c38ca6be2d0":"source","631b319d30ac9cf9560818047ddc32c7300a85cd":"los","3012afb7a46cb27542c40267247671ad7eedc6d6_formatted_address": "address","lost_time":"lost_time", 
        "first_won_time":"first_won_time", 
        "won_time":"won_time", 
        "add_time":"created_time", "9898ffe382c18203bd32aae1463c3f8bb8ecb2ed":"Conversion_link", "3012afb7a46cb27542c40267247671ad7eedc6d6_admin_area_level_1":"Region", "3012afb7a46cb27542c40267247671ad7eedc6d6_admin_area_level_2":"City", "3012afb7a46cb27542c40267247671ad7eedc6d6_country":"Country", "3f8aa053d84f3a83242ed0c9155c59cfa59a3d10":"Landlord","094983ca03ef940d7a22f3dd8a6c5a72d7b8eb67":"CheckIn","094983ca03ef940d7a22f3dd8a6c5a72d7b8eb67_until":"CheckOut",
        "person_id.name":"name", 
        "3012afb7a46cb27542c40267247671ad7eedc6d6":"Property_Address",
        "3012afb7a46cb27542c40267247671ad7eedc6d6_subpremise":"Property_Address_subpremise", 
        "3012afb7a46cb27542c40267247671ad7eedc6d6_street_number":"Property_street_number",
        "3012afb7a46cb27542c40267247671ad7eedc6d6_route":"Property_route",
        "3012afb7a46cb27542c40267247671ad7eedc6d6_sublocality":"Property_sublocality",
        "3012afb7a46cb27542c40267247671ad7eedc6d6_locality":"Property_locality",
        "3012afb7a46cb27542c40267247671ad7eedc6d6_postal_code":"Property_postal_code",
        "17cce94b969fb60614bf3f90af67210d289635a0":"Bo_Client",
        "69ab67ab721aea9fdde7673f723dc4b1cda91b5d_currency":"BO_Currency",
        "9898ffe382c18203bd32aae1463c3f8bb8ecb2ed": "BO_conversation_link",
        "97c04ec10344a24dc05f6c37a21afbb403bc3bff": "Preferred_comms_chanel",
        "dfb4c2fd00bc46eef822a6d5bf9193b5742aa88a": "customer_channel",
        "317776fca8f7f04538b4809c532a251c3d305676": "BO_website",
        "c4eeba18c65a3619839c01b86d168ad78fe826f8": "Property_type",
        "2ba42491d36cd9bd975a999ce6ed8fea138954a4": "Guest_Mobile",
        "1f8c84f61dc045d22f458dfb302405083bba6cf4": "email_stage",
        "a3998230d0937b61908502ed9a95a571ad82397d": "Guest_conversation_link",
        "171c0a7803eee0218a123b1bb0bf98c238290ead": "City_of_the_property",
        "3b2576516f137a9a5033d22ec9d930da1f24a3a5": "Building_name",
        "5f5324216433bfe5d231c1df300afbd841b1a6bf": "Room_type",
        "148cfc02760bbbd5f43179da4fbe70356b6c3bcf": "Created_by",
        "b00a497645cde0f6f613717ca702a4f1a205af86": "offer_presentation",
        "cce81be9be0056671f5ac6f914d15b6fd308b75e": "response_status",
        "332d7baede7d1809ce8b0f1d846bb941d0ff7350": "offer_status",
        "7138af1adc35b042789533edc824d0c04a8f4986": "Contract_status",
        "6026f21b431a3733b61cc4f6530aac6001fc773f": "Prepare_handover",
        "739fc9588f22b0ad6d92c5b9ce00299f5cfd25a8": "Contract_attached",
        "e3886744bfa78cabef82174f46d593294531ae86": "Projections_shorlets_only",
        "624851876183fc5b81344409da3d29145c40c8b2": "Presentation_Deck_Attached",
        "cd56aa853f0a2312520c4b14ade44c8db290744e": "Long_Lets_Standard_Flow",
        "522b6b424764a767a4bd1b9d74a054bcdf05a880": "AirBnB_Flow",
        "06ab7da0f2178a79b8024cfbbb06a9cc0be532ea": "Follow_Up_CaseStudy",
        "280abb7ff052499b2deae119ab91a09ef4264a98": "Email_Offer_Status",
        "cd882771df21898391f6870ec60247c69fa9d903": "Prepare_Summary_Email",
        "2a70eb88815ffe066fcebfe68fe7fcd706640528": "Prep_Email1_Additional_info_about_students",
        "630b68162b29bc3b09498d60059db7344115309f": "Prep_Email2_STK_More_Information",
        "f70b8878f2df2c3dd3a15ca21bc4b4755b02987d": "Deal_Source",
        "dd36ef2a9c44101bbd998519ad96caadbb2a6219": "Academic_year",
        "e7ee20df535052eddf3da910384934b1af5966f3": "Long_Le_Signed_Contract_Link",
        "3bcb19adc39182502c65fe0eca8a67bba8ff5323": "Short_Let_Signed_Contract_Link",
        "45597623a2f50458207986341bbd808c4a87bcac": "Long_Let_Client",
        "28cb85a4ffab70cd051b4d6d3c280c0215406450": "Permission_to_Advertise_Date",
        "e2ed44c44d9fbc425af68b131c53784da7abef8d": "Date_when_the_Onboarding_Information_was_Collected",
        "c67c5ebae03dc236f6b6c49a5de43ae168bee6c6": "Number_of_Buildings_to_Advertise_On_Long_Let",
        "69ab67ab721aea9fdde7673f723dc4b1cda91b5d":"other"    
        })

    # renames rows
    conditions = [
        data["los"] == "282",
        data["los"] == "281",
        data["los"] == "325"
    ]

    values = ["SLL", "TLL", "TLL"]

    data["los_label"] = np.select(conditions,values)

    conditions = [
        data["source"] == "15",
        data["source"] == "16",
        data["source"] == "605"
    ]

    values = ["On the Market", "Airbnb", "Zoopla"]

    data["Source_name"] = np.select(conditions, values, default = "Missing Source")
    
    data.columns = data.columns.str.replace("\.","_")

    return data

"""
Upload to BigQuery
"""

def upload_bq(data, table_name):
    """uploads dataframe to bigquery

    Args:
        data (df): dataframe to be uploaded
        table_name (str): dataset and name of table
    """
    
    data.to_gbq(
        destination_table=table_name,
        project_id=GBQ_DATASET_ID,
        location=GBQ_LOCATION,
        chunksize=GBQ_CHUNK_SIZE,
        if_exists="replace"
    )



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--table_name", "-tn",help="BigQuery Table name in the form project_name.table_name", type=str)
    args = parser.parse_args()
    
    table_name = args.table_name
    
    logging.basicConfig(filename='/var/log/etl', level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    
    number_pages = query_size()
    data = api_request(number_pages)
    upload_bq(data, table_name)
    exit(0)
    