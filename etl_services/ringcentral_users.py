from config.credentials import TABLE_RINGCENTRAL_USER_TABLE_NAME
from gbq.run_query import push_data
from ringcentral.fetch_data import fetch_all_user_data
from ringcentral.api_clients.users_api_client import UserAPIClient
from ringcentral.schemas import API_RINGCENTRAL_USERS_SCHEMA
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

USER_API_CLIENT = UserAPIClient()

def import_all_users():
    # USERS
    try:
      user_dataframe, _ = fetch_all_user_data(api_client=USER_API_CLIENT,
                                                resource_name='ringcentral_users',
                                                schema=API_RINGCENTRAL_USERS_SCHEMA,
                                                params={'count':300}
                                                )
      push_data(user_dataframe,
                TABLE_RINGCENTRAL_USER_TABLE_NAME,
                if_exists='replace'
              )  
      logger_info(logger, 'Data pushed in Ringcentral Users')
    except Exception as e:
      logger_error(logger, f"{e}: EXCEPTION OCCURED IN RINGCENTRAL USERS WHILE PUSHING DATA")
import_all_users()

