#!/usr/bin/env python
"""
Description: Connects to Properties Core and runs a Given SQL query which is uploaded into Google bigquery
"""
__author___ = "Pedro Magalhães"
__email__ = "pedro@wininnkepeers.net"
__status__ = "live"
__version__ = "1.1.0"

"""
Importing relevant libraries
"""
import logging
import argparse
import pandas_gbq
import pandas.io.sql as sqlio
import psycopg2
from config.credentials import (HOST, DB_NAME, USER, PASSWORD, PORT)
from google.oauth2 import service_account
from config.credentials import (GBQ_DATASET_ID,
                                GBQ_CHUNK_SIZE,
                                GBQ_PROJECT,
                                GBQ_LOCATION,
                                GOOGLE_CLI_CREDS)

"""
Building connection to services
"""

# Building google bigquery connection using service account. Defines env variable
GOOGLE_CLIENT_CREDENTIALS = service_account.Credentials.from_service_account_file(
    filename=GOOGLE_CLI_CREDS
)
pandas_gbq.context.credentials = GOOGLE_CLIENT_CREDENTIALS
pandas_gbq.context.project = GBQ_DATASET_ID

# init logger
logger = logging.getLogger(__name__)


def connect_to_propertycore():
    # Connection to postgresql database
    logger.info("Attempting connection to Property core")

    try:
        db_conn = psycopg2.connect(host=HOST,
                                   dbname=DB_NAME,
                                   user=USER,
                                   password=PASSWORD,
                                   port=PORT)
        logger.info("Successfully connected to Property Core")
        return db_conn
    except ConnectionError:
        logger.exception("Connection exception when trying to Property Core review credentials")
        db_conn == "fail"
        return db_conn


"""
Run Query and upload to BigQuery
"""


def query_ingestion(sql_query_path, table_name, conn):
    """
    Run a SQL query against property core and loads result into Google Bigquery

    :param sql_query_path: string path to sql query
    :param table_name
    :param connection (default = conn)
    """

    # returns sql query from file
    with open(sql_query_path, "r") as file:
        sql_query = file.read()
    df = sqlio.read_sql_query(sql_query, conn)

    df.to_gbq(
        destination_table=table_name,
        project_id=GBQ_DATASET_ID,
        location=GBQ_LOCATION,
        chunksize=GBQ_CHUNK_SIZE,
        if_exists="replace"
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--sql_query_path", "-sql_p", help="Path to SQL query file", type=str)
    parser.add_argument("--table_name", "-tn", help="BigQuery Table name in the form project_name.table_name", type=str)
    args = parser.parse_args()

    query = args.sql_query_path
    table_name = args.table_name

    logging.basicConfig(filename='/var/log/etl', level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

    # Connecting to property core
    db = connect_to_propertycore()

    if db == "fail":
        exit(-1)
        logger.info("Failed on fetching data from Property Core and upload to BigQuery")
    else:
        query_ingestion(query, table_name, db)
        # Closes connection and exit message
        db.close()
        exit(0)
