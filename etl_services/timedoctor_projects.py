from config.credentials import TABLE_TIMEDOCTOR_PROJECTS_TABLE_NAME,COMPANY_ID
from gbq.run_query import push_data
from timedoctor.fetch_data import fetch_all_timdoctor_projects_data
from timedoctor.api_clients.projects_api_client import ProjectsAPIClient
from timedoctor.schemas import API_TIMEDOCTOR_PROJECTS_SCHEMA
from timedoctor.api_clients.base import ACCESS_AND_REFRESH_TOKEN
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

PROJECTS_API_CLIENT = ProjectsAPIClient()

try:
  project_dataframe, _ = fetch_all_timdoctor_projects_data(api_client=PROJECTS_API_CLIENT,
                                            resource_name='projects_data',
                                            schema=API_TIMEDOCTOR_PROJECTS_SCHEMA,
                                            params={
                                              'token':ACCESS_AND_REFRESH_TOKEN,
                                              'company':COMPANY_ID, 
                                              'all':'true',
                                              'show-integration':'true'
                                            }
                                            )
  push_data(project_dataframe,
          TABLE_TIMEDOCTOR_PROJECTS_TABLE_NAME,
          if_exists='replace'
          )
  logger_info(logger, 'Projects data pushed in GBQ')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN TIMEDOCTOR PROJECTS WHILE PUSHING DATA")