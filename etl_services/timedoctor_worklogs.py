from config.credentials import TABLE_TIMEDOCTOR_WORKLOGS_TABLE_NAME,COMPANY_ID
from gbq.run_query import push_data
from timedoctor.fetch_data import fetch_all_timdoctor_worklogs_data
from timedoctor.api_clients.worklogs_api_client import WorklogsAPIClient
from timedoctor.schemas import API_TIMEDOCTOR_WORKLOGS_SCHEMA
from datetime import datetime,timedelta
from timedoctor.api_clients.users_api_client import UserAPIClient
from timedoctor.api_clients.base import ACCESS_AND_REFRESH_TOKEN
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)
WORKLOGS_API_CLIENT = WorklogsAPIClient()
USER_API_CLIENT = UserAPIClient()

USER_ID_LIST = USER_API_CLIENT.get_user_list({
                                            'token':ACCESS_AND_REFRESH_TOKEN,
                                            'company':COMPANY_ID, 
                                            'limit':500,
                                          })

def start_end_dat():
  format = '%Y-%m-%d'
  start = datetime.strptime(str(datetime.now().date()), format).date()
  end = start - timedelta(days=1)
  return start,end


start,end = start_end_dat()
try:
  worklogs_dataframe, _ = fetch_all_timdoctor_worklogs_data(api_client=WORKLOGS_API_CLIENT,
                                            resource_name='worklogs_data',
                                            schema=API_TIMEDOCTOR_WORKLOGS_SCHEMA,
                                            params={
                                              'token':ACCESS_AND_REFRESH_TOKEN,
                                              'company':COMPANY_ID, 
                                              'user':USER_ID_LIST,
                                              'from':end,
                                              'to':start,
                                              'detail':'true',
                                              'task-project-names':'true'
                                            }
                                            ) 
  push_data(worklogs_dataframe,
            TABLE_TIMEDOCTOR_WORKLOGS_TABLE_NAME,
            if_exists='append'
          )
  logger_info(logger, 'Worklogs data pushed in GBQ')
except Exception as e:
  logger_error(logger, f"{e}: EXCEPTION OCCURED IN TIMEDOCTOR WORKLOGS WHILE PUSHING DATA")