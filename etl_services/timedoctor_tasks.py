from config.credentials import TABLE_TIMEDOCTOR_TASKS_TABLE_NAME,COMPANY_ID
from gbq.run_query import push_data
from timedoctor.fetch_data import fetch_all_timdoctor_tasks_data
from timedoctor.api_clients.tasks_api_client import TasksAPIClient
from timedoctor.schemas import API_TIMEDOCTOR_TASKS_SCHEMA
from timedoctor.api_clients.base import ACCESS_AND_REFRESH_TOKEN
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)


TASKS_API_CLIENT = TasksAPIClient()


try:
  tasks_dataframe, _ = fetch_all_timdoctor_tasks_data(api_client=TASKS_API_CLIENT,
                                            resource_name='tasks_data',
                                            schema=API_TIMEDOCTOR_TASKS_SCHEMA,
                                            params={
                                              'token':ACCESS_AND_REFRESH_TOKEN,
                                              'company':COMPANY_ID, 
                                            }
                                            )
  push_data(tasks_dataframe,
          TABLE_TIMEDOCTOR_TASKS_TABLE_NAME,
          if_exists='replace'
          )
  logger_info(logger, 'Tasks data pushed in GBQ')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN TIMEDOCTOR TASKS WHILE PUSHING DATA")