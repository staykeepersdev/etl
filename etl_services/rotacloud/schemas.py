import pandas as pd

API_ROTACLOUD_USERS_SCHEMA = {
    'id':int,
    'email':pd.StringDtype(),
    'first_name':pd.StringDtype(),
    'last_name':pd.StringDtype(),
    'deleted':pd.BooleanDtype(),
    'deleted_at':pd.StringDtype(),
    'created_at':int,
}

API_ROTACLOUD_EMPLOYEE_SHIFTS_SCHEMA = {
    'id':int,
    'user':pd.StringDtype(),
    'deleted':pd.BooleanDtype(),
    'deleted_at':pd.StringDtype(), 
    'published':pd.BooleanDtype(),
    'open':pd.BooleanDtype(),
    'location':int,
    'role':pd.StringDtype(),
    'minutes_break':int,
    'notes':pd.StringDtype(),
    'created_by':int,
    'updated_at':pd.StringDtype(),
    'updated_by':pd.StringDtype(),
    'claimed':pd.BooleanDtype(),
    'claimed_at':pd.StringDtype(),
    'unavailability_requests':pd.StringDtype(),
    'swap_requests':pd.StringDtype(),
    'acknowledged':pd.BooleanDtype(),
    'acknowledged_at':pd.StringDtype(),
    'date_fields': {
        'start_time': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'end_time': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},

    }

}
API_ROTACLOUD_EMPLOYEE_LEAVES_SCHEMA={
    'id':int,
    'deleted':pd.StringDtype(),
    'deleted_at':pd.StringDtype(),
    'type':int,
    'paid':pd.StringDtype(),
    'user':int,
    'status':pd.StringDtype(),
    'user_message':pd.StringDtype(),
    'start_am_pm':pd.StringDtype(),
    'end_am_pm':pd.StringDtype(),
    'dates':pd.StringDtype(),
    'date_fields': {
        'start_date': {'type': 'date', 'format': '%Y-%m-%d'},
        'end_date': {'type': 'date', 'format': '%Y-%m-%d'},
    }
}
