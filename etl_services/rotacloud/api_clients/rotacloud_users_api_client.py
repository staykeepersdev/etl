from .base import BaseAPIClient

class RotacloudUsersAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='users/')

    def get_user_data(self):
        return self._get_json_response(url=self.list_url())

    