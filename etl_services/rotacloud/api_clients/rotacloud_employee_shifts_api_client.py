from .base import BaseAPIClient

class RotacloudEmployeeShiftsAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='shifts')

    def get_employee_shifts_data(self,params):
        return self._get_json_response(url=self.list_url(),params=params)

