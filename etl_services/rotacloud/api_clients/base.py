import json
import requests
from urllib.parse import urljoin
from config.credentials import ROTACLOUD_BASE_URL,ROTACLOUD_AUTHORIZATION_HEADER
from datetime import datetime
class BaseAPIClient:

    def __init__(
        self,
        endpoint=None,
        url=ROTACLOUD_BASE_URL,
    ):
        self.endpoint = endpoint
        self.url = url
        self.headers = {
            'Authorization': ROTACLOUD_AUTHORIZATION_HEADER
        }

    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers, params=params)
        return json.loads(response.content)
 
    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def get_list(self, params=None):
        if self.__class__.__name__ == 'RotacloudUsersAPIClient':
            return self._get_json_response(url=self.list_url(),params=params)
        if self.__class__.__name__ == 'RotacloudEmployeeShiftsAPIClient':
            return [{'id':shifts['id'],'user':shifts['user'],'deleted':shifts['deleted'],'deleted_at':shifts['deleted_at'],'published':shifts['published'],'open':shifts['open'],'location':shifts['location'],'role':shifts['role'],'minutes_break':shifts['minutes_break'],'notes':shifts['notes'],'created_by':shifts['created_by'],'updated_at':shifts['updated_at'],'updated_by':shifts['updated_by'],'claimed':shifts['claimed'],'claimed_at':shifts['claimed_at'],'unavailability_requests':shifts['unavailability_requests'],'swap_requests':shifts['swap_requests'],'acknowledged':shifts['acknowledged'],'acknowledged_at':shifts['acknowledged_at'],'start_time':str(datetime.fromtimestamp(shifts['start_time'])),'end_time':str(datetime.fromtimestamp(shifts['end_time'])),'created_at':str(datetime.fromtimestamp(shifts['created_at']))} for shifts in self._get_json_response(url=self.list_url(),params=params)]
        if self.__class__.__name__ == 'RotacloudEmployeeLeavesAPIClient':
            return [{'id':leaves['id'],'deleted':leaves['deleted'],'deleted_at':leaves['deleted_at'],'type':leaves['type'],'paid':leaves['paid'],'user':leaves['user'],'status':leaves['status'],'user_message':leaves['user_message'],'start_am_pm':leaves['start_am_pm'],'end_am_pm':leaves['end_am_pm'],'dates':leaves['dates'],'start_date':leaves['start_date'],'end_date':leaves['end_date']} for leaves in self._get_json_response(url=self.list_url(),params=params) ]