from .base import BaseAPIClient

class RotacloudEmployeeLeavesAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='leave')

    def get_employee_leaves_data(self):
        return self._get_json_response(url=self.list_url())

