from datetime import datetime, timedelta

import pandas as pd

from config.credentials import (STK_BASE_URL,
                                TABLE_STK_PLENTIFIC_PROPERTY_TABLE_NAME)
from gbq.run_query import push_data
from push_cbi_and_stk_data.call_api import fetch_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info
from cbi_data_to_gbq.schemas import STK_PLENTIFIC_PROPERTY_SCHEMAS
from hostify.utils import get_schema_columns

logger = logging.getLogger(__name__)

schema = STK_PLENTIFIC_PROPERTY_SCHEMAS
params = {"date" : datetime.now().date() - timedelta(days=1)}

try:
    data = fetch_data(f'{STK_BASE_URL}plentificproperty',params)
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN STK PLENTIFIC PROPERTY API ON {params['date']}")

try:
    if data:
        for obj in data:
            obj['response_data'] = str(obj['response_data'])
            obj['request_data'] = str(obj['request_data'])
            obj['unit_id'] = str(obj['unit_id'])
        column_names = get_schema_columns(schema)
        all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
        plentificproperty_df = pd.DataFrame(data,columns=column_names).astype(all_schema)
        plentificproperty_df['created_at'] = pd.to_datetime(plentificproperty_df["created_at"])
        plentificproperty_df['updated_at'] = pd.to_datetime(plentificproperty_df["updated_at"])
        plentificproperty_df['created_by'] = pd.to_datetime(plentificproperty_df["created_by"])
        plentificproperty_df['updated_by'] = pd.to_datetime(plentificproperty_df["updated_by"])
        
        if not plentificproperty_df.empty:
            push_data(plentificproperty_df, TABLE_STK_PLENTIFIC_PROPERTY_TABLE_NAME,  if_exists="append")
            logger_info(logger, 'Plentificproperty Data Pushed in GBQ')
        else:
            logger_info(logger, 'No Data Found in Plentificproperty')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN STK PLENTIFIC PROPERTY WHILE PUSHING DATA ON {params['date']}")