
import logging
from functools import wraps
from slack.utils import parse_push_error

logger = logging.getLogger(__name__)


def retry(times):
    """
    Decorator to retry any functions 'times' times.
    """
    def retry_decorator(func):
        @wraps(func)
        def retried_function(*args, **kwargs):
            for i in range(times - 1):
                try:
                    return func(*args, **kwargs)
                    
                except Exception as e:  
                    print('retry ex', e)
                    pass
            func(*args, **kwargs)

        return retried_function

    return retry_decorator


def raise_api_conn_err(err_msg, api_client, params):
    if err_msg:
        api_client_dict = api_client.__dict__
        connection_params = {**{'url': api_client_dict.get('url'),
                                'endpoint': api_client_dict.get('endpoint')},
                             **params}

        parse_push_error('Hostify API Call',
                         ConnectionError(f'Failed to establish a new connection: {err_msg} \n'
                              f'Connection params: {connection_params}'))
         
        logger.error(f'Hostify API call failed. params: {connection_params}')


def get_schema_columns(schema, custom=False):
    if custom:
        date_columns = schema.get('date_fields')
        date_columns = {f'custom_field_{k}': v for k,
                        v in date_columns.items()}
        column_names = [
            f'custom_field_{k}' for k in schema.keys() if k != 'date_fields']

    else:
        date_columns = schema.get('date_fields')
        column_names = [k for k in schema.keys() if k != 'date_fields']

    if date_columns:
        column_names.extend(list(date_columns.keys()))

    return column_names