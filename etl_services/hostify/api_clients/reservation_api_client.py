from .base import BaseAPIClient


class ReservationAPIClient(BaseAPIClient):
    def __init__(self,  account='LeapSllTll'):
        super().__init__(endpoint='reservations/', account=account)

    def get_reservations(self, params=None):
        return self.get_list(params=params)
