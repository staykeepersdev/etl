from .base import BaseAPIClient

class ReviewAPIClient(BaseAPIClient):
    def __init__(self,  account='LeapSllTll'):
        super().__init__(endpoint='reviews/', account=account)