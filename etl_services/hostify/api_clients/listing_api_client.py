from urllib.parse import urljoin

from .base import BaseAPIClient


class ListingAPIClient(BaseAPIClient):
    def __init__(self,  account='LeapSllTll'):
        super().__init__(endpoint='listings/', account=account)
    
    def listings(self, page=1, per_page=100, service_pms=None):
        self.endpoint = f'listings?page={page}&per_page={per_page}'
        
        if service_pms == True:  self.endpoint += f'&service_pms=1'
        if service_pms == False: self.endpoint += f'&service_pms=0'
        
        return self.get_list()

    def get_listing(self, listing_id, params):
        self.endpoint = f'listings/'
        return self.get_item(listing_id, params)
    
    def get_listing_children(self, identifier: int, params=None):
        url = urljoin(self.list_url(), 'children/' + str(identifier))
        return self._get_json_response(url=url, params=params)
    
    
    # Custom fields related
    # @link https://app.hostify.com/docs#set-values-custom-fields
    def set_custom_fields_values(self, data):
        self.endpoint = 'custom_fields/set_values'
        return self.post_item(data=data)
