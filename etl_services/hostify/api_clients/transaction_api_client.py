from .base import BaseAPIClient

class TransactionAPIClient(BaseAPIClient):
    def __init__(self,  account='LeapSllTll'):
        super().__init__(endpoint='transactions/', account=account)