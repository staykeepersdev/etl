from .base import BaseAPIClient


class CalendarAPIClient(BaseAPIClient):
    def __init__(self, account='LeapSllTll'):
        super().__init__(endpoint='calendar/', account=account)

    def get_listing_days(self, params=None):
        days_data = self.get_list(params=params)
        return days_data
