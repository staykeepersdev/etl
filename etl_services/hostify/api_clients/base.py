import json
import requests
import traceback

from urllib.parse import urljoin

from config.credentials import LEAP_SLL_TLL_API_KEY, STK_API_KEY, BASE_URL

class BaseAPIClient:

    def __init__(
        self,
        endpoint=None,
        url=BASE_URL,
        account='LeapSllTll'
    ):
        if account == 'LeapSllTll':
            self.api_key = LEAP_SLL_TLL_API_KEY
        elif account == 'Staykeepers':
            self.api_key = STK_API_KEY
        self.endpoint = endpoint
        self.url = url
        self.headers = {
            'x-api-key': self.api_key
        }

    def _get_json_response(self, url: str, params=None):
        response = requests.get(url=url, headers=self.headers, params=params)
        return json.loads(response.content)

    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def detail_url(self, identifier: int):
        return urljoin(self.list_url(), str(identifier))

    def get_list(self, params=None):
        url = self.list_url()
        return self._get_json_response(url=url, params=params)

    def get_item(self, identifier: int, params=None):
        url = self.detail_url(identifier=identifier)
        return self._get_json_response(url=url, params=params)

    def post_item(self, data=None):       
        url = self.list_url()
        return self._post_json_response(url=url, data=data)
    
    def _post_json_response(self, url: str, data=None):

        try:
            response = requests.post(url=url, headers=self.headers, json=data)
            return json.loads(response.content)

        except Exception as e:
            raise Exception(
                f'Exception: {str(e)} \n'
                f'Traceback: {traceback.format_exc()} \n'
                f'Response: {response.content}'
            )
