import pandas as pd


# Custom Fields Documentation : https://staykeepers.co/customer/custom_fields

API_TRANSACTION_SCHEMA = {
    'id': int,
    'type': pd.StringDtype(),
    'amount': float,
    'source': pd.StringDtype(),
    'added_by': float,
    'currency': pd.StringDtype(),
    'guest_id': int,
    'guest_name': pd.StringDtype(),
    'charge_type': pd.StringDtype(),
    'payout_type': int,
    'is_completed': int,
    'charge_status': pd.StringDtype(),
    'details': pd.StringDtype(),
    'reservation_id': int,
    'integration_name': pd.StringDtype(),
    'listing_nickname': pd.StringDtype(),
    'type_description': pd.StringDtype(),
    'channel_listing_id': pd.StringDtype(), #TODO type channel_listing_id should be Int64Dtype but we have string data
    'integration_nickname': pd.StringDtype(),
    'channel_reservation_id': pd.StringDtype(),
    'channel_transaction_id': pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'charge_date': {'type': 'date', 'format': '%Y-%m-%d'},
        'arrival_date': {'type': 'date', 'format': '%Y-%m-%d'}
    }
}


API_PARENT_LISTING_SCHEMA = {
    'id': pd.StringDtype(), # Parent ID
    'nickname': pd.StringDtype(),

    'price_markup': float,
    'cleaning_fee': float,
    
    # There are problems with the DF when these are int:
    # ValueError: Cannot convert non-finite values (NA or inf) to integer
    'min_nights': float,
    'max_nights': float,
}

API_CHILD_LISTING_SCHEMA = {
    'id': pd.StringDtype(), # Child ID
    'parent_id': pd.StringDtype(), # Parent ID
    'channel_listing_id': pd.StringDtype(), # Airbnb ID

    'price_markup': float,
    'extra_person': float,
    'cleaning_fee': float,

    # There are problems with the DF when these are int:
    # ValueError: Cannot convert non-finite values (NA or inf) to integer
    'min_nights': float,
    'max_nights': float,
    'min_notice_hours': float,
    'max_notice_days': float,

    'checkin_start': pd.StringDtype(),
    'checkin_end': pd.StringDtype(),
    'checkout': pd.StringDtype(),

    'cancel_policy': pd.StringDtype(),
    'instant_booking': pd.StringDtype(),
}
API_PARENT_CHILD_LISTING_SCHEMA = {
    **{f'parent_{key}': val for key, val in API_PARENT_LISTING_SCHEMA.items()},
    **{f'child_{key}': val for key, val in API_CHILD_LISTING_SCHEMA.items() if key != 'parent_id'},
}


API_LISTING_SCHEMA = {
    'id': int,
    'name': pd.StringDtype(),
    'nickname': pd.StringDtype(),
    'cleaning_fee': float,       
    'room_type': float, #pd.Int64Dtype()   
    'bedrooms': float, #pd.Int64Dtype()
    'beds': float, #pd.Int64Dtype()
    'cancel_policy': float, #pd.Int64Dtype()
    'integration_id': int,
    'default_daily_price': float,
    'fs_integration_type': float, #pd.Int64Dtype()
    'lat': pd.StringDtype(),
    'lng': pd.StringDtype(),
    'city': pd.StringDtype(),
    'state': pd.StringDtype(),
    'country': pd.StringDtype(),
    'currency': pd.StringDtype(),
    'timezone': pd.StringDtype(),
    'channel_listing_id': pd.StringDtype(), #TODO type channel_listing_id should be Int64Dtype as documentation suggest but we have string data
    'address': pd.StringDtype(),
    'integration_status': pd.StringDtype(),   
    'integration_nickname': pd.StringDtype(), 
    'street': pd.StringDtype(),
    'zipcode': pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'}
    }
}



API_LISTING_STK_CUSTOM_FIELDS = {
    '478': float, #pd.Int64Dtype()
    '671': pd.StringDtype(),
    '672': pd.StringDtype(),
    'date_fields': {
        '676': {'type': 'datetime', 'format': '%m/%d/%Y'}
    }
}

API_LISTING_LP_CUSTOM_FIELDS = {
    '723': pd.StringDtype(),
    '724': pd.StringDtype(),
    'date_fields': {
    }
}


API_CALENDARDAY_SCHEMA = {
    'id': int,
    'currency': pd.StringDtype(),
    'reservation_id': float, #pd.Int64Dtype()
    'listing_id': int,
    'status': pd.StringDtype(),
    'base_price': float,
    'price': float,
    'date_fields': {
        'date': {'type': 'date', 'format': '%Y-%m-%d'}
    }
}


API_RESERVATION_SCHEMA = {
    'id': int,
    'guests': float, #pd.Int64Dtype()
    'nights': int,
    'source': pd.StringDtype(),
    'status': pd.StringDtype(),
    'revenue': float,
    'currency': pd.StringDtype(),
    'guest_id': int,
    'subtotal': float,
    'base_price': float,
    'guest_name': pd.StringDtype(),
    'listing_id': int,
    'tax_amount': float,
    'net_revenue': float,
    'sum_refunds': float,
    'addons_price': float,
    'advance_days': float, #pd.Int64Dtype()
    'cleaning_fee': float,
    'extras_price': float,
    'payout_price': float,
    'cancel_policy': float, #pd.Int64Dtype()
    'listing_title': pd.StringDtype(),
    'owner_revenue': float,
    'integration_id': int,
    'security_price': float,
    'service_charge': float,
    'extras_price_ex': float,
    'price_per_night': float,
    'transaction_fee': float,
    'cancellation_fee': float,
    'extras_price_inc': float,
    'integration_name': pd.StringDtype(),
    'integration_type': float, #pd.Int64Dtype()
    'listing_nickname': pd.StringDtype(),
    'confirmation_code': pd.StringDtype(),
    'parent_listing_id': float, #pd.Int64Dtype()
    'channel_commission': float,
    'listing_channel_id': pd.StringDtype(),
    'integration_nickname': pd.StringDtype(),
    'channel_reservation_id': pd.StringDtype(),
    'payout_by_channel': float, #pd.Int64Dtype()
    'paid_sum': float,
    'paid_part': pd.StringDtype(),
    'owner_commission': float,
    'extra_guest_price': float,
    'notes': pd.StringDtype(),
    'guest_details': pd.StringDtype(),
    'date_fields': {
        'updated_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'created_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'cancelled_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'checkIn': {'type': 'datetime', 'format': '%Y-%m-%d'},
        'checkOut': {'type': 'datetime', 'format': '%Y-%m-%d'},
        'confirmed_at': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'},
        'planned_arrival': {'type': 'date', 'format': '%H:%M:%S'}
    }

}

API_RESERVATION_STK_CUSTOM_FIELDS = {
    '478': float, #pd.Int64Dtype()
    '485': pd.StringDtype(),
    '487': float,
    '488': float,
    '489': float,
    '490': float,
    '493': float,
    '494': float,
    '495': float,
    '496': float,
    '498': float,
    '536': float,
    '579': float,
    '479': float,
    '673': pd.StringDtype(),
    '620': pd.StringDtype(),
    '491': pd.StringDtype(),
    '544': pd.StringDtype(),
    '671': pd.StringDtype(),
    '672': pd.StringDtype(),
    '734': pd.StringDtype(),
    '736': pd.StringDtype(),
    'date_fields': {
        '564': {'type': 'date', 'format': '%Y-%m-%d'},
    }
}

API_RESERVATION_LP_CUSTOM_FIELDS = {
    '683': pd.StringDtype(),
    '684': float,  
    '685': float,  
    '686': float,  
    '687': float,  
    '688': float, #pd.Int64Dtype()
    '689': float,  
    '690': float,  
    '691': float,  
    '692': float,  
    '693': float,  
    '694': pd.StringDtype(),
    '695': float,  
    '696': pd.StringDtype(),
    '698': float,  
    '699': pd.StringDtype(),
    '700': pd.StringDtype(),
    '701': pd.StringDtype(),
    '702': float,  
    '703': float, #pd.Int64Dtype()
    '704': float,  
    '705': pd.StringDtype(),
    '706': float, 
    '707': float, 
    '708': pd.StringDtype(),
    '709': pd.StringDtype(),
    '710': pd.StringDtype(),
    '711': pd.StringDtype(),
    '712': float,  
    '713': float, #pd.Int64Dtype()
    '714': pd.StringDtype(),
    '715': pd.StringDtype(),
    '722': float, #pd.Int64Dtype()
    '727': pd.StringDtype(),
    '728': float, #pd.Int64Dtype()
    '697': pd.StringDtype(),
    'date_fields': {
      #'697': {'type': 'date', 'format': '%Y-%m-%d'} # TODO https://staykeepers.co/reservations/view/808785 audit date is not in date format
    } 
}


API_REVIEW_SCHEMA = {
    'id': int,
    'channel_review_id': int,
    'comments': pd.StringDtype(),
    'feedback': pd.StringDtype(),
    'accuracy_comments': pd.StringDtype(),
    'checkin_comments': pd.StringDtype(),
    'clean_comments': pd.StringDtype(),
    'communication_comments': pd.StringDtype(),
    'improve_comments':	 pd.StringDtype(),
    'location_comments': pd.StringDtype(),
    'value_comments': pd.StringDtype(),
    'rating': float,
    'accuracy_rating':	float,
    'checkin_rating':	float,
    'clean_rating':	float,
    'communication_rating':	float,
    'location_rating':	float,
    'value_rating': float,
    'parent_listing_id': float, #pd.Int64Dtype()
    'listing_id': int,
    'reservation_id': int,
    'guest_id': int,
    'integration_id': int,
    'date_fields': {
        'created': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'}
    }
}
