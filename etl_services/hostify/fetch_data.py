import logging
import math
import pandas
import pytz

import concurrent.futures
from datetime import datetime

from .utils import raise_api_conn_err, get_schema_columns, retry

logger = logging.getLogger(__name__)

DEFAULT_MAX_WORKERS = 10

def fetch_all_hostify_data(api_client,
                           resource_name: str,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Hostify Data"""
    resources = []

    # @FIXME THIS PART IS NOT GENERIC ENOUGH
    # The page count is collected via the Base Class get_list() method
    # The resource_name should be used instead and this should be extracted to new f
    data = api_client.get_list(params={'page': 1, 'per_page': 1}) or {}
    if not data['success']:
        raise_api_conn_err(data.get('error'), api_client, params)

    total_item = data.get('total')
    per_page = params['per_page']
    total_loop_count = math.ceil(total_item / per_page) + 1

    logger.info(
        f'Total {total_item} item / per_page {per_page}: {total_loop_count}')

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names += get_schema_columns(custom_fields_schema, custom=True)

    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client, page, params,
            resource_name, column_names,
            schema, custom_fields_schema): page for page in range(1, total_loop_count)}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())

    logger.info(f'Collected all {resource_name} data from Hostify')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})

    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
    df.insert(len(df.columns), 'is_deleted', False)
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema


def fetch_all_calendarday_data(api_client,
                               resource_name: str,
                               params=[],
                               schema={},
                               custom_fields_schema={},
                               ):
    """Fetching Calendarday Hostify Data"""
    resources = []

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names += get_schema_columns(custom_fields_schema, custom=True)

    logger.info(f'Hostify API calls will be started to collect all'
                f'calendarday data of {len(params)} listings')

    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client, 1, param,
            resource_name, column_names,
            schema, custom_fields_schema): param for param in params}

        for future in concurrent.futures.as_completed(responses):
            try:
                resources.extend(future.result())
            except Exception as e:
                logger.error(e)
                pass

    logger.info(f'Collected all calendarday data from Hostify')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})

    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
    df.insert(len(df.columns), 'batch_inserted_at', datetime.utcnow())

    logger.info(f'Dataframe of calendar created with new data')
    return df


@retry(3)
# @FIXME URL PATH PARAMETERS NOT SUPPORTED
# ONLY POST Body params supported
def api_call_per_page(api_client, page, params,
                      resource_name,
                      column_names,
                      schema,
                      custom_fields_schema):
    resources = []
    page_params = {'page': page}
    params = {**params, **page_params} if params else page_params

    data = api_client.get_list(params=params)
    if data['success']:
        resource = data[resource_name]
        for row in resource:
            # Convert custom fields datetime columns string to datetime
            if custom_fields_schema:
                custom_fields = row.get('custom_fields')
                row.update({f"custom_field_{field.get('id')}": field.get(
                    'value') for field in custom_fields})

                for key, val in custom_fields_schema['date_fields'].items():
                    timestamp = datetime.strptime(row[f"custom_field_{key}"], val['format']
                                                  ).replace(tzinfo=pytz.utc
                                                            ) if row.get(f"custom_field_{key}") else None

                    row[f"custom_field_{key}"] = timestamp

            # Convert Datetime columns string to datetime
            date_fields = schema.get('date_fields')
            if date_fields:
                for key, val in date_fields.items():
                    timestamp = datetime.strptime(row[key], val['format']
                                                  ).replace(tzinfo=pytz.utc
                                                            ) if row[key] else None
                    row[key] = timestamp

            # Fix for: some float columns have empty string data. This is required to cast float type.
            new_row = [[None if value == '' else value for value in list(
                map(row.get, column_names))]]
            resources.extend(new_row)

    else:
        err_msg = data.get('error')
        raise_api_conn_err(err_msg, api_client, params)
    return resources


def fetch_listings_children_data(stk_parent_df, func, schema={}):
    parent_ids = list(stk_parent_df.parent_id)

    resources = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        futures = {executor.submit(func, parent_id): parent_id for parent_id in parent_ids}

        for future in concurrent.futures.as_completed(futures):
            parent_id = futures[future]
            try:
                response = future.result()
                if not response['success']:
                    continue

                for listing in response['listings']:
                    listing['parent_id'] = parent_id
                    resources.append(listing)
            except Exception as e:
                logger.error(e)
                pass

    return pandas.DataFrame(resources, columns=list(schema.keys()))