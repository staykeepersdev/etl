''' INSERT ONLY (append table) - KEEP HISTORICAL DATA
    Insert new and updated reservation data as new rows to keep data history.
'''
import logging

from config.credentials import (TABLE_STK_RESERVATIONS,
                                TABLE_LP_RESERVATIONS, 
                                TABLE_TLL_SLL_RESERVATIONS
                                )

from hostify.api_clients.reservation_api_client import ReservationAPIClient
from hostify.fetch_data import fetch_all_hostify_data
from hostify.schemas import (
    API_RESERVATION_SCHEMA,
    API_RESERVATION_STK_CUSTOM_FIELDS,
    API_RESERVATION_LP_CUSTOM_FIELDS
)
from slack.utils import exception_catcher
from utils import prepare_push_data, parse_args


logger = logging.getLogger(__name__)
STK_RESERVATION_API_CLIENT = ReservationAPIClient('Staykeepers')
LEAP_SLL_TLL_RESERVATION_API_CLIENT = ReservationAPIClient()


@exception_catcher
def import_stk_reservations():
    """STK_RESERVATION ETL """

    logger.info('STK Reservations ETL Process started')

    stk_dataframe, all_schema = fetch_all_hostify_data(api_client=STK_RESERVATION_API_CLIENT,
                                           resource_name='reservations',
                                           schema=API_RESERVATION_SCHEMA,
                                           custom_fields_schema=API_RESERVATION_STK_CUSTOM_FIELDS,
                                           params={
                                               'per_page': 1000}
                                           )
    logger.info('STK Reservations - Fetched all Hostify Data')

    prepare_push_data(
        stk_dataframe,
        all_schema,
        TABLE_STK_RESERVATIONS
    )
    logger.info('STK Reservations ETL Process completed')
    

@exception_catcher
def import_leap_reservations():
    """LEAP_SLL_TLL_RESERVATION ETL """

    logger.info('LEAP_SLL_TLL Reservations ETL Process started')

    lp_sll_tll_df, all_schema = fetch_all_hostify_data(api_client=LEAP_SLL_TLL_RESERVATION_API_CLIENT,
                                           resource_name='reservations',
                                           schema={**API_RESERVATION_SCHEMA},
                                           custom_fields_schema=API_RESERVATION_LP_CUSTOM_FIELDS,
                                           params={'per_page': 1000}
                                           )

    logger.info('LEAP_SLL_TLL Reservations - Fetched all Hostify Data')
  
    # LP prefix
    leap_dataframe = lp_sll_tll_df[
                        lp_sll_tll_df['listing_nickname'].str.startswith('LP')   
                        ]

    # LEAP
    prepare_push_data(leap_dataframe,
                      all_schema,
                      TABLE_LP_RESERVATIONS
                      ) 

    logger.info('LP Reservations ETL Process completed')

    # ('SLL_', 'TLL_', 'TTL_')
    tll_sll_dataframe = lp_sll_tll_df[lp_sll_tll_df['listing_nickname'].str.startswith(
        ('SLL_', 'TLL_', 'TTL_'))]

    # TLL_SLL
    prepare_push_data(tll_sll_dataframe,
                      all_schema,
                      TABLE_TLL_SLL_RESERVATIONS
                    )

    logger.info('TLL_SLL Reservations ETL Process completed')


if __name__ == '__main__':
    parser = parse_args()
    args = parser.parse_args()
    func_call = args.__dict__['type']
    logging.basicConfig(filename='/var/log/etl', level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    if func_call == 'stk':
        import_stk_reservations()
    elif func_call == 'leap':
        import_leap_reservations()
