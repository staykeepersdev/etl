''' INSERT ONLY (append table) - KEEP HISTORICAL DATA
    Insert new and updated transaction data as new row to keep data history.
'''

import logging

from config.credentials import (TABLE_STK_TRANSACTIONS,
                                TABLE_LP_TRANSACTIONS, 
                                TABLE_TLL_SLL_TRANSACTIONS)

from hostify.api_clients.transaction_api_client import TransactionAPIClient
from hostify.fetch_data import fetch_all_hostify_data
from hostify.schemas import API_TRANSACTION_SCHEMA
from slack.utils import exception_catcher
from utils import prepare_push_data, parse_args


logger = logging.getLogger(__name__)
STK_TRANSACTION_API_CLIENT = TransactionAPIClient('Staykeepers')
LEAP_SLL_TLL_TRANSACTION_API_CLIENT = TransactionAPIClient()


@exception_catcher
def import_stk_transactions():
    """STK_TRANSACTION ETL """

    logger.info('STK Transactions ETL Process started')

    stk_dataframe, all_schema = fetch_all_hostify_data(api_client=STK_TRANSACTION_API_CLIENT,
                                           resource_name='transactions',
                                           schema=API_TRANSACTION_SCHEMA,
                                           custom_fields_schema={},
                                           params={
                                               'per_page': 1000}
                                           )

    logger.info('STK Transactions - Fetched all Hostify Data')

    prepare_push_data(
        stk_dataframe,
        all_schema,
        TABLE_STK_TRANSACTIONS
    )

    logger.info('STK Transactions ETL Process completed')


@exception_catcher
def import_leap_transactions():
    """LEAP_SLL_TLL_TRANSACTION ETL """

    logger.info('LEAP_SLL_TLL Transactions ETL Process started')

    lp_sll_tll_df, all_schema = fetch_all_hostify_data(api_client=LEAP_SLL_TLL_TRANSACTION_API_CLIENT,
                                           resource_name='transactions',
                                           schema={**API_TRANSACTION_SCHEMA},
                                           custom_fields_schema={},
                                           params={
                                               'per_page': 1000}
                                           )
    logger.info('LEAP_SLL_TLL Transactions - Fetched all Hostify Data')

    # ('SLL_', 'TLL_', 'TTL_')
    tll_sll_dataframe = lp_sll_tll_df[lp_sll_tll_df['listing_nickname'].str.startswith(
        ('SLL_', 'TLL_', 'TTL_'))]

    # TLL_SLL
    prepare_push_data(tll_sll_dataframe,
                      all_schema,
                      TABLE_TLL_SLL_TRANSACTIONS
                      )

    logger.info('TLL_SLL Transactions ETL Process completed')

    # LP prefix
    leap_dataframe = lp_sll_tll_df[lp_sll_tll_df['listing_nickname'].str.startswith(
        'LP')]

    # LEAP
    prepare_push_data(leap_dataframe,
                      all_schema,
                      TABLE_LP_TRANSACTIONS
                      )

    logger.info('LP Transactions ETL Process completed')


if __name__ == '__main__':
    parser = parse_args()
    args = parser.parse_args()
    func_call = args.__dict__['type']
    logging.basicConfig(filename='/var/log/etl', level=logging.INFO,
                        format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
    if func_call == 'stk':
        import_stk_transactions()
    elif func_call == 'leap':
        import_leap_transactions()
