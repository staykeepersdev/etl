import logging
from asana.fetch_data import fetch_all_asana_data
from asana.api_clients.task_api_client import TaskAPIClient
from config.credentials import TABLE_ASANA_POSITIVE_REVIEWS, POSITIVE_REVIEW_PROJECT_ID
from gbq.run_query import push_data
from asana.schemas import API_POSITIVE_REVIEW_SCHEMA
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info

logger = logging.getLogger(__name__)

TASK_API_CLIENT = TaskAPIClient()

try:
    dataframe, _ = fetch_all_asana_data(
        TASK_API_CLIENT,
        resource_name = 'asana_positive_guest_reviews',
        schema = API_POSITIVE_REVIEW_SCHEMA,
        project_id = POSITIVE_REVIEW_PROJECT_ID,
        params={'opt_pretty':'true', 'opt_expand':"assignee"}
    )

    push_data(
        dataframe,
        TABLE_ASANA_POSITIVE_REVIEWS,
        if_exists='replace'
        )

    logger_info(logger, f'Asana Positive Reviews ETL Process completed - Replaced entire table with '
                f'new Positive Reiviews data')
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ASANA POSTITVE REWIVES WHILE PUSHING DATA")