''' FETCH ALL REVIEWS DATA FROM HOSTIFY
    AND PUSH DATA TO GBQ - (replace table) 
'''

import logging
import pandas as pd

from config.credentials import TABLE_REVIEWS 
from gbq.run_query import push_data
from hostify.api_clients.review_api_client import ReviewAPIClient
from hostify.fetch_data import fetch_all_hostify_data
from hostify.schemas import API_REVIEW_SCHEMA


logging.basicConfig(filename='/var/log/etl',
                    level=logging.INFO,
                    format='%(levelname)s|%(asctime)s|%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

logger = logging.getLogger(__name__)
STK_REVIEW_API_CLIENT = ReviewAPIClient('Staykeepers')
LEAP_SLL_TLL_REVIEW_API_CLIENT = ReviewAPIClient()


def import_all_reviews():
    """STK_LEAP_REVIEWS ETL"""

    logger.info('Reviews ETL Process started')

    # STK
    stk_dataframe, _ = fetch_all_hostify_data(api_client=STK_REVIEW_API_CLIENT,
                                              resource_name='reviews',
                                              schema=API_REVIEW_SCHEMA,
                                              params={
                                                  'per_page': 1000}
                                              )

    logger.info('STK Reviews - Fetched all Hostify Data')

    # LEAP-SLL-TLL
    lp_sll_tll_df, _ = fetch_all_hostify_data(api_client=LEAP_SLL_TLL_REVIEW_API_CLIENT,
                                              resource_name='reviews',
                                              schema=API_REVIEW_SCHEMA,
                                              params={
                                                  'per_page': 1000}
                                              )

    logger.info('LEAP-SLL-TLL Reviews - Fetched all Hostify Data')

    final_df = pd.concat([stk_dataframe, lp_sll_tll_df])
    final_df.rename(columns={'id': 'api_id'}, inplace=True)

    push_data(final_df,
              TABLE_REVIEWS,
              if_exists='replace'
            )

    logger.info(f'Reviews ETL Process completed - Replaced entire table with '
                f'new STK,TLL and SLL data')


import_all_reviews()
