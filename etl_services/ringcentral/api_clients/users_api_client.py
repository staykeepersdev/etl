from .base import BaseAPIClient

class UserAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='scim/Users')

    def get_user_data(self,params=None):
        return self._get_json_response(url=self.list_url(),params=params)
