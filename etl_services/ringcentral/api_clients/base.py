from hashlib import new
import json
import requests
from requests.auth import HTTPBasicAuth
from urllib.parse import urljoin
from config.credentials import RINGCENTRAL_API_URL,RINGCENTRAL_HEADER_ACCEPT,RINGCENTRAL_HEADER_CONTENT_TYPE,RINGCENTRAL_CLIENT_ID_OR_BASIC_AUTH_USERNAME,RINGCENTRAL_CLIENT_SECRET_OR_BASIC_AUTH_PASSWORD,RINGCENTRAL_USERNAME,RINGCENTRAL_PASSWORD,RINGCENTRAL_EXTENSION,RINGCENTRAL_GRANT_TYPE
from datetime import datetime,timedelta

def generate_token():
    body={'username':RINGCENTRAL_USERNAME,'password':RINGCENTRAL_PASSWORD,'extension':RINGCENTRAL_EXTENSION,'grant_type':RINGCENTRAL_GRANT_TYPE}
    response= requests.post(url=f'{RINGCENTRAL_API_URL}/restapi/oauth/token',
            auth = HTTPBasicAuth(RINGCENTRAL_CLIENT_ID_OR_BASIC_AUTH_USERNAME, RINGCENTRAL_CLIENT_SECRET_OR_BASIC_AUTH_PASSWORD),
            headers={'Content-Type':RINGCENTRAL_HEADER_CONTENT_TYPE,'Accept':RINGCENTRAL_HEADER_ACCEPT},data=body)
    return json.loads(response.content)


class BaseAPIClient:
 
    def __init__(
        self,
        endpoint=None,
        url=RINGCENTRAL_API_URL,
    ):  
        self.endpoint = endpoint
        self.url = url
        self.headers = {
            'Accept':'application/json',
            'Authorization': f"Bearer {generate_token().get('access_token')}",
        }
    def _get_json_response(self, url: str, params=None,data=None):
        if self.__class__.__name__ == 'UserAPIClient' or self.__class__.__name__ == 'CallLogAPIClient' :
            response = requests.get(url=url,headers=self.headers, params=params)
            if response.status_code == 401:
                self.headers = {
                'Authorization': f"Bearer {generate_token().get('access_token')}"}
                response = requests.get(url=url,headers=self.headers, params=params)
                return json.loads(response.content)   
            return json.loads(response.content)  
        
        response = requests.post(url=url,headers=self.headers, params=params,data=data)
        if response.status_code == 401:
                self.headers = {
                'Authorization': f"Bearer {generate_token().get('access_token')}"}
                response = requests.get(url=url,headers=self.headers, params=params,data=data)
                return json.loads(response.content) 
        return json.loads(response.content)
    
    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def get_list(self, params=None,data=None):
        if self.__class__.__name__ == 'UserAPIClient':
                return [{'id':user_data['id'],'userName':user_data['userName'],'email':user_data['emails'][0]['value'],'department':user_data['urn:ietf:params:scim:schemas:extension:enterprise:2.0:User']['department'] if 'urn:ietf:params:scim:schemas:extension:enterprise:2.0:User' in user_data.keys() else '' } for user_data in self.get_user_data(params=params)['Resources']]
        if self.__class__.__name__ == 'CallPerformanceAPIClient':
            data_call_performance = []
            for call_performance in self.get_call_performance_data(data=data,params=params)['data']['records']:
                data_call_performance.append(
                        {
                        'key':call_performance['key'],
                        'keyInfo_name':call_performance['info']['name'],
                        'timers_allCalls_valueType':call_performance['timers']['allCalls']['values'],
                        'counters_allCalls_valueType':call_performance['counters']['allCalls']['values'],
                        'json':call_performance,
                        'timeFrom':str(datetime.fromisoformat(eval(data)['timeSettings']['timeRange']['timeFrom'][:-1]))+' UTC',
                        'timeTo':str(datetime.fromisoformat(eval(data)['timeSettings']['timeRange']['timeTo'][:-1]))+' UTC'
                    })
            return data_call_performance

        if self.__class__.__name__ == 'UserPerformanceAPIClient':
            data_user_performace = []
            for user in self.get_user_performance_data(data=data,params=params)['data']['records']:
                data_user_performace.append(
                        {
                        'key':user['key'],
                        'keyInfo_name':user['info']['name'],
                        'keyInfo_extensionNumber':user['info']['extensionNumber'],
                        'timers_allCalls_valueType':user['timers']['allCalls']['values'],
                        'counters_allCalls_valueType':user['counters']['allCalls']['values'],
                        'json':user,
                        'timeFrom':str(datetime.fromisoformat(eval(data)['timeSettings']['timeRange']['timeFrom'][:-1]))+' UTC',
                        'timeTo':str(datetime.fromisoformat(eval(data)['timeSettings']['timeRange']['timeTo'][:-1]))+' UTC'
                    })
            return data_user_performace
        
        if self.__class__.__name__ == 'CallLogAPIClient' :
            if data:
                return [
                    {
                    'sessionid' : int(record.get('sessionId')),
                    'durationMS' : record.get('durationMs'),
                    'direction' : record.get('direction'),
                    'action' : record.get('action'),
                    'result' : record.get('result'),
                    'from_name' : record.get('from').get('name'),
                    'to_name' : record.get('to').get('name'),
                    'startTime' : record.get('startTime').replace('T',' ')[:19],
                    } for record in data]