from .base import BaseAPIClient

class CallLogAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='restapi/v1.0/account/~/call-log')

    def get_call_log_data(self, params=None):
        return self._get_json_response(url=self.list_url(), params=params)

