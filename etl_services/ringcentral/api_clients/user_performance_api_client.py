from .base import BaseAPIClient

class UserPerformanceAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='analytics/calls/v1/accounts/~/aggregation/fetch')

    def get_user_performance_data(self,data,params=None):
        return self._get_json_response(url=self.list_url(),data=data,params=params)

