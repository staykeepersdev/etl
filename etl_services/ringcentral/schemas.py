import pandas as pd

API_RINGCENTRAL_USERS_SCHEMA = {
    'id':pd.StringDtype(),
    'userName':pd.StringDtype(),
    'email':pd.StringDtype(),
    'department':pd.StringDtype(),
    
}

API_RINGCENTRAL_CALL_PERFORMANCE_SCHEMA  = {

    'key':pd.StringDtype(),
    'keyInfo_name':pd.StringDtype(),
    'timers_allCalls_valueType':pd.StringDtype(),
    'counters_allCalls_valueType':pd.StringDtype(),
    'json':pd.StringDtype(),
    'timeFrom':pd.StringDtype(),
    'timeTo':pd.StringDtype(),
    
}

API_RINGCENTRAL_USER_PERFORMANCE_SCHEMA = {
    'key':pd.StringDtype(),
    'keyInfo_name':pd.StringDtype(),
    'keyInfo_extensionNumber':pd.StringDtype(),
    'timers_allCalls_valueType':pd.StringDtype(),
    'counters_allCalls_valueType':pd.StringDtype(),
    'json':pd.StringDtype(),
    'timeFrom':pd.StringDtype(),
    'timeTo':pd.StringDtype(),
    
}

API_RINGCENTRAL_CALL_LOG_SCHEMA = {
    'sessionid' : pd.Int64Dtype(),
    'durationMS' : pd.Int64Dtype(),
    'direction' : pd.StringDtype(),
    'action' : pd.StringDtype(),
    'result' : pd.StringDtype(),
    'from_name' : pd.StringDtype(),
    'to_name' : pd.StringDtype(),
    'date_fields': {
        'startTime': {'type': 'datetime', 'format': '%Y-%m-%d %H:%M:%S'}
    }
}