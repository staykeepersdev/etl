from .base import BaseAPIClient

class StoryAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='stories/')

    def get_story_data(self, task_gid, params={'opt_pretty':'true'}):
        return self.get_item(task_gid, params)
