from .base import BaseAPIClient
from config.credentials import TEAM_ID

class TeamAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='teams/')

    def get_projects(self, TEAM_ID, params={'archived':'false'}):
        return self.get_item(TEAM_ID, params=params)
