from .base import BaseAPIClient

class ProjectAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='projects/')

    def get_tasks_from_project(self, project_gid, params={'opt_pretty':'true', 'limit':100, 'offset':None}):
        return self.get_item(project_gid, params)