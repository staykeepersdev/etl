from .base import BaseAPIClient
from config.credentials import WORKSPACE_ID
class UserAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='users/')

    def get_users(self, params={'workspace':WORKSPACE_ID,'opt_pretty':'true', 'opt_fields':'name,email'}):
        url = self.list_url()
        return self._get_json_response(url=url, params=params)
