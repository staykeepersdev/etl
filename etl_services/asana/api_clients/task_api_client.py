from .base import BaseAPIClient

class TaskAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint='tasks/')

    def get_task(self, task_gid, params):
        return self.get_item(task_gid, params)
