from .base import BaseAPIClient
class SearchAPIClient(BaseAPIClient):
    def __init__(self):
        super().__init__(endpoint="search/")

    def search_tasks(self, params=None):
        return self.get_list(params)
