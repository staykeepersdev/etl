import json
import requests
import time
from urllib.parse import urljoin

from config.credentials import ASANA_AUTHORIZATION_HEADER, ASANA_BASE_URL, WORKSPACE_ID

class BaseAPIClient:

    def __init__(
        self,
        endpoint=None,
        url=ASANA_BASE_URL,
    ):
        self.endpoint = endpoint
        self.url = url
        self.headers = {
            'Authorization': ASANA_AUTHORIZATION_HEADER
        }
        self.count = 0

    def _get_json_response(self, url: str, params=None):
        if self.count == 1200:
            self.count=0
            time.sleep(30)
        response = requests.get(url=url, headers=self.headers, params=params)
        self.count=self.count+1
        return json.loads(response.content)
 
    def list_url(self):
        return urljoin(self.url, self.endpoint)

    def detail_url(self, identifier: int):
        return urljoin(self.list_url(), str(identifier))

    def get_item(self, identifier: int, params=None):
        url = self.detail_url(identifier=identifier)
        if self.endpoint == 'teams/':
            url += '/projects'
        if self.endpoint == 'projects/':
            url += '/tasks' 
        if self.endpoint == 'stories/':
            url = url.replace('stories','tasks')
            url += '/stories' 
        return self._get_json_response(url=url, params=params)

    def get_list(self, params=None):
        url = self.list_url()
        if self.endpoint == 'search/':
            url = url.replace('search/', f"workspaces/{WORKSPACE_ID}/tasks/search")
        return self._get_json_response(url=url, params=params)