from datetime import datetime, timedelta
from config.credentials import TEAM_ID
from asana.api_clients.teams_api_client import TeamAPIClient
from asana.api_clients.task_api_client import TaskAPIClient
from asana.api_clients.project_api_client import ProjectAPIClient
from asana.api_clients.story_api_client import StoryAPIClient

TASK_API_CLIENT = TaskAPIClient()
TEAM_API_CLIENT = TeamAPIClient()
STORY_API_CLIENT = StoryAPIClient()
PROJECT_API_CLIENT = ProjectAPIClient()
TODAY = datetime.now().date()
YESTERDAY = TODAY - timedelta(1)
YEAR = YESTERDAY.year
MONTH = YESTERDAY.strftime("%B")
JAN_01_2022 = datetime(2022, 1, 1, 00, 00)
ELEVEN_TO_TWENTY = ['11','12','13','14','15','16','17','18','19', '20']

def get_project_name():
    '''
    Get Yesterday's Project Name
    '''
    DAY = YESTERDAY.day
    suffix = ''
    if str(DAY) in ELEVEN_TO_TWENTY: 
        suffix += 'th'
    elif str(DAY)[-1] == '1':
        suffix += 'st'
    elif str(DAY)[-1] == '2':
        suffix += 'nd'
    elif str(DAY)[-1] == '3':
        suffix += 'rd'
    else:
        suffix += 'th'

    project_name = f"{DAY}{suffix} of {MONTH} - {YEAR} -DAILY CLEANING SCHEDULE"
    return project_name.replace(' ','').replace('-','')

def get_project_id():
    '''
    Get Project ID from Team ID
    '''
    projects = TEAM_API_CLIENT.get_projects(TEAM_ID)
    projects_data = projects['data']
    project_name = get_project_name()
    project_id = ''
    for project_data in projects_data:
        if project_data['name'].replace(' ','').replace('-','') == project_name:
            project_id = project_data['gid']
            print(project_id, project_name)
    return project_id

def fetch_tasks_id_list_from_project(project_id = None):
    '''
    Fetch all tasks IDs list from project ID
    '''
    tasks_id_list = []
    if not project_id:
        project_id = get_project_id()
    is_offset = True
    tasks = PROJECT_API_CLIENT.get_tasks_from_project(project_id)
    
    while(tasks['data'] and is_offset):
        for data in tasks['data']: 
            tasks_id_list.append(data['gid'])
        if tasks['next_page'] and tasks['next_page']['offset']:
            offset = tasks['next_page']['offset']
            tasks = PROJECT_API_CLIENT.get_tasks_from_project(project_id, params={'opt_pretty':'true', 'limit':100, 'offset':offset})
        else:
            is_offset = False
    return tasks_id_list


def task_story_data(story_data, task_id):
    '''
    Get the Required Story Data from the API
    '''
    task_story = {
        'data':[]
    }
    try:
        for data in story_data['data']:
            if not 'assigned to' in data['text']:
                continue
            required_story_data = {
                'task_id':task_id,
                'story_id':data['gid'],
                'created_at':data['created_at'],
                'resource_subtype':data['resource_subtype'],
                'text':data['text'],
            }
            task_story['data'].append(required_story_data)
    except Exception as e:
        print(e, task_id)

    return task_story


def get_positive_reviews(get_task_data):
    '''
    Get Positive Reviews Data from the API
    '''
    positive_reviews_data = {'data':[]}
    assignee_and_membership_data={}
    data = get_task_data['data']
    gid = data['gid'] if data['gid'] else ''
    task_name = data['name'] if data['name'] else ''
    notes = data['notes'] if data['notes'] else ''
    created_at = data['created_at'][:19].replace('T', ' ') if data['created_at'] else ''
    completed_at = data['completed_at'][:19].replace('T', ' ') if data['completed_at'] else ''
    last_modified = data['modified_at'][:19].replace('T', ' ') if data['modified_at'] else ''
    start_date = data['start_on'] if data['start_on'] else ''
    tags = data['tags'] if data['tags'] else ''
    parent_task = data.get('parent').get('name') if data.get('parent') else ''
    assignee_data = data.get('assignee')
    memberships_data = data.get('memberships')

    if assignee_data:
        assignee_and_membership_data['name'] = assignee_data.get('name')
        assignee_and_membership_data['assignee'] = assignee_data.get('gid')
        assignee_and_membership_data['assignee_email'] = assignee_data.get('email')
    
    if memberships_data:
        for dict_data in memberships_data:
            for key,value in dict_data.items():
                if key == 'project':
                    if assignee_and_membership_data.get('projects'):
                        assignee_and_membership_data['projects'] = f"{assignee_and_membership_data['projects']}, {value['name']}"
                    else:
                        assignee_and_membership_data['projects'] = value['name']
                if key == 'section':
                    if assignee_and_membership_data.get('section'):
                        assignee_and_membership_data['section'] = f"{assignee_and_membership_data['section']}, {value['name']}"
                    else:
                        assignee_and_membership_data['section'] = value['name']
    
    due_date = data['due_on'] if data['due_on'] else ''
    project_id = ''
    if data['projects']:
        project_data = [project['gid'] for project in data['projects'] if 'Positive Guest Reviews' in project['name']]
        project_id = project_data[0]

    required_fields = {
        'gid':gid,
        'task_name':task_name,
        'due_date': due_date,
        'projects':project_id,
        'note':notes,
        'created_at':created_at,
        'completed_at':completed_at,
        'last_modified':last_modified,
        'start_date':start_date,
        'tags':tags,
        'parent_task':parent_task,
    }    
    display_name_dict = {}
    display_value_list = [display['name'] for display in data['custom_fields']]

    for display_values in display_value_list:
        try:
            display_data = [custom_field['display_value'] for custom_field in data['custom_fields'] if custom_field['name'] == display_values]
            while None in display_data:
                display_data.remove(None)
            display_name_dict[display_values.lower().replace(' ','_')] = ''
            if len(display_data) > 1:
                idx=0
                for dis_data in display_data:
                    display_name_dict[display_values.lower().replace(' ','_')] += f"{dis_data}, "
                    idx +=1
            if display_data:
                display_name_dict[display_values.lower().replace(' ','_')] = display_data[0]
        except Exception as e:
            print(e)

    required_fields.update(display_name_dict)
    required_fields.update(assignee_and_membership_data)
    positive_reviews_data['data'].append(required_fields)
    required_fields['check_in/comfort']=required_fields.get('check-in/comfort')

    return positive_reviews_data

def get_negative_reviews(get_task_data):
    '''
    Get Negative Reviews Data from the API
    '''
    negative_reviews_data = {'data':[]}
    assignee_and_membership_data = {}
    data = get_task_data['data']
    gid = data['gid'] if data['gid'] else ''
    task_name = data['name'] if data['name'] else ''
    notes = data['notes'] if data['notes'] else ''
    created_at = data['created_at'][:19].replace('T', ' ') if data['created_at'] else ''
    completed_at = data['completed_at'][:19].replace('T', ' ') if data['completed_at'] else ''
    last_modified = data['modified_at'][:19].replace('T', ' ') if data['modified_at'] else ''
    start_date = data['start_on'] if data['start_on'] else ''
    tags = data['tags'] if data['tags'] else ''
    parent_task = data.get('parent').get('name') if get_task_data.get('parent') else ''
    
    assignee_data = data.get('assignee')
    memberships_data = data.get('memberships')

    if assignee_data:
        assignee_and_membership_data['name'] = assignee_data.get('name')
        assignee_and_membership_data['assignee'] = assignee_data.get('gid')
        assignee_and_membership_data['assignee_email'] = assignee_data.get('email')
    
    if memberships_data:
        for dict_data in memberships_data:
            for key,value in dict_data.items():
                if key == 'project':
                    if assignee_and_membership_data.get('projects'):
                        assignee_and_membership_data['projects'] = f"{assignee_and_membership_data['projects']}, {value['name']}"
                    else:
                        assignee_and_membership_data['projects'] = value['name']
                if key == 'section':
                    if assignee_and_membership_data.get('section'):
                        assignee_and_membership_data['section'] = f"{assignee_and_membership_data['section']}, {value['name']}"
                    else:
                        assignee_and_membership_data['section'] = value['name']

    
    due_date = data['due_on'] if data['due_on'] else ''

    project_id = ''
    if data['projects']:
        project_data = [project['gid'] for project in data['projects'] if 'Negative Guest Reviews' in project['name']]
        project_id = project_data[0]

    required_fields = {
        'gid':gid,
        'task_name':task_name,
        'due_date': due_date,
        'project_id':project_id,
        'notes':notes,
        'created_at':created_at,
        'completed_at':completed_at,
        'last_modified':last_modified,
        'start_date':start_date,
        'tags':tags,
        'parent_task':parent_task
    }
    display_value_list = [display['name'] for display in data['custom_fields']]
    display_name_dict = {}
    for display_values in display_value_list:
        try:
            display_data = [custom_field['display_value'] for custom_field in data['custom_fields'] if custom_field['name'] == display_values]
            if None in display_data:
                display_data.remove(None)
            display_name_dict[display_values.lower().replace(' ','_')] = ''
            if len(display_data) > 1:
                idx=0
                for dis_data in display_data:
                    display_name_dict[display_values.lower().replace(' ','_')] += f"{dis_data}, "
                    idx +=1
            if display_data:
                display_name_dict[display_values.lower().replace(' ','_')] = display_data[0]
        except Exception as e:
            print(e)
    required_fields.update(display_name_dict)
    required_fields.update(assignee_and_membership_data)
    required_fields['check_in/comfort']=required_fields.get('check-in/comfort')
    required_fields['last_cc_agent_interaction_(agent_before_medylyn)']=required_fields.get('last_cc_agent_interaction_(last_agent_before_hans/meds_for_courtesy_call_or_email)')
    required_fields['how_many_followups_did_the_guest_do_about_this_issue?_(count_both_email_or_phone_call)']=required_fields.get('how_many_follow-ups_did_the_guest_do_about_this_issue?_(count_both_email_and/or_call)')
    negative_reviews_data['data'].append(required_fields)

    return negative_reviews_data


def get_guest_requests_or_complaint(get_task_data):
    '''
    Get Guest Request And Complaint Data from the API
    '''
    data = {}
    get_task_data = get_task_data['data']
    
    data['task_id'] = get_task_data.get('gid') if get_task_data.get('gid') else None
    data['created_at'] = get_task_data.get('created_at')[:19].replace('T', ' ') if get_task_data.get('created_at') else ''
    data['completed_at'] = get_task_data.get('completed_at')[:19].replace('T', ' ') if get_task_data.get('completed_at') else ''
    data['last_modified'] = get_task_data.get('modified_at')[:19].replace('T', ' ') if get_task_data.get('modified_at') else ''
    data['start_date '] = get_task_data.get('start_on') if get_task_data.get('start_on') else ''
    data['due_date'] = get_task_data.get('due_on') if get_task_data.get('due_on') else ''
    data['tags'] = get_task_data.get('tags') if get_task_data.get('tags') else ''
    data['notes'] = get_task_data.get('notes') if get_task_data.get('notes') else ''
    data['parent_task '] = get_task_data.get('parent').get('name') if get_task_data.get('parent') else ''

    assignee_data = get_task_data.get('assignee')
    memberships_data = get_task_data.get('memberships')

    if assignee_data:
        data['name'] = assignee_data.get('name')
        data['assignee'] = assignee_data.get('gid')
        data['assignee_email'] = assignee_data.get('email')
    
    if memberships_data:
        for dict_data in memberships_data:
            for key,value in dict_data.items():
                if key == 'project':
                    if data.get('projects'):
                        data['projects'] = f"{data['projects']}, {value['name']}"
                    else:
                        data['projects'] = value['name']
                if key == 'section':
                    if data.get('section'):
                        data['section'] = f"{data['section']}, {value['name']}"
                    else:
                        data['section'] = value['name']
    
    display_value_list = [display['name'] for display in get_task_data['custom_fields']]
    display_name_dict = {}
    for display_values in display_value_list:
        try:
            display_data = [custom_field['display_value'] for custom_field in get_task_data['custom_fields'] if custom_field['name'] == display_values]
            if None in display_data:
                display_data.remove(None)
            display_name_dict[display_values.lower().replace(' ','_')] = ''
            if len(display_data) > 1:
                idx=0
                for dis_data in display_data:
                    display_name_dict[display_values.lower().replace(' ','_')] += f"{dis_data}, "
                    idx +=1
            if display_data:
                display_name_dict[display_values.lower().replace(' ','_')] = display_data[0]
        except Exception as e:
            print(e)
    
    data.update(display_name_dict)
    return {'data': [data]}    

def rename_dataframe(dataframe, resource_name):
    '''
    Rename positive and negative api dataframe.
    '''
    if resource_name == 'asana_ops_tasks':
        dataframe.rename(
        columns = {
            'cf_check-out':'cf_check_out',
            'cf_type_':'cf_type',
            },
            inplace = True
        )

    if resource_name == 'asana_positive_guest_reviews':
        dataframe.rename(
        columns = {
            'accuracy/facilities':'accuracy_facilities',
            'staff/communication':'staff_communication',
            'check_in/comfort':'check_in_comfort',
            'other_cc_agents_who_helped_in_getting_the_5*_guest_review':'other_cc_agents_who_helped_in_getting_the_5_star_guest_review',
            'last_cc_agent_interaction_(agent_before_medylyn)':'last_cc_agent_interaction_agent_before_medylyn',
            'section':'section_column',
            'reservation_number':'booking_reference_number',
            },
            inplace = True
        )
    if resource_name == 'asana_negative_guest_reviews':
        dataframe.rename(
            columns = {
                'accuracy/facilities':'accuracy_facilities',
                'guest_self-isolating':'guest_self_isolating',
                'staff/communication':'staff_communication',
                'check_in/comfort':'check_in_comfort',
                'last_cc_agent_interaction_(agent_before_medylyn)':'last_cc_agent_interaction_agent_before_medylyn',
                'what_was_the_major_issue_that_caused_the_negative_review?':'what_was_the_major_issue_that_caused_the_negative_review',
                'how_many_followups_did_the_guest_do_about_this_issue?_(count_both_email_or_phone_call)':'how_many_followups_did_the_guest_do_about_this_issue_count_both_email_or_phone_call',
                'was_the_issue_resolved?_(resolved/_not_resolved)':'was_the_issue_resolved_resolved_or_not_resolved',
                'if_issue_is_resolved,_how_long_did_it_take_for_the_issue_to_be_resolved?_(hours_or_days)':'if_issue_is_resolved_or_how_long_did_it_take_for_the_issue_to_be_resolved_hours_or_days',
                'if_the_isssue_is_not_resolved,_how_long_was_the_time_since_the_issue_was_raised_by_the_guest_until_the_guest_checked_out?_(hours_or_days)':'if_the_isssue_is_not_resolved_how_long_was_the_time_since_the_issue_was_raised_by_the_guest_until_the_guest_checked_out_hours_or_days',
                'if_the_issue_was_not_resolved,_was_there_another_guest_who_checked-in_in_the_same_room._(y/n)':'if_the_issue_was_not_resolved_was_there_another_guest_who_checked_in_the_same_room_y_or_n',
                'was_willingness_to_assist_shown?':'was_willingness_to_assist_shown_y_or_n',
                'did_agent_show/display_understanding_of_the_issue_presented_by_the_guest?':'did_the_agent_show_or_display_understanding_of_the_issue_presented_by_guest_y_or_n',
                'were_correct_actions_to_resolve_issue_done/taken?_(y/n)':'were_correct_actions_to_resolve_issue_done_or_taken_y_or_n',
                'was_agent_rude/did_the_agent_show_untoward_behavior_during_the_interaction?':'was_agent_rude_or_did_agent_show_untoward_behavior_during_the_interaction_y_or_n',
                'was_issue_escalated_if_needed?':'was_the_issue_escalated_if_needed_y_or_n',
                'were_correct_actions_done/taken_to_resolve_issue?':'were_correct_actions_done_or_taken_to_resolve_issue',
                'section':'section_column',
                'reservation_number':'booking_reference_number',
                },
                inplace = True
            )
    
    if resource_name == 'asana_guest_review_or_complaint':
        dataframe.rename(
            columns = {
                'what_is_the_major_issue_that_causes_guests_complaints?':'what_is_the_major_issue_that_causes_guests_complaints',
                'guest_self_isolating_':'guest_self_isolating',
                'booking_status_':'booking_status',
                'issue_resolved':'resolutions_stages',
                'property':'building_name',
            },
            inplace = True
        )

    return dataframe

def get_project_name_specific_date(datetime_obj):
    '''
    Get project name of specific date
    '''
    DATE = datetime_obj.date()
    YEAR = DATE.year
    MONTH = DATE.strftime("%B")
    DAY = DATE.day
    suffix = ''
    if str(DAY) in ELEVEN_TO_TWENTY: 
        suffix += 'th'
    elif str(DAY)[-1] == '1':
        suffix += 'st'
    elif str(DAY)[-1] == '2':
        suffix += 'nd'
    elif str(DAY)[-1] == '3':
        suffix += 'rd'
    else:
        suffix += 'th'  

    project_name = f"{DAY}{suffix} of {MONTH} - {YEAR} -DAILY CLEANING SCHEDULE"
    return project_name.replace(' ','').replace('-','')


def get_project_id_for_specific_date():
    '''
    Get project_id_list of specific Date
    '''
    project_ids_list = []
    projects = TEAM_API_CLIENT.get_projects(TEAM_ID)
    projects_data = projects['data']
    datetime_obj = JAN_01_2022

    previous_date = f"{datetime_obj.date().day}{datetime_obj.date().month}"

    while(previous_date != f"{TODAY.day}{TODAY.month}"):
        project_name = get_project_name_specific_date(datetime_obj)
        project_id = ''
        for project_data in projects_data:
            if project_data['name'].replace(' ','').replace('-','') == project_name:
                project_id = project_data['gid']
                project_ids_list.append(project_id)
                print(project_id, project_name)
        datetime_obj = datetime_obj + timedelta(1)
        previous_date = f"{datetime_obj.date().day}{datetime_obj.date().month}"
    return project_ids_list

def get_tasks_search_details(search_data):
    required_fields = {}
    prepared_data ={'data':[]}

    for data in search_data['data']:
        task_id = data['gid'] 
        assignee= data['assignee'] 
        if type(assignee) is dict:
            assignee = assignee['gid']
        parent= data['parent'] 
        if type(parent) is dict:
            parent = parent['gid']
        workspace= data['workspace']
        if type(workspace) is dict:
            workspace = workspace['gid']
        attachments = True if data['attachments'] else False
        modified_at = data['modified_at'] if data['modified_at'] else ''
        due_on = data['due_on'] if data['due_on'] else ''
        name = data['name'] if data['name'] else ''
        notes = data['notes'] if data['notes'] else ''
        created_at = data['created_at'] if data['created_at'] else ''
        completed_at = data['completed_at'] if data['completed_at'] else ''
        assignee_status = data['assignee_status'] if data['assignee_status'] else ''
        completed = True if data['completed'] else False
        due_at = data['due_at'] if data['due_at'] else ''
        permalink_url = data['permalink_url'] if data['permalink_url'] else ''
        resource_type = data['resource_type'] if data['resource_type'] else ''
        followers = True if data['followers'] else False
        start_at = data['start_at'] if data['start_at'] else ''
        start_on = data['start_on'] if data['start_on'] else ''
        tags = True if data['tags'] else False
        try:
            projects = ''
            projects_name = ''
            if data['projects']:
                project_data = [project for project in data['projects'] if 'DAILY CLEANING SCHEDULE' in project['name']]
                if project_data:
                    projects = project_data[0]['gid']
                    projects_name = project_data[0]['name']
                else:
                    project_data = [project for project in data['projects']]
                    if project_data:
                        projects = project_data[0]['gid']
                        projects_name = project_data[0]['name']

        except Exception as e:
            print(e, task_id)


        required_fields = {
            'task_id':task_id,
            'name':name,
            'assignee':assignee,
            'assignee_status':assignee_status,
            'hasAttachments':attachments,
            'completed':completed,
            'completed_at':completed_at,
            'created_at':created_at,
            'due_at':due_at,
            'due_on':due_on,
            'followers':followers,
            'modified_at':modified_at,
            'notes':notes,
            'parent':parent,
            'permalink_url':permalink_url,
            'projects':projects,
            'projects_name':projects_name,
            'resource_type':resource_type,
            'start_at':start_at,
            'start_on':start_on,
            'tags':tags,
            'workspace':workspace,
        }
        display_name_dict = {
            'cf_check_in_status': '',
            'cf_time_for_cleaning': '0',
            'cf_type_': '',
            'cf_clean_task_status': '',
            'cf_building_name': '',
            'cf_sets_of_linen': '',
            'cf_confirmation_code': '',
            'cf_status': '',
            'cf_check-out': ''
        }
        display_value_list = [display['name'] for display in data['custom_fields']]
        for display_values in display_value_list:
            try:
                display_data = [custom_field['display_value'] for custom_field in data['custom_fields'] if custom_field['name'] == display_values]
                while None in display_data:
                    display_data.remove(None)
                if len(display_data) > 1:
                    idx=0
                    for dis_data in display_data:
                        display_name_dict[f"cf_{display_values.lower().replace(' ','_')}"] += f"{dis_data}, "
                        idx +=1
                if display_data:
                    display_name_dict[f"cf_{display_values.lower().replace(' ','_')}"] = display_data[0]

            except Exception as e:
                pass

        required_fields.update(display_name_dict)
        prepared_data['data'].append(required_fields)
    return prepared_data

def get_checkin_search_details(search_data):
    required_fields = {}
    prepared_data ={'data':[]}

    for data in search_data['data']:
        task_id = data.get('gid') 
        name = data.get('name') 
        assignee= data['assignee'] if data['assignee'] else ''
        assignee_status = data.get('assignee_status') 
        attachments = True if data['attachments'] else False
        completed = data.get('completed')
        completed_at = data['completed_at'][:19].replace('T', ' ') if data['completed_at'] else ''
        created_at = data['created_at'][:19].replace('T', ' ') if data['created_at'] else ''
        modified_at = data['modified_at'][:19].replace('T', ' ') if data['modified_at'] else ''
        due_at = data['due_at'][:19].replace('T', ' ') if data['due_at'] else ''
        if type(assignee) is dict:
            assignee = assignee['gid']
        parent= data.get('parent') 
        if type(parent) is dict:
            parent = parent.get('gid')
        workspace= data['workspace'] 
        if type(workspace) is dict:
            workspace = workspace.get('gid')
        due_on = data.get('due_on') 
        followers = True if data['followers'] else False
        notes = data.get('notes') 
        permalink_url = data.get('permalink_url') 
        resource_type = data.get('resource_type') 
        start_at = data.get('start_at') 
        start_on = data.get('start_on') 
        tags = True if data['tags'] else False
        try:
            project_id = ''
            project_name = ''
            if data['projects']:
                project_data = [project for project in data['projects'] if ('Check Ins' or 'check ins' or 'Check-ins' or 'check-ins') in project['name']]
                if project_data:
                    project_id = project_data[0]['gid']
                    project_name = project_data[0]['name']
                else:
                    project_data = [project for project in data['projects']]
                    if project_data:
                        project_id = project_data[0]['gid']
                        project_name = project_data[0]['name']
        except Exception as e:
            print(e, task_id)

        required_fields = {
            'task_id':task_id,
            'name':name,
            'assignee':assignee,
            'assignee_status':assignee_status,
            'hasAttachments':attachments,
            'completed':completed,
            'completed_at':completed_at,
            'created_at':created_at,
            'due_at':due_at,
            'due_on':due_on,
            'followers':followers,
            'modified_at':modified_at,
            'notes':notes,
            'parent':parent,
            'permalink_url':permalink_url,
            'project_id':project_id,
            'project_name':project_name,
            'resource_type':resource_type,
            'start_at':start_at,
            'start_on':start_on,
            'tags':tags,
            'workspace':workspace,
        }
        display_name_dict = {}

        display_value_list = [display['name'] for display in data['custom_fields']]

        for display_values in display_value_list:
            try:
                display_name_dict[f"cf_{display_values.lower().replace(' ','_')}"] = ''
                display_data = [custom_field['display_value'] for custom_field in data['custom_fields'] if custom_field['name'] == display_values]
                while None in display_data:
                    display_data.remove(None)
                if len(display_data) > 1:
                    idx=0
                    for dis_data in display_data:
                        display_name_dict[f"cf_{display_values.lower().replace(' ','_')}"] += f"{dis_data}, "
                        idx +=1
                if display_data:
                    display_name_dict[f"cf_{display_values.lower().replace(' ','_')}"] = display_data[0]
            except Exception as e:
                print(e)
        
        required_fields.update(display_name_dict)
        prepared_data['data'].append(required_fields)

    return prepared_data