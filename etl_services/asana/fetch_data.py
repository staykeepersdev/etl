import logging
import pandas
import pytz
import concurrent.futures
from datetime import datetime
from asana.methods import fetch_tasks_id_list_from_project, get_checkin_search_details, get_negative_reviews, get_positive_reviews, get_tasks_search_details, rename_dataframe, task_story_data, get_guest_requests_or_complaint
from hostify.utils import get_schema_columns, raise_api_conn_err
from hostify.fetch_data import DEFAULT_MAX_WORKERS
from utils import get_tasks_param_list

logger = logging.getLogger(__name__)

def fetch_all_asana_users(
    api_client,
    resource_name: str,
    params=[],
    schema={},
    custom_fields_schema={},
    project_id=str,
    ):
    """Fetching all asana users"""
    resources = []
    task_id = ''
    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names += get_schema_columns(custom_fields_schema, custom=True)

    logger.info(f'Asana API calls will be started to collect all asana users')

    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client, params, resource_name, column_names, schema, task_id, custom_fields_schema)}

        for future in concurrent.futures.as_completed(responses):
            try:
                resources.extend(future.result())
            except Exception as e:
                logger.error(e)
                pass
    logger.info(f'Collected all asana users from Asana')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})

    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
    df.insert(len(df.columns), 'created_at', datetime.utcnow())

    logger.info(f'Dataframe of asana user created with new data')
    return df


def fetch_all_asana_data(api_client,
                        resource_name: str,
                        params=None,
                        schema={},
                        custom_fields_schema={},
                        project_id = ''
                        ):
    """Fetching Asana Tasks"""
    resources = []
    column_names = get_schema_columns(schema)
    try:
        get_tasks_list = fetch_tasks_id_list_from_project(project_id)
    except Exception as e:
        print(e)
        get_tasks_list = []
    if custom_fields_schema:
        column_names += get_schema_columns(custom_fields_schema, custom=True)
    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client, params, resource_name, column_names, schema, task_id, custom_fields_schema):task_id for task_id in get_tasks_list}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())

    logger.info(f'Collected all {resource_name} data from Asana')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
    if resource_name in ['asana_negative_guest_reviews', 'asana_positive_guest_reviews', 'asana_guest_review_or_complaint']:        
        df = rename_dataframe(df, resource_name)
    logger.info(f'Dataframe of {resource_name} created with new data')

    return df, all_schema

def api_call_per_page(api_client, params, resource_name, column_names, schema, task_id, custom_fields_schema):
    resources = []
    if api_client.endpoint == 'users/' and resource_name == 'asana_users':
        data = api_client.get_users()
    if api_client.endpoint == 'stories/' and resource_name == 'asana_ops_tasks_story':
        story_data = api_client.get_story_data(task_id)
        data = task_story_data(story_data, task_id)
    if api_client.endpoint == 'search/' and resource_name == 'asana_ops_tasks':
        search_data = api_client.search_tasks(params=params)
        data = get_tasks_search_details(search_data)
    if api_client.endpoint == 'search/' and resource_name == 'asana_check_ins':
        search_data = api_client.search_tasks(params=params)
        data = get_checkin_search_details(search_data)
    if api_client.endpoint == 'tasks/' and resource_name == 'asana_positive_guest_reviews':
        get_task_data = api_client.get_task(task_id, params)
        data = get_positive_reviews(get_task_data)
    if api_client.endpoint == 'tasks/' and resource_name == 'asana_negative_guest_reviews':
        get_task_data = api_client.get_task(task_id, params)
        data = get_negative_reviews(get_task_data)
    if api_client.endpoint == 'tasks/' and resource_name == 'asana_guest_review_or_complaint':
        get_task_data = api_client.get_task(task_id, params)
        data = get_guest_requests_or_complaint(get_task_data)

    date_fields = schema.get('date_fields')
    if data['data']:
        resource = data['data']
        for row in resource:
            if date_fields:
                for key, val in date_fields.items():
                    timestamp = datetime.strptime(row[key], val['format']).replace(tzinfo=pytz.utc) if row[key] else None
                    row[key] = timestamp
            new_row = [[None if value == '' else value for value in list(map(row.get, column_names))]]
            resources.extend(new_row)
    else:
        err_msg = data.get('error')
        raise_api_conn_err(err_msg, api_client, params)
    return resources


def fetch_asana_checkins_and_tasks(api_client,
                           resource_name: str,
                           params=None,
                           schema={},
                           custom_fields_schema={},
                           ):
    """Fetching Asana Tasks and Check-ins"""
    resources = []

    param_list = get_tasks_param_list(api_client, params)

    column_names = get_schema_columns(schema)
    if custom_fields_schema:
        column_names += get_schema_columns(custom_fields_schema, custom=True)
    with concurrent.futures.ThreadPoolExecutor(max_workers=DEFAULT_MAX_WORKERS) as executor:
        responses = {executor.submit(
            api_call_per_page, api_client, params,
            resource_name, column_names,
            schema, 1, custom_fields_schema): params for params in param_list}

        for future in concurrent.futures.as_completed(responses):
            resources.extend(future.result())

    logger.info(f'Collected all {resource_name} data from Asana')

    all_schema = {k: v for k, v in schema.items() if k != 'date_fields'}
    if custom_fields_schema:
        _ = {k: v for k, v in custom_fields_schema.items() if k !=
             'date_fields'}

        all_schema.update({f'custom_field_{k}': v for k,
                           v in _.items()})
    df = pandas.DataFrame(resources, columns=column_names).astype(all_schema)
    if resource_name in ['asana_ops_tasks']:
        df = rename_dataframe(df, resource_name)
    logger.info(f'Dataframe of {resource_name} created with new data')
    return df, all_schema
