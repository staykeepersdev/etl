from asana.fetch_data import fetch_all_asana_users
from asana.api_clients.user_api_client import UserAPIClient
from asana.schemas import API_USER_SCHEMA
from config.credentials import TABLE_ASANA_USERS
from gbq.run_query import push_data
import logging
from config.settings import SANTARY_SDK
from config.logger_handler import logger_error,logger_info


logger = logging.getLogger(__name__)

ASANA_API_CLIENT = UserAPIClient()

try:
    dataframe = fetch_all_asana_users(
        ASANA_API_CLIENT,
        resource_name = 'asana_users',
        schema = API_USER_SCHEMA
    )

    push_data(
        dataframe,
        TABLE_ASANA_USERS,
        if_exists='replace'
        )
    logger_info(logger, f"Data pushed for Asana Users")
except Exception as e:
    logger_error(logger, f"{e}: EXCEPTION OCCURED IN ASANA USERS WHILE PUSHING DATA")
