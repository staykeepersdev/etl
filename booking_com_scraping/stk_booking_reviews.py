import pandas as pd
import numpy as np
import pygsheets

import requests
from lxml import html
import re
import concurrent.futures
from datetime import datetime
import os
import sys

CURR_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
DIR_PATH = os.path.dirname(CURR_DIR_PATH)

# The GBQ functionality is developed only for one dataset = "landlordhostifydatabase" and "push_data" has to be refactored to update table names
# from etl_services.gbq.run_query import push_data

headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 Safari/537.36'}

global ROW_LIST_STATIC
ROW_LIST_STATIC = []

THREADS = 2


def url_clean(url):
    url_clean = ".".join(url.split(".")[2:3])
    url_clean = f'https://www.booking.{url_clean}.en-gb.html'
    return url_clean


def get_booking_url_gsheet():
    g_account_file = os.path.join(DIR_PATH, 'etl_services/config/staykeepers-9e5566de4b53.json')
    sheet_url = pygsheets.authorize(service_account_file=g_account_file)
    sheet_id = '1fx3WrReXTIbLPtL1K7sHCN7T8JdZeA7UuiAk4EmttiI'
    sheet = sheet_url.open_by_key(sheet_id)
    sheet_wks = sheet.worksheet_by_title('Booking')
    booking_df = sheet_wks.get_as_df(include_tailing_empty=True)

    booking_df = booking_df[booking_df['open/close'] == 'open']

    return booking_df


def fetch_bcom_static_content(bcom_hotel_url):
    url = url_clean(bcom_hotel_url)
    print(url)
    r = requests.get(url, headers=headers)
    tree = html.fromstring(r.content)

    try:
        hotel_id = tree.xpath('//div[@data-param-dest_type="hotel"]/@data-param-dest_id')[0]
    except:
        hotel_id = ''

    try:
        hotel_type = tree.xpath("//h2[@id='hp_hotel_name']/span/text()")[0]
    except:
        hotel_type = ''

    try:
        country = tree.xpath('//div/a[contains(@href,"country")]/text()')[0].strip()
    except:
        country = ''

    try:
        city = tree.xpath('//div/a[contains(@href,"city")]/text()')[0].strip()
    except:
        city = ''

    try:
        hotel_name = tree.xpath("//h2[@id='hp_hotel_name']/text()")[1].strip()
    except:
        hotel_name = ''

    try:
        region = tree.xpath('//div/a[contains(@href,"region")]/text()')[0].strip()
    except:
        region = ''

    try:
        district = tree.xpath('//div/a[contains(@href,"district")]/text()')[0].strip()
    except:
        district = ''

    try:
        lat = tree.xpath('//a[@id="hotel_address"]/@data-atlas-latlng')[0].split(',')[0]
        lon = tree.xpath('//a[@id="hotel_address"]/@data-atlas-latlng')[0].split(',')[1]
    except:
        lat = ''
        lon = ''

    try:
        address = tree.xpath('//p[@class="address address_clean"]/a/following-sibling::span[1]/text()')[0].strip()
    except:
        address = ''

    try:
        # rating = tree.xpath('//*[@class="review-score-widget__body"]/ancestor::span/span/text()')[0].split()[0]
        rating = tree.xpath('//div[@data-testid="review-score-component"]/div[1]/text()')[0]
    except:
        rating = ''

    try:
        reviews = ''
        reviews_raw = tree.xpath('//div[@data-testid="review-score-component"]/div[last()]/span[last()]/text()')
        for review_raw in reviews_raw:
            if 'reviews' in review_raw:
                reviews = review_raw.replace('reviews', '').strip()
    except:
        reviews = ''

    try:
        staff_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Staff")]/following-sibling::span/text()')[0]
    except:
        staff_rating = ''

    try:
        facilities_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Facilities")]/following-sibling::span/text()')[0]
    except:
        facilities_rating = ''

    try:
        cleanliness_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Cleanliness")]/following-sibling::span/text()')[0]
    except:
        cleanliness_rating = ''

    try:
        comfort_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Comfort")]/following-sibling::span/text()')[0]
    except:
        comfort_rating = ''

    try:
        value_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Value for money")]/following-sibling::span/text()')[0]
    except:
        value_rating = ''

    try:
        location_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Location")]/following-sibling::span/text()')[0]
    except:
        location_rating = ''

    try:
        internet_rating = \
        tree.xpath('//*[@class="c-score-bar"]/*[contains(text(), "Free WiFi")]/following-sibling::span/text()')[0]
    except:
        internet_rating = ''

    scrape_dict = {}
    scrape_dict.update({
        'hotel_id': hotel_id,
        'hotel_url': url,
        'hotel_type': hotel_type,
        'hotel_name': hotel_name,
        'country': country,
        'region': region,
        'city': city,
        'district': district,
        'lat': lat,
        'lon': lon,
        'address': address,
        'rating': rating,
        'reviews': reviews,
        'staff_rating': staff_rating,
        'facilities_rating': facilities_rating,
        'cleanliness_rating': cleanliness_rating,
        'comfort_rating': comfort_rating,
        'value_rating': value_rating,
        'location_rating': location_rating,
        'internet_rating': internet_rating
    })

    # FOR DEBUGGING: ...
    # print('\n\n', scrape_dict, '\n\n')

    ROW_LIST_STATIC.append(scrape_dict)
    return ROW_LIST_STATIC


def scrape_booking_reviews(threads_number):
    booking_df = get_booking_url_gsheet()
    bcom_hotel_urls_list = booking_df['link'][booking_df['link'].replace('', np.nan).notna()].to_list()

    with concurrent.futures.ThreadPoolExecutor(max_workers=threads_number) as executor:
        executor.map(fetch_bcom_static_content, bcom_hotel_urls_list) # [0:10]

    bcom_static_content_df = pd.DataFrame(ROW_LIST_STATIC)

    bcom_static_content_df = bcom_static_content_df[bcom_static_content_df['hotel_id'].replace('', np.nan).notna()]
    bcom_static_content_df.loc[:, 'hotel_id'] = bcom_static_content_df.loc[:, 'hotel_id'].astype(int)
    booking_df = booking_df[booking_df['ID'].replace('', np.nan).notna()]
    booking_df['hotel_id'] = booking_df['ID'].astype(int)

    bcom_static_content_merge_df = bcom_static_content_df.merge(
        booking_df[['Neighbourhood', 'hotel_id']],
        on='hotel_id'
    )

    # adds time stamp for merge
    bcom_static_content_merge_df['timestamp'] = datetime.now()
    # push_data(dataframe=bcom_static_content_merge_df, table_name='scraping.booking_com_reviews')

    # Commented for debug
    # print(bcom_static_content_merge_df[['hotel_id', 'hotel_url', 'reviews', 'rating']])
    bcom_static_content_merge_df.to_gbq(
        destination_table='staging.booking_com_reviews',
        project_id='itdata-308413',
        location="europe-west2",
        chunksize=100000,
        if_exists='append'
    )


scrape_booking_reviews(THREADS)