# README #

### Quick summary
* Python Version   : Python 3.8.10
* Server Address   : 178.62.8.93
* Hostify API Docs : [https://staykeepers.co/docs](https://staykeepers.co/docs)
* Google Big Query : [itdata-308413:landlordhostifydatabase](https://console.cloud.google.com/bigquery?project=itdata-308413)  


### What is this repository for? ###
This project provides various scripts to fetch Hostify Data and update relevalant \
Google Big Query tables to collect Hostify data regularly, by scheduled cron job tasks. \
Take a look [here](https://staykeepers.co/docs) for instructions on how to start with **Hostify API**. 

A brief overview of **ELT Process** is available [here](https://miro.com/app/board/o9J_l72kx-0=/). 

### How do I get set up? ###

* Summary of set up
To install `pip`, follow the appropriate set of instructions [here](https://pip.pypa.io/en/stable/installing/). \
#this step requires the venv module to be installed along with Python, see https://docs.python.org/3/library/venv.html for more details

  python -m venv venv \
  source venv/bin/activate \
  pip install --upgrade pip \
  pip install -r requirements.txt 


* How to run tests
  
  python test_historical_data.py

## Project Structure

* `/config` Slack, Hostify, Google Big Query config and credentials.

* `/gbq` GBQ service functions to run queries. (Set Google API credentials,
         run sql queries to read data from or write data to gbq tables)

* `/hostify` 
      
  - `/api_clients` Listing, Reservation, Transaction, Calendar, Review API Clients 
      
  - `fetch_data.py` Main function calls to collect Hostify Data
      
  - `schemas.py`  Required column names which is extracted from Hostify data, to
                      create dataframes and push same columns/data types to gbq tables

* `/slack` Slack Webhook Client and slack_exception_handler decorator files.
           Occured errors and exceptions being pushed to etl_errors Slack channel.

*   Main scripts to trigger ELT process
    
    - `calendardays.py`                                                     
    - `listings.py`                             
    - `reservations.py`
    - `transactions.py` 
    - `reviews.py` 
      
* `utils.py` Data preprocess and comparison functions to compare new Hostify dataframes and source dataframes
      
* `/tests/test_historical_data.py` Unit tests for comparing source(gbq table) and 
                                   new data(hostify data) to find new inserts, updates
                                   or deleted data

## Data

* *Listings*
  
  [Hostify API Docs:](https://staykeepers.co/docs#list-listings) 

  STK_LISTINGS

    `GBQ Table Fullname : ` 'itdata-308413.landlordhostifydatabase.stk_listings' \
    `Hostify Account : ` StayKeepers \
    `Hostify API_KEY : ` STK_API_KEY (config/credentials.py) \
    `Schema : ` API_LISTING_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` API_LISTING_STK_CUSTOM_FIELDS (hostify/schemas.py) 

  LP_LISTINGS

    `GBQ Table Fullname : ` 'itdata-308413.landlordhostifydatabase.lp_listings' \
    `Hostify Account : ` LeapSllTll \
    `Hostify API_KEY : ` LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema : ` API_LISTING_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` API_LISTING_LP_CUSTOM_FIELDS (hostify/schemas.py)

  TLL_SLL_LISTINGS

    `GBQ Table Fullname : ` 'itdata-308413.landlordhostifydatabase.tll_sll_listings'  \
    `Hostify Account : ` LeapSllTll \
    `Hostify API_KEY : ` LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema : ` API_LISTING_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` API_LISTING_LP_CUSTOM_FIELDS (hostify/schemas.py) 



* *Reservations*
  
  [Hostify API Docs:](https://staykeepers.co/docs#list-reservations) 

  STK_RESERVATIONS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.stk_reservations'  \
    `Hostify Account :      ` StayKeepers \
    `Hostify API_KEY :      ` STK_API_KEY (config/credentials.py) \
    `Schema :               ` API_RESERVATION_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` API_RESERVATION_STK_CUSTOM_FIELDS (hostify/schemas.py)

  LP_RESERVATIONS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.lp_reservations'  \
    `Hostify Account :      `  LeapSllTll \
    `Hostify API_KEY :      `  LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema :               `  API_RESERVATION_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : `  API_RESERVATION_LP_CUSTOM_FIELDS (hostify/schemas.py)

  TLL_SLL_RESERVATIONS

    `GBQ Table Fullname :  ` 'itdata-308413.landlordhostifydatabase.tll_sll_reservations'  \
    `Hostify Account :     `  LeapSllTll \
    `Hostify API_KEY :     `  LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema :              `  API_RESERVATION_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema :`  API_RESERVATION_LP_CUSTOM_FIELDS (hostify/schemas.py) 



* *Transactions*
  
  [Hostify API Docs:](https://staykeepers.co/docs#list-transactions) 

  STK_TRANSACTIONS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.stk_transactions'  \
    `Hostify Account :      ` StayKeepers \
    `Hostify API_KEY :      ` STK_API_KEY (config/credentials.py) \
    `Schema :               ` API_TRANSACTION_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` 

  LP_TRANSACTIONS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.lp_transactions'  \
    `Hostify Account :      `  LeapSllTll \
    `Hostify API_KEY :      `  LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema :               `  API_TRANSACTION_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : `  

  TLL_SLL_TRANSACTIONS

    `GBQ Table Fullname :  ` 'itdata-308413.landlordhostifydatabase.tll_sll_transactions'  \
    `Hostify Account :     `  LeapSllTll \
    `Hostify API_KEY :     `  LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema :              `  API_TRANSACTION_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema :`   
 


* *Calendardays*
 
  [Hostify API Docs:](https://staykeepers.co/docs#calendar) 

  STK_TRANSACTIONS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.stk_calendardays'  \
    `Hostify Account :      ` StayKeepers \
    `Hostify API_KEY :      ` STK_API_KEY (config/credentials.py) \
    `Schema :               ` API_CALENDARDAY_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` 

  LP_TRANSACTIONS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.lp_calendardays'  \
    `Hostify Account :      `  LeapSllTll \
    `Hostify API_KEY :      `  LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema :               `  API_CALENDARDAY_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : `   

  TLL_SLL_TRANSACTIONS

    `GBQ Table Fullname :  ` 'itdata-308413.landlordhostifydatabase.tll_sll_calendardays'  \
    `Hostify Account :     `  LeapSllTll \
    `Hostify API_KEY :     `  LEAP_SLL_TLL_API_KEY (config/credentials.py) \
    `Schema :              `  API_CALENDARDAY_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema :`   


* *Reviews*
  
  [Hostify API Docs:](https://staykeepers.co/docs#list-reviews) 

  REVIEWS

    `GBQ Table Fullname :   ` 'itdata-308413.landlordhostifydatabase.reviews'   \
    `Hostify Account :      ` StayKeepers, LeapSllTll \
    `Hostify API_KEY :      ` STK_API_KEY, LEAP_SLL_TLL_API_KEY (config/credentials.py)  \
    `Schema :               ` API_REVIEW_SCHEMA (hostify/schemas.py) \
    `Custom Fields Schema : ` 



We keep historical data for Listings, Reservations and Transactions data
which means no deletion, appending each new data and updates of each api_id, and
also keep inactive/deleted data.


*How to detect inactive listings?*

Inactive/deleted data can be filtered by `is_deleted=True` flag.

If a listing with api_id 1234 exist in gbq_table - but does not exist in dataframe
which created by the newly fetched,  
latest Hostify data, we append new row for 
the listing(1234) with the column is_deleted as True to keep records also for
inactive listings.


## Example Workflow to collect STK Listings:

+ Trigger process by `import_stk_listings()` function
+ Fetch all STK Listings Data from Hostify by API Client
  
    + fetch_all_hostify_data(
                         api_client=STK_LISTING_API_CLIENT, \
                         resource_name='listings', \
                         schema=API_LISTING_SCHEMA, \
                         custom_fields_schema=API_LISTING_STK_CUSTOM_FIELDS, \
                         params={'per_page': 1000}
                        )

    + Extract required columns (general schema + custom fields)               
    
  `output` -> New Dataframe: Create a new dataframe with newly fetched Hostify data
              column names and datatypes of dataframe determined by \
              schema + custom fields schema
            - Add new column, is_deleted, and assign column value as False \ 
              for all dataframe - if we can fetch from Hostify, means all data is active.

+ Trigger data preprocess, determine data to be inserted, and push \
  
    `prepare_push_data(new_dataframe, all_schema, target_table='stk_listings')`
  - Rename new dataframe's id column as api_id, that's the naming we use for
      the unique id
  -  Trim all string columns of new dataframe : `trim_all_string_columns()` \
      remove leading and trailing whitespaces and replace \r, \n, \t with '' 

  + Read STK Listings GBQ Table to get latest data of each api_id by 
      comparing batch_inserted_at column.

      `fetch_historical_data(target_table='stk_listings', all_schema)`

      `output` -> Source Dataframe


  + Comparing Data of New Dataframe and Source Dataframe \
  
    `compare_data(target_table, new_dataframe, source_dataframe)` \
    
    Data to push(to_push_data):
    - Find new data to be inserted (i.e newly created listings or reservation)
    - Find existing listings data updates (i.e if a listing(api_id) title changes)
    - Find if any Hostify listing deleted - set is_deleted=True to keep inactive listings
    - Add new column, batch_inserted_at, and assign current utc time for all dataframe

  + Push data to gbq table - to_push_data dataframe rows will be appended
    as new gbq table's rows.

    `push_data(target_table='stk_listings', to_push_data)`



## Example Workflow to collect STK Calendardays:

  + Fetch STK Listings gbq table data
  + Extract api_id column to get list of STK Listings' id (active+inactive)
  + Fetch each listing's calendarday data one by one

    Sample Calendarday API Call parameters:

      {'listing_id': 156300, 
      'per_page': 1000, 
      'start_date': '2021-08-23',
      'end_date': '2022-05-20'}

    We collect 271 days data for each listing id.

  + Push new dataframe(which is calendardays of all stk listings
    between the dates: yesterday - (today + 9 months))

  + !! This process does not compare if new calendarday data exist in gbq tables and
    pushes(appends) every insert and updates.
    
  + GBQ script will remove duplicate calendarday data from the tables
    


## ETL pipeline from Properties Core 

+ Connects to Postrgresql database
+ propertyCore_to_Bq.py script runs SQL scripts agains database and deploys result into a BigQuery Table.
+ Request to properties Core should be a .sql file with the following structure containing sql code
+ BihQuery Table should be provided as argument to run the script

## ETL pipeline ingest pipedrive data
+ pipedrive_deals_to_Bq.py extracts all information uploaded to pipedrive and loads into a bq table
``` 