import requests
import json
import time
import os

from google.cloud import bigquery

CURR_DIR_PATH = os.path.dirname(os.path.abspath(__file__))
DIR_PATH = os.path.dirname(CURR_DIR_PATH)

# Set any default values for these variables if they are not found from Environment variables

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.path.join(DIR_PATH, 'etl_services/config/itdata-308413-36f4e74d7495.json')

PROJECT_ID = os.environ.get("GCP_PROJECT", "itdata-308413")
OPENEXCHANGERATES_KEY = os.environ.get("OPENEXCHANGERATES_KEY", "c7141e3151c540678285891f889c43fb")
REGIONAL_ENDPOINT = os.environ.get("REGIONAL_ENDPOINT", "europe-west2")
DATASET_ID = os.environ.get("DATASET_ID", "staging")
TABLE_NAME = os.environ.get("TABLE_NAME", "currency_rates_responses")
BASE_CURRENCY = os.environ.get("BASE_CURRENCY", "USD")


def main():

    latest_response = get_latest_currency_rates()
    write_to_bq(latest_response)
    return "Success"

def get_latest_currency_rates():
    
    params={'app_id': OPENEXCHANGERATES_KEY , 'base': BASE_CURRENCY}
    response = requests.get("https://openexchangerates.org/api/latest.json", params=params)
    print(response.json())
    return response.json()

def write_to_bq(response):

    # Instantiates a client
    bigquery_client = bigquery.Client(project=PROJECT_ID)

    # Prepares a reference to the dataset
    dataset_ref = bigquery_client.dataset(DATASET_ID)

    table_ref = dataset_ref.table(TABLE_NAME)
    table = bigquery_client.get_table(table_ref) 

    # get the current timestamp so we know how fresh the data is
    timestamp = time.time()
    # Ensure the Response is a String not JSON
    rows_to_insert = [{"timestamp":timestamp,"data":json.dumps(response)}]

    errors = bigquery_client.insert_rows(table, rows_to_insert)  # API request
    print(errors)
    assert errors == []

main()