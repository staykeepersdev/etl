#!/bin/bash

cd /srv/etl/
source venv/bin/activate

git checkout master
git pull

# installing python dependencies on the server
# psycopg2
sudo apt-get install gcc libpq-dev -y
sudo apt-get install python-dev  python-pip -y
sudo apt-get install python3-dev python3-pip python3-venv python3-wheel -y
pip3 install wheel

find . -name "*.pyc" -exec rm -rf {} \;

pip install -r requirements.txt
